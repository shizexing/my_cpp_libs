set(TARGET_NAME "cxx_test")

file(GLOB FILES
    ${CMAKE_CURRENT_SOURCE_DIR}/*.h
    ${CMAKE_CURRENT_SOURCE_DIR}/*.hpp
    ${CMAKE_CURRENT_SOURCE_DIR}/*.cpp
    )

add_executable(${TARGET_NAME} ${FILES})
add_dependencies(${TARGET_NAME} ${PROJECT_NAME})

# link internal library
if (MSVC)
	set(LIB_NAME_SUFFIX lib)
else()
	set(LIB_NAME_SUFFIX a)
endif()
if(TN_PLATFORM_LINUX OR TN_PLATFORM_QNX)
    target_link_libraries(${TARGET_NAME} ${CMAKE_BINARY_DIR}/lib${PROJECT_NAME}.a)
elseif(TN_PLATFORM_IOS)
    set(LIB_DIR "${CMAKE_BINARY_DIR}/\$(CONFIGURATION)\$(EFFECTIVE_PLATFORM_NAME)")
    target_link_libraries(${TARGET_NAME}
        debug ${LIB_DIR}/lib${PROJECT_NAME}.a
        optimized ${LIB_DIR}/lib${PROJECT_NAME}.a
    )
else()  # OSX & WINDOWS
    target_link_libraries(${TARGET_NAME}
        debug ${CMAKE_BINARY_DIR}/debug/lib${PROJECT_NAME}.${LIB_NAME_SUFFIX}
        optimized ${CMAKE_BINARY_DIR}/release/lib${PROJECT_NAME}.${LIB_NAME_SUFFIX}
    )
endif()

# link external library
include(${CMAKE_SOURCE_DIR}/cmake/util.cmake)
set(LIBS
    gtest
    )
tn_target_link_libraries(${TARGET_NAME}
    ${LIBS}
)