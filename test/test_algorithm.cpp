//
//  test_algorithm.cpp
//  demo
//
//  Created by Shi, Zexing on 7/28/18.
//

#include <vector>

#include <gtest/gtest.h>

#include <cxx/algorithm.hpp>

TEST(TEST_ALGORITHM, TEST_find_if_not)
{
    std::vector<int> v{0, 1, 2, 3, 4};
    std::vector<int>::const_iterator it = std::find_if_not(v.begin(), v.end(), [](int i)->bool{
        return 0 <= i;
    });
    ASSERT_EQ(it, v.end());
    it = std::find_if_not(v.begin(), v.end(), [](int i)->bool{
        return 4 > i;
    });
    ASSERT_NE(it, v.end());
}

TEST(TEST_ALGORITHM, TEST_any_of)
{
    std::vector<int> v{0, 1, 2, 3, 4};
    bool has = std::any_of(v.begin(), v.end(), [](int i)->bool{
        return 5 == i;
    });
    ASSERT_FALSE(has);
    has = std::any_of(v.begin(), v.end(), [](int i)->bool{
        return 3 == i;
    });
    ASSERT_TRUE(has);
}

TEST(TEST_ALGORITHM, TEST_none_of)
{
    std::vector<int> v{0, 1, 2, 3, 4};
    bool has = std::none_of(v.begin(), v.end(), [](int i)->bool{
        return i < 0;
    });
    ASSERT_TRUE(has);
    has = std::none_of(v.begin(), v.end(), [](int i)->bool{
        return i <= 0;
    });
    ASSERT_FALSE(has);
}
