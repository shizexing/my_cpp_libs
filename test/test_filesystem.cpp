//
//  test_filesystem.cpp
//  cxx_test
//
//  Created by Shi, Zexing on 11/22/18.
//

// XCode10 (clang) support __has_include
#if defined(__has_include)
#   if __has_include(<filesystem>)
#       pragma message("hooray!, filesystem is supported")
#       include <filesystem>
#   elif __has_include(<experimental/filesystem>)
#       pragma message("hooray!, filesystem is supported")
#       include <experimental/filesystem>
#   else
#       include <cxx/filesystem.hpp>
#   endif   // filesystem
#else
#   include <cxx/filesystem.hpp>                       // filesystem
#endif  // __has_include

#include <gtest/gtest.h>

TEST(filesystem, path)
{
    // constructor
    std::filesystem::path p("./fs");
    ASSERT_EQ(p.string(), "./fs");
    
    std::filesystem::path p2("./a/b/c/def/gh/test.json");
    std::cout << "p=" << p2.parent_path().string() << std::endl;
    
    std::filesystem::path p0("./abc.db");
    ASSERT_EQ(std::filesystem::path("."), p0.parent_path());
    p0 = std::filesystem::path("def.db");
    ASSERT_EQ(std::filesystem::path("."), p0.parent_path());
    p0 = "./1/2/aaa.db";
    ASSERT_EQ(std::filesystem::path("./1/2"), p0.parent_path());
    p0 = "/bb.db";
    ASSERT_EQ(std::filesystem::path("/"), p0.parent_path());
    p0 = ".";
    ASSERT_EQ(std::filesystem::path("."), p0.parent_path());
    p0 = "/";
    ASSERT_EQ(std::filesystem::path("/"), p0.parent_path());
}

TEST(filesystem, op)
{
    namespace fs = std::filesystem;
    fs::file_status status = fs::status("./fs");
    ASSERT_TRUE(fs::status_known(status));
    
    fs::create_directory("./fs");
    ASSERT_TRUE(fs::is_directory("./fs"));
    fs::remove_all("./fs");
    ASSERT_FALSE(fs::is_directory("./fs"));
    
    fs::path raw_path("fs");
    bool b = fs::create_directory(raw_path);
    ASSERT_TRUE(b);
    ASSERT_TRUE(fs::is_directory(raw_path));
    
    fs::remove(raw_path);
    ASSERT_FALSE(fs::exists(raw_path));
    
    fs::path root("fs/a/b/c");
    fs::create_directories(root);
    ASSERT_TRUE(fs::is_directory(root));
    
    fs::remove_all("fs");
    ASSERT_FALSE(fs::exists("fs"));
    
    // test space
    fs::space_info si = fs::space(".");
    std::cout << si.capacity << ", " << si.available << ", " << si.free << std::endl;
    EXPECT_GE(si.capacity, 0);
    EXPECT_GE(si.available, 0);
    EXPECT_GE(si.free, 0);
    fs::space_info si2 = fs::space("/Users/zexings/project/map/navcore/README.md");
    EXPECT_GE(si2.capacity, 0);
    EXPECT_GE(si2.available, 0);
    EXPECT_GE(si2.free, 0);
}
