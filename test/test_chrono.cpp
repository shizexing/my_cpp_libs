//
//  test_chrono.cpp
//  cxx_test
//
//  Created by Shi, Zexing on 11/27/18.
//

#include <gtest/gtest.h>

#include <cxx/chrono.hpp>

TEST(chrono, time)
{
    std::chrono::system_clock::time_point pt = std::chrono::system_clock::now();
    auto d = pt.time_since_epoch();
    std::cout << "time since epoch:" << d.count() << std::endl;
    std::chrono::duration<double, std::ratio<1> > dd = (d);
    std::cout << "time since epoch:" << std::setprecision(13) << dd.count() << std::endl;
}
