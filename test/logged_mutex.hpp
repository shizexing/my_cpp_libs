//
//  logged_mutex.hpp
//  demo
//
//  Created by Shi, Zexing on 7/28/18.
//

#ifndef logged_mutex_h
#define logged_mutex_h

#include <sstream>                              // stringstream

#include <cxx/macros.hpp>                       // __FUNCTION__
#include <cxx/chrono.hpp>                       // chrono
#include <cxx/mutex.hpp>                        // mutex
#include <cxx/thread.hpp>                       // this_thread

struct logged_mutex {
    typedef std::timed_mutex mutex_type;

    logged_mutex()
        : name("mutex_")
        , locked(false)
    {
        static size_t idx = 0;
        name += std::to_string(idx++);
    }
    logged_mutex(const std::string& name)
        : name(name)
        , locked(false)
    {}
    void lock() {
        m.lock();
        locked = true;
        std::stringstream ss;
        ss << "[" << std::this_thread::get_id() << "]" << __FUNCTION__ << "[" << name << "] :" << this << std::endl;
        std::cout << ss.str() << std::endl;
    }
    bool try_lock() {
        std::stringstream ss;
        ss << "[" << std::this_thread::get_id() << "]" <<  __FUNCTION__ << "[" << name << "] :" << this << std::endl;
        std::cout << ss.str() << std::endl;
        bool ret = m.try_lock();
        if (ret)    locked = true;
        return ret;
    }
    template <class _Rep, class _Period>
    bool try_lock_for(const std::chrono::duration<_Rep, _Period>& d)
    {
        return locked |= try_lock_until(std::chrono::steady_clock::now() + d);
    }
    template <class _Clock, class _Duration>
    bool try_lock_until(const std::chrono::time_point<_Clock, _Duration>& tp)
    {
        return locked |= m.try_lock_until(tp);
    }
    void unlock() {
        if (!locked)
        {
            std::stringstream ss;
            ss << "[" << std::this_thread::get_id() << "]" <<  __FUNCTION__ << "[" << name << "] : unlock not locked one?\n";
            std::cerr << ss.str() << std::endl;
        }
        std::stringstream ss;
        ss << "[" << std::this_thread::get_id() << "]" <<  __FUNCTION__ << "[" << name << "] :" << this << std::endl;
        std::cout << ss.str() << std::endl;
        locked = false;
        m.unlock();
    }
    
    bool is_locked() {
        return locked;
    }
private:
    
    logged_mutex(const logged_mutex&);// = delete;
    logged_mutex& operator=(const logged_mutex&);// = delete;
    
    mutex_type m;
    std::string name;
    volatile bool locked;
};

#endif /* logged_mutex_h */
