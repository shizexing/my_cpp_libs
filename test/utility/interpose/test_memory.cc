// clang test.cc -o test
// clang test.cc -force_flat_namespace -o test 
// clang test.cc -force_flat_namespace -L. -lmalloc_trace -o test 
// this is test example to use malloc

// To test malloc_trace.c, you should build it to dynamic libs and run
// 

#include <stdio.h>
#include <stdlib.h>
#include <string.h>             // memset
#include <future>
#include <chrono>

void test_malloc(int i)
{
    char * s = (char*)malloc(sizeof(char) * (i + 1));
    if (s)
    {
    memset(s, 'a' + i, i);
    s[i] = '\0';
    printf("s[%d]:%s\n", i, s);
    free(s);
    }
    else
    {
        fprintf(stderr, "malloc failed for %d\n", i);
    }
}

void test_new(int i)
{
    int* n = new int[i];
    if (n)
    {
        n[0] = i * 2;
        printf("2n:%d\n", n[0]);
        delete[] n;
    }
    else
    {
        fprintf(stderr, "new int : %d failed\n", i);
    }
}

// posix_memalign

// memalign

// std::allocator

// realloc

int main() {
    int n = 5;
    int *a = (int*)malloc(sizeof(int) * n);
    for (int i = 0; i < n; ++i) {
        a[i] = i;
        printf("i = %d\n", a[i]);
    }
    free(a);

    for (int i = 1; i < 10; i++)
    {
        if (i % 2 == 1)
        {
            std::async(std::launch::async, test_malloc, i);
        }
        else
        {
            std::async(std::launch::async, test_new, i);
        }
    }
    printf("sleep for 2s\n");
    std::this_thread::sleep_for(std::chrono::seconds(2));
    printf("exit main thread!\n");
    return 0;
}

