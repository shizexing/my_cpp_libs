/**
 * Copyright (c) 2018 Shi, Zexing
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 * 
 * test_io.cpp
 * Created by Shi, Zexing on 07/02/18.
 * 
 */
#include <stdio.h>
#include <fstream>
#include <iostream>
#include <string>

auto constexpr fn = "test_fopen.txt";

void test_fopen()
{
    FILE* fp = fopen(fn, "w");
    if (!fp)
    {
        fprintf(stderr, "cannot open file : %s\n", fn);
        return;
    }
    fprintf(fp, "hello fopen!\n");
    fflush(fp);
    fclose(fp);
}

void test_fstream()
{
    std::ifstream fs(fn);
    if (!fs.is_open())
    {
        std::cerr << "cannot open fstream: " << fn << std::endl;
        return;
    }
    fs.seekg(0, std::ios_base::end);
    size_t size = fs.tellg();
    std::cout << "file size: " << size << std::endl;
    std::string buf(size, '\0');
    fs.seekg(std::ios_base::beg);   // rewind
    fs.read(&buf[0], size);
    std::cout << "content:" << buf << std::endl;
}

int main(int argc, char **argv)
{
    test_fopen();
    test_fstream();

    return 0;
}