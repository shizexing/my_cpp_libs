//
//  Delegate.h
//  test/utility
//
//  Created by Shi, Zexing on 05/06/2019.
//

#include <memory>

#pragma mark - Delegate
class Delegate {
public:
    using ptr = std::shared_ptr<Delegate>;
public:
    Delegate(/* ... */);
};    // Delegate

#pragma mark - DelegateBuilder
class DelegateBuilder {
public:
    using ptr = std::shared_ptr<DelegateBuilder>;
public:
    virtual ~DelegateBuilder();
    virtual Delegate::ptr createDelegate(/*Args for Delegate(...) constructor*/);
};	// DelegateBuilder

#pragma mark - DelegateFactory
class DelegateFactory final {
    DelegateBuilder::ptr fBuilder;
public:
    static DelegateFactory& instance();
    Delegate::ptr createDelegate();
    void setBuilder(DelegateBuilder::ptr &&builder);
private:
    DelegateFactory() = default;
    DelegateFactory(WebServiceDelegateFactory const&) = delete;
    DelegateFactory(WebServiceDelegateFactory&&) = delete;
    DelegateFactory& operator=(WebServiceDelegateFactory const&) = delete;
    DelegateFactory& operator=(WebServiceDelegateFactory&&) = delete;
};	// DelegateFactory
