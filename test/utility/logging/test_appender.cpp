/**
 * @Copyright (c) 2018 Shi, Zexing
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 * 
 * 
 * test_logging.cpp
 * Created by Shi, Zexing on 05/19/18.
 * 
 */

#include "utility/logging/appender.h"

int test_appender();

bool test_zout() 
{
    std::cout << std::string(20, '=') << __FUNCTION__ << std::string(20, '=') << std::endl; 
    zout << "[out] zout OK?\n";
    zout.printf("[out] @%s. %s+%d", __FUNCTION__, __FILE__, __LINE__) << "\n" 
        << "[out] zout ok!";
    zout << std::endl;
    zout << std::endl;
    return true;
}

bool test_zerr()
{
    std::cout << std::string(20, '=') << __FUNCTION__ << std::string(20, '=') << std::endl; 
    zerr << "[err] expect to put into stderr\n";
    zerr << std::endl;
    return true;
}

bool test_file()
{
    std::cout << std::string(20, '=') << __FUNCTION__ << std::string(20, '=') << std::endl; 
    using namespace zs::utils::logging;
    appender app("test.logging");
    app << "[f] how are you?\n";
    app.printf("[f] %s, %d\n", __FUNCTION__, __LINE__) << "byebye\n";
    std::cout << std::endl;
    return true;
}

bool test_wchar()
{
    // reference about local & facet:
    //  1. http://www.cantrip.org/locale.html
    //  2. http://www.cnblogs.com/zyl910/archive/2013/01/20/wchar_crtbug_01.html
    //  3. http://en.cppreference.com/w/cpp/locale/locale
    //  4. https://stackoverflow.com/questions/1096291/how-to-format-my-own-objects-when-using-stl-streams/
    // std::locale::global(std::locale(""));
    std::cout << "User-preferred locale setting is " << std::locale("").name() << std::endl;
    std::wcout.imbue(std::locale(""));
    std::cout << std::string(20, '=') << __FUNCTION__ << std::string(20, '=') << std::endl; 
    std::wcout << L"中文" << " chars:" << "english" << std::endl;
    using namespace zs::utils::logging;
    basic_appender<wchar_t> app;
    app << L"[wout] 您好！。" << std::endl;
    app << std::endl;
    return true;
}

int test_appender()
{
    std::cout << std::endl;
    int r = 0;
    if (!test_zout())   ++r;
    if (!test_zerr())   ++r;
    if (!test_file())   ++r;
    if (!test_wchar())  ++r;
    return r;
}
