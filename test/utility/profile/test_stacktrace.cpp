/**
 * Copyright (c) 2018 Shi, Zexing
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 * 
 * 
 * test_stacktrace.cpp
 * Created by Shi, Zexing on 05/18/18.
 * 
 */
#include "utility/profile/stacktrace.h"

#include <cstdio>
#include <iostream>

// forward declaration
void test();

int main(int argc, char **argv) {
    test();
    return 0;
}

int test_function(int n) {
    using namespace zs::utils::profile;
    stacktrace(std::cout);
    return n+1;
}

template <class T>
bool test_template_function(T t) {
    using namespace zs::utils::profile;
    stacktrace(std::cout);
    std::cout << "t=" << t << std::endl;
    return true;
}

void test_var_args(const char* fmt, ...) {
    // TODO
}

void test() {
    test_function(3);
    test_template_function<char>('K');
}