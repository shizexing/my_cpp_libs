//
//  test_mutex.cpp
//  demo
//
//  Created by Shi, Zexing on 7/28/18.
//

#include <gtest/gtest.h>

#include <cxx/functional.hpp>                           // ref
#include <cxx/future.hpp>                               // async
#include <cxx/mutex.hpp>                                // mutex

#include "logged_mutex.hpp"                             // logged_mutex

TEST(TEST_MUTEX, TEST_adopt_lock)
{
    logged_mutex m;
    m.lock();
    ASSERT_TRUE(m.is_locked());
    {
        std::lock_guard<logged_mutex> lk(m, std::adopt_lock);
        ASSERT_TRUE(m.is_locked());
    }
    ASSERT_FALSE(m.is_locked());
}

TEST(TEST_MUTEX, TEST_lock)
{
    logged_mutex m1, m2, m3;
    std::lock(m1, m2, m3);
    ASSERT_TRUE(m1.is_locked());
    ASSERT_TRUE(m2.is_locked());
    ASSERT_TRUE(m3.is_locked());
    m1.unlock();
    m2.unlock();
    m3.unlock();
}

TEST(TEST_MUTEX, TEST_lock_guard)
{
    logged_mutex m;
    {
        std::lock_guard<logged_mutex> lk(m);
        ASSERT_TRUE(m.is_locked());
    }
    ASSERT_FALSE(m.is_locked());
}

TEST(TEST_MUTEX, TEST_once)
{
    std::once_flag f;
    int i = 0;
#if 0	// gcc4.8.1 has compile error
// /usr/local/gcc481/include/c++/4.8.1/functional: In instantiation of ‘struct std::_Bind_simple<TEST_MUTEX_TEST_once_Test::TestBody()::__lambda4(int)>’:
// /usr/local/gcc481/include/c++/4.8.1/mutex:775:41:   required from ‘void std::call_once(std::once_flag&, _Callable&&, _Args&& ...) [with _Callable = TEST_MUTEX_TEST_once_Test::TestBody()::__lambda4; _Args = {int&}]’
// /data/02/zxshi/project/navcore/cxx/test/test_mutex.cpp:55:9:   required from here
// /usr/local/gcc481/include/c++/4.8.1/functional:1697:61: error: no type named ‘type’ in ‘class std::result_of<TEST_MUTEX_TEST_once_Test::TestBody()::__lambda4(int)>’
//       typedef typename result_of<_Callable(_Args...)>::type result_type;
// /usr/local/gcc481/include/c++/4.8.1/functional:1727:9: error: no type named ‘type’ in ‘class std::result_of<TEST_MUTEX_TEST_once_Test::TestBody()::__lambda4(int)>’
//          _M_invoke(_Index_tuple<_Indices...>)
// make[2]: *** [test/CMakeFiles/cxx_test.dir/test_mutex.cpp.o] Error 1
// make[1]: *** [test/CMakeFiles/cxx_test.dir/all] Error 2
    auto func = [&]() {
        std::call_once(f, [](int & i){
            ++i;
        }, i);
    };
#else
    auto func = [&]() {
        std::call_once(f, std::bind([](int & i){
            ++i;
        }, std::ref(i)));
    };
#endif	// 0
    ASSERT_EQ(0, i);
    auto t1 = std::async(std::launch::async, [&](){
        func();
    });
    auto t2 = std::async(std::launch::async, [&](){
        func();
    });
    auto t3 = std::async(std::launch::async, [&](){
        func();
    });
    t1.get();
    t2.get();
    t3.get();
    ASSERT_EQ(1, i);
    func();
    ASSERT_EQ(1, i);
}

TEST(TEST_MUTEX, TEST_scoped_lock)
{
    logged_mutex m1, m2;
    {
        std::scoped_lock<logged_mutex, logged_mutex> lk(m1, m2);
        ASSERT_TRUE(m1.is_locked() && m2.is_locked());
    }
    ASSERT_TRUE(!m1.is_locked() && !m2.is_locked());
    auto t1 = std::async(std::launch::async, [&](){
        std::scoped_lock<logged_mutex, logged_mutex> lk(m1, m2);
        ASSERT_TRUE(m1.is_locked() && m2.is_locked());
    });
    auto t2 = std::async(std::launch::async, [&](){
        std::scoped_lock<logged_mutex, logged_mutex> lk(m2, m1);
        ASSERT_TRUE(m1.is_locked() && m2.is_locked());
    });
    auto t3 = std::async(std::launch::async, [&](){
        std::scoped_lock<logged_mutex, logged_mutex> lk(m1, m2);
        ASSERT_TRUE(m1.is_locked() && m2.is_locked());
    });
    auto t4 = std::async(std::launch::async, [&](){
        std::scoped_lock<logged_mutex, logged_mutex> lk(m2, m1);
        ASSERT_TRUE(m1.is_locked() && m2.is_locked());
    });
    t1.get();
    t2.get();
    t3.get();
    t4.get();
    ASSERT_TRUE(!m1.is_locked() && !m2.is_locked());
}

TEST(TEST_MUTEX, TEST_timed_mutex)
{
    std::timed_mutex tm;
    tm.lock();
    auto t1 = std::async(std::launch::async, [&]() -> bool {
        return tm.try_lock_for(std::chrono::seconds(1));
    });
    auto t2 = std::async(std::launch::async, [&](){
        std::this_thread::sleep_for(std::chrono::seconds(2));
        tm.unlock();
    });
    auto t3 = std::async(std::launch::async, [&]() -> bool{
        return tm.try_lock_for(std::chrono::seconds(4));
    });
    t2.get();
    ASSERT_FALSE(t1.get());
    ASSERT_TRUE(t3.get());
    tm.unlock();
}
