//
//  job_download_size.cpp
//  demo
//
//  Created by Shi, Zexing on 10/23/18.
//

#include "job_download_size.h"

#include <iostream>

#include "utility.h"

namespace job {
    
std::string JobCalcDownloadSize::key() const {
    return KEY;
}

static const char* const ARG_KEY_ROOT_PATH = "root";
static const char* const ARG_KEY_MIN_LAT = "min_lat";
static const char* const ARG_KEY_MIN_LNG = "min_lng";
static const char* const ARG_KEY_MAX_LAT = "max_lat";
static const char* const ARG_KEY_MAX_LNG = "max_lng";

boost::shared_ptr<boost::program_options::option_description> JobCalcDownloadSize::option() const {
    static boost::shared_ptr<boost::program_options::option_description> OPTION(new boost::program_options::option_description(KEY, new boost::program_options::untyped_value(true), "download size statistic"));
    return OPTION;
}

boost::program_options::options_description JobCalcDownloadSize::options() const {
    boost::program_options::options_description sub_cmd_options("download size statistic.\n \
                                                                usage:  --download_size --root $ROOT_PATH --min_lat ### --min_lng ### --max_lat ### --max_lng ### \n\n \
                                                                Options");
    sub_cmd_options.add(option());
    sub_cmd_options
    .add_options()
    (ARG_KEY_ROOT_PATH, boost::program_options::value<std::string>()->value_name("root")->required(), "root path")
    (ARG_KEY_MIN_LAT, boost::program_options::value<double>()->value_name(ARG_KEY_MIN_LAT)->required(), "min latitude")
    (ARG_KEY_MIN_LNG, boost::program_options::value<double>()->value_name(ARG_KEY_MIN_LNG)->required(), "min longitude")
    (ARG_KEY_MAX_LAT, boost::program_options::value<double>()->value_name(ARG_KEY_MAX_LAT)->required(), "max latitude")
    (ARG_KEY_MAX_LNG, boost::program_options::value<double>()->value_name(ARG_KEY_MAX_LNG)->required(), "max longitude")
    ;
    return sub_cmd_options;
}

bool JobCalcDownloadSize::operator()(const boost::program_options::variables_map &vm) const {
    // initialize
    const std::string& root = vm[ARG_KEY_ROOT_PATH].as<std::string>();
    double min_lat = vm[ARG_KEY_MIN_LAT].as<double>();
    double min_lng = vm[ARG_KEY_MIN_LNG].as<double>();
    double max_lat = vm[ARG_KEY_MAX_LAT].as<double>();
    double max_lng = vm[ARG_KEY_MAX_LNG].as<double>();
    
    std::cout << "root:" << root << ", " << std::setprecision(10) << min_lat << "," << min_lng << " " << max_lat << "," << max_lng << std::endl;
    
    // initialize data access
    tn::map::DataAccess::ptr p = tn::map::DataUtility::AccessInstance();
    if (!p)
    {
        std::cerr << "cannot create map DataAccess instance!!!\n";
        return false;
    }
    bool isInit = p->initialize("", root);
    if (!isInit)
    {
        std::cerr << "initialize map DataAccess failure!!!\n";
        return false;
    }
    int n = 0;
    while (!p->isInitializationDone()) {
        std::this_thread::sleep_for(std::chrono::seconds(1));
        ++n;
//        if (n > 30) break;
        if (n > 3000) break;
    }
    if (!p->isInitializationDone()) {
        std::cerr << "warmup map DataAccess failure!!!\n";
        return false;
    }
    tn::map::BoundingBox bb(min_lat, min_lng, max_lat, max_lng);
    std::vector<tn::map::FileStatus> fs;
    bool r = util::filelist(*p, bb, fs);
    if (!r)
    {
        std::cerr << "get filelist failure!!!\n";
        return false;
    }
//    std::cout << "fs:" << fs << std::endl;
    std::cout << "fs:" << std::endl;
    size_t size = 0;
    for (std::vector<tn::map::FileStatus>::const_iterator it = fs.begin(); it != fs.end(); ++it)
    {
        size += it->size;
        std::cout << *it << std::endl;
    }
//    std::cout << "total size = " << size << " B, " << (size / 1024.f) << "KB, " << (size / 1024.f / 1024) << "MB" << std::endl;
    std::cout << "total size = " << std::setprecision(4) << (size / 1024.f / 1024) << "MB" << std::endl;
    return true;
}

static bool sRegisterStatus = JobExecutor::instance().register_job_handler(std::unique_ptr<JobCalcDownloadSize>(new JobCalcDownloadSize));

}   // job

