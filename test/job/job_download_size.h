//
//  job_download_size.hpp
//  demo
//
//  Created by Shi, Zexing on 10/23/18.
//

#ifndef job_download_size_hpp
#define job_download_size_hpp

#include "common/job.h"

#include <cxx/keywords.hpp>                         // CONSTEXPR, override

namespace job {

// Paris metro
// --download_size --root /Users/zexings/data/ngx/eu_here_17q3/ --min_lat 48.754406 --min_lng 2.247984 --max_lat 48.883750 --max_lng 2.455045
// 66761093 B, 65196.4KB, 63.6683MB

// --download_size --root /Users/zexings/data/ngx/eu_here_18q1/ --min_lat 48.901213 --min_lng 2.236256 --max_lat 48.753844 --max_lng 2.459008
// 66761093 B, 65196.4KB, 63.6683MB
    
// Berlin Rural
// --download_size --root /Users/zexings/data/ngx/eu_here_17q3/ --min_lat 52.314430 --min_lng 13.048412 --max_lat 52.413682 --max_lng 13.313743
// 18670612 B, 18233KB, 17.8057MB

// San Francisco Metro
// --download_size --root /Users/zexings/data/ngx/na_here_16q2/ --min_lat 37.72829 --min_lng -122.489954 --max_lat 37.797969 --max_lng -122.38387
// 63569624 B, 62079.7KB, 60.6247MB
    
// San Francisco Rural
// --download_size --root /Users/zexings/data/ngx/na_here_16q2/ --min_lat 37.774729 --min_lng -122.2944 --max_lat 37.872174 --max_lng -122.24858
// 60798570 B, 59373.6KB, 57.982MB

// Detroit Metroit
// --download_size --root /Users/zexings/data/ngx/na_here_16q2/ --min_lat 42.327709 --min_lng -82.984113 --max_lat 42.342175 --max_lng -83.065241
// 69153810 B, 67533KB, 65.9502MB


/*
 Paris metro    48.754406, 2.247984    48.883750, 2.455045    9 mi * 9 mi
 Berlin Rural    52.314430, 13.048412    52.413682, 13.313743    11 mi * 7 mi
 San Francisco Metro    37.72829, -122.489954    37.797969, -122.38387    6 mi * 6 mi
 San Francisco Rural    37.774729, -122.2944    37.872174, -122.24858    2.5 mi * 6.7 mi
 Detroit Metroit    42.327709, -82.984113    42.342175, -83.065241    4 mi * 1 mi
*/
    
/**
 * statistic download size
 *
 * usage: --download_size --root $ROOT_PATH --min_lat # --min_lng # --max_lat # --max_lng #
 */
class JobCalcDownloadSize : public JobHandler {
public:
    static auto CONSTEXPR KEY = "download_size";
    
    virtual std::string key() const override;
    
    virtual boost::shared_ptr<boost::program_options::option_description> option() const override;
    
    virtual boost::program_options::options_description options() const override;
    
    virtual bool operator()(const boost::program_options::variables_map &vm) const override;
    
};  // JobCalcDownloadSize
    
}   // job


#endif /* job_download_size_hpp */
