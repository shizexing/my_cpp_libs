//
//  test_thread_utils.cpp
//  cxx_test
//
//  Created by Shi, Zexing on 9/21/18.
//
#if defined(__APPLE__)
#   include <sched.h>
#endif  // __APPLE__
#include <gtest/gtest.h>

#include <cxx/utility/thread.h>

TEST(TEST_THREAD_UTILS, TEST_name)
{
    std::thread t([](){
        util::thread::set_name("thread1");
        std::this_thread::sleep_for(std::chrono::milliseconds(100));
        std::cout << "thread name:" << std::this_thread::get_id() << std::endl;
        EXPECT_EQ("thread1", util::thread::get_name());
    });
    std::thread t2([]{
        util::thread::set_name("thread2");
        std::cout << "thread 2 running\n";
        EXPECT_EQ("thread2", util::thread::get_name());
    });
    std::this_thread::sleep_for(std::chrono::milliseconds(50));
#if !defined(WIN32) && !defined(_WIN32)
    // NOTE: get_name(std::thread&) is not working on WINDOWS
    std::string name = util::thread::get_name(t);
    ASSERT_EQ(name, "thread1");
    name = util::thread::get_name(t2);
    ASSERT_EQ(name, "thread2");
#endif  // !Win32
    if (t.joinable())
    {
        t.join();
    }
    if (t2.joinable())
    {
        t2.join();
    }
}

static void get_min_max_priority(int& min, int& max)
{
#if defined(__APPLE__)
    min = sched_get_priority_min(SCHED_FIFO);
    max = sched_get_priority_max(SCHED_FIFO);
    std::cout << "priority range for SCHED_FIFO: " << min << " - " << max << std::endl;
    min = sched_get_priority_min(SCHED_RR);
    max = sched_get_priority_max(SCHED_RR);
    std::cout << "priority range for SCHED_RR: " << min << " - " << max << std::endl;
    min = sched_get_priority_min(SCHED_OTHER);
    max = sched_get_priority_max(SCHED_OTHER);
    std::cout << "priority range for SCHED_OTHER: " << min << " - " << max << std::endl;
#endif  // __APPLE__
}

TEST(TEST_THREAD_UTILS, TEST_priority)
{
    int i = 0;
    std::thread t([&](){
        std::this_thread::sleep_for(std::chrono::milliseconds(10));
        util::thread::set_priority(23);
        ++i;
    });
    std::thread t2([&]{
        i += 2;
    });
    
    int min = 15;
    int max = 47;
    
    get_min_max_priority(min, max);
    
    int o = util::thread::get_priority(t);
    bool r = util::thread::set_priority(t2, o - 1);
    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    std::cout << "default priority:" << o << std::endl;
    ASSERT_EQ(23, util::thread::get_priority(t));
    if (r)
    {
        ASSERT_EQ(o - 1, util::thread::get_priority(t2));
    }
    if (t.joinable())
    {
        t.join();
    }
    if (t2.joinable())
    {
        t2.join();
    }
    ASSERT_EQ(3, i);
}

TEST(thread, sleep)
{
    std::chrono::system_clock::time_point p0 = std::chrono::system_clock::now();
    std::this_thread::sleep_for(std::chrono::duration<double>(0.1));
    std::chrono::system_clock::time_point p1 = std::chrono::system_clock::now();
    auto d = p1 - p0;
    auto d2 = std::chrono::duration_cast<std::chrono::milliseconds>(d);
    std::cout << "d2 = " << d2.count() << std::endl;
    ASSERT_GE(d2.count(), 100);
}
