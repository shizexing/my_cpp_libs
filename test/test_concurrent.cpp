//
//  test_concurrent_stack.cpp
//  cxx_test
//
//  Created by Shi, Zexing on 8/3/18.
//

#include <gtest/gtest.h>

#include <array>                                        // array

#include <concurrent/deque.hpp>                         // concurrent::deque
#include <concurrent/queue.hpp>                         // concurrent::queue
#include <concurrent/stack.hpp>                         // concurrent::stack
#include <concurrent/utility.hpp>                       // concurrent<T>
#include <cxx/future.hpp>                               // async

TEST(TEST_CONCURRENT, TEST_stack)
{
    concurrent::stack<int> st;
    std::vector<std::future<void> > rs;
    for (int i = 0; i < 10; ++i)
    {
        rs.emplace_back(std::async(std::launch::async, [&](int i) {
            st.push(i);
        }, i));
    }
    for (auto& r : rs)
    {
        r.get();
    }
    ASSERT_EQ(10, st.size());
    std::array<bool, 10> arr;
    auto t1 = std::async(std::launch::async, [&](){
        while(!st.empty()) {
            int v = st.top();
            st.pop();
            arr[v] = true;
        }
    });
    auto t2 = std::async(std::launch::async, [&](){
        while(!st.empty()) {
            int v = st.top();
            st.pop();
            arr[v] = true;
        }
    });
    t1.get();
    t2.get();
    ASSERT_TRUE(st.empty());
    ASSERT_TRUE(std::none_of(arr.begin(), arr.end(), [](bool v)->bool{
        return !v;
    }));
}

class overloaded_allocator
{
public:
    /**
     * override operator new
     */
    void *operator new(size_t size)
    {
        return new_p(size);
    }
    /**
     * override operator delete
     */
    void operator delete(void *p)
    {
        free(p);
    }
    /**
     * override operator new[]
     */
    void *operator new[](size_t size)
    {
        return new_p(size);
    }
    /**
     * override operator delete[]
     */
    void operator delete[](void *p)
    {
        free(p);
    }
private:
    static void *new_p(size_t size)
    {
        if (0 == size)  size = 1;
        void *p;
        while (0 == (p = ::malloc(size)))
        {
            //#if __cplusplus >= 201103L  // c++11    // this is not working on gcc 4.8
#if defined(__clang__) || (__GNUC__ > 4 || (__GNUC__ >= 4 && __GNUC_MINOR__ > 8))
            // If malloc fails and there is a new_handler,
            // call it to try free up memory.
            std::new_handler nh = std::get_new_handler();
            if (nh)
                nh();
            else
#endif
                throw std::bad_alloc();
        }
        return p;
    }
};  // overloaded_allocator

template <class T>
class ManagedType : public overloaded_allocator {
    T t;
public:
    ManagedType(const T& t)
        : t(t)
    {}
    typedef T value_type;
    operator T&() {
        return t;
    }
    operator const T&() const {
        return t;
    }
};  // ManagedType

TEST(TEST_CONCURRENT, TEST_deque)
{
    std::unique_ptr<ManagedType<int> > p(new ManagedType<int>(4655));
    ASSERT_TRUE(*p == 4655);
    // TODO:
    std::vector<ManagedType<int> > tmp;
    concurrent::deque<ManagedType<int> > q;
    // test push_back
    for (int i = 0; i < 20000; ++i)
    {
        q.push_back(i);
        tmp.push_back(i);
    }
    
    // test operator[]
    ASSERT_TRUE(tmp[333] == 333);
    ASSERT_TRUE(tmp[333] % 3 == 0);
    
    // NOTE: zexing
    // in this ASSERTION, it requires the following conversion
    // 1. concurrent::deque<ManagedType<int> >::reference (a.k.a std::allocator<ManagedType<int> >::reference) -> ManagedType<int>&
    // 2. ManagedType<int>& -> int&
    // in OSX (clang), both conversion 1 and conversion 2 works fine.
    // but it has compile error for the combined conversion. concurrent::deque<ManagedType> >::reference -> int&
    // Invalid operands to binary expression ('concurrent::deque<ManagedType<int>, std::__1::allocator<ManagedType<int> > >::reference' and 'int')
//    ASSERT_TRUE(q[333] == 333);
    
    int j = 0;
    for (int i = 0; i < 2000; i += 100)
    {
        j = static_cast<const ManagedType<int>& >(q[i]);
        ASSERT_TRUE(j % 100 == 0);
        q[i] = j * 2;
        ASSERT_TRUE(static_cast<const ManagedType<int>& >(q[i]) % 200 == 0);
    }
    const concurrent::deque<ManagedType<int> >& cq = q;
    for (int i = 0; i < 2000; i += 100)
    {
        j = static_cast<const ManagedType<int>& >(cq[i]);
        ASSERT_TRUE(j % 200 == 0);
    }
}

TEST(TEST_CONCURRENT, TEST_deque_concurrent)
{
    concurrent::deque<int> q;
    auto f1 = std::async(std::launch::async, [&](){
        for (int i = 0; i < 100; ++i) {
            // test - push_back
            q.push_back(i);
        }
    });
    auto f2 = std::async(std::launch::async, [&](){
        for (int i = 100; i < 200; ++i) {
#if __cplusplus >= 201103L
            // test - emplace_back
            q.emplace_back(i);
#else
            q.push_back(i);
#endif
        }
    });
    f1.get();
    f2.get();
    ASSERT_TRUE(q[0] == 0 || q[0] == 100);
    ASSERT_TRUE(q.size() == 200);
}

TEST(TEST_CONCURRENT, TEST_queue)
{
    // TODO
}

struct Tracked
{
private:
    int i;
    int set_count;
    mutable int get_count;
    mutable std::mutex m;
public:
    Tracked(int i)
        : i(i)
        , set_count(0)
        , get_count(0)
    {}
    void set(int v) {
        std::lock_guard<std::mutex> lk(m);
        ++set_count;
        i = v;
    }
    int get() const {
        std::lock_guard<std::mutex> lk(m);
        ++get_count;
        return i;
    }
    
    Tracked(const Tracked& t)
        : i(t.i)
        , set_count(t.set_count)
        , get_count(t.get_count)
    {}
    Tracked& operator=(const Tracked& t)
    {
        if (this == &t) return *this;
        std::scoped_lock<std::mutex, std::mutex> lk(m, t.m);
        i = t.i;
        set_count = t.set_count;
        get_count = t.get_count;
        return *this;
    }
#if __cplusplus >= 201103
    Tracked(Tracked&& t)
        : i(t.i)
        , set_count(t.set_count)
        , get_count(t.get_count)
    {}
    Tracked& operator=(Tracked&& t)
    {
        if (this == &t) return *this;
        std::scoped_lock<std::mutex, std::mutex> lk(m, t.m);
        i = t.i;
        set_count = t.set_count;
        get_count = t.get_count;
        return *this;
    }
#endif  // >= C++11
};

TEST(TEST_CONCURRENT, TEST_utility)
{
    // default constructor
    concurrent::atomic<int> i;
    // move assignment for value_type
    i = 10;
    // type cast to value_type
    std::cout << "i=" << i << std::endl;
    int j = i;
    ASSERT_EQ(j, 10);
    
    concurrent::atomic<Tracked> t(2);
    ASSERT_EQ(2, static_cast<Tracked>(t).get());
}
