//
//  filesystem.cpp
//  CXX
//
//  Created by Shi, Zexing on 11/22/18.
//

#include <cxx/filesystem.hpp>

#if defined(_MSC_VER)   // Visual Studio
// VS2015 support filesystem. no need here
#else
#   include <dirent.h>                                  // DIR
#   include <libgen.h>                                  // basename
#   include <sys/param.h>                               // MAXPATHLEN, PATH_MAX
#   include <sys/stat.h>                                // stat
#   include <sys/time.h>                                // gettimeofday
#   include <sys/statvfs.h>                             // statvfs
#   include <time.h>                                    // clock_gettime (POSIX)
#   include <unistd.h>                                  // pathconf

#   include <errno.h>                                   // errno
#   include <stddef.h>                                  // offsetof
#   include <stdlib.h>                                  // malloc, realpath
#   include <string.h>                                  // strcmp

#   include <iostream>                                  // cerr

#   include <cxx/utility/string.h>                      // replace_all

namespace std
{
namespace filesystem
{

#pragma mark - _FilesystemClock
const bool _FilesystemClock::is_steady;

_FilesystemClock::time_point _FilesystemClock::now() noexcept {
    typedef chrono::duration<rep> secs;
//#if defined(_LIBCPP_USE_CLOCK_GETTIME) && defined(CLOCK_REALTIME)
    typedef chrono::duration<rep, nano> nsecs;
    struct timespec tp;
    if (0 != clock_gettime(CLOCK_REALTIME, &tp))
    {
        return time_point(secs(time(nullptr)));
    }
    return time_point(secs(tp.tv_sec) + chrono::duration_cast<duration>(nsecs(tp.tv_nsec)));
//#else
//    typedef chrono::duration<rep, micro> __microsecs;
//    timeval tv;
//    gettimeofday(&tv, 0);
//    return time_point(__secs(tv.tv_sec) + __microsecs(tv.tv_usec));
//#endif // _LIBCPP_USE_CLOCK_GETTIME && CLOCK_REALTIME
}

#pragma mark - directory_iterator::impl
struct directory_iterator::impl
{
public:
#if __cplusplus >= 201103L || defined(_MSC_VER)
    impl() = delete;
    impl(const impl&) = delete;
    impl& operator=(const impl&) = delete;
    impl(impl &&r)
        : h(r.h)
        , root(move(r.root))
        , entry(move(r.entry))
        , buffer(r.buffer)
    {
        r.h = nullptr;
        r.buffer = nullptr;
    }
#else
private:
    impl();
    impl(const impl&);
    impl& operator=(const impl&);
public:
#endif  // >= C++11
    impl(const path &p)
        : h(nullptr)
        , root(p)
        , buffer(nullptr)
    {
        h = ::opendir(root.c_str());
        {
            long name_max = ::pathconf(p.c_str(), _PC_NAME_MAX);
            if (name_max == -1)         /* Limit not defined, or error */
                name_max = 255;         /* Take a guess */
            size_t len = offsetof(struct dirent, d_name) + name_max + 1;
            buffer = static_cast<struct dirent*>(::malloc(len));
        }
        advance();
    }
    ~impl() noexcept
    {
        close();
    }
    
    bool advance()
    {
        if (!h) return false;
        while(true)
        {
//            struct dirent *entry = ::readdir(h);
            struct dirent *entry = nullptr;
            int r = ::readdir_r(h, buffer, &entry);
            if (nullptr == entry || 0 != r)
            {
                // reach end
                close();
                return false;
            }
            if (0 == strcmp(entry->d_name, ".")
                || 0 == strcmp(entry->d_name, ".."))
            {
                continue;
            }
            else
            {
                this->entry.assign(root / entry->d_name);
                return true;
            }
        }
    }
    void close()
    {
        if (h)
        {
            ::closedir(h);
            h = nullptr;
        }
        if (buffer)
        {
            ::free(buffer);
            buffer = nullptr;
        }
    }
    DIR* h; /// handle of the directory
    path root;  /// root path
    directory_entry entry;  /// current entry
    struct dirent *buffer;
};  // directory_iterator::impl

#pragma mark - directory_iterator
directory_iterator::directory_iterator(const path &pth, error_code*, directory_options/* = directory_options::none*/)
    : p(new impl(pth))
{
    if (nullptr == p->h)
    {
        p.reset();
    }
}

#if __cplusplus >= 201103L || defined(_MSC_VER)
#else
/*explicit */
directory_iterator::directory_iterator(const path& pth)
{
    initialize(pth);
}

void directory_iterator::initialize(const path& pth)
{
    p.reset(new impl(pth));
    if (nullptr == p->h)
    {
        p.reset();
    }
}
#endif  //

const directory_entry& directory_iterator::operator*() const
{
    return p->entry;
}

directory_iterator& directory_iterator::operator++()
{
    bool b = p->advance();
    if (!b) p.reset();
    return *this;
}

#pragma mark - path

#if __cplusplus >= 201103L || defined(_MSC_VER)
path::path(string_type&& s, format/* = format::auto_format*/) noexcept
    : pathname(move(s))
{
    util::replace_all(pathname, '\\', preferred_separator);
}
#endif  // >= C++11
    
path::path(const string_type& src, format/* = format::auto_format*/)
    : pathname(src)
{
    util::replace_all(pathname, '\\', preferred_separator);
}

path::path(const char *p)
    : pathname(p)
{
    util::replace_all(pathname, '\\', preferred_separator);
}

#if __cplusplus >= 201103L || defined(_MSC_VER)
path& path::assign(string_type&& s) noexcept
{
    pathname = move(s);
    util::replace_all(pathname, '\\', preferred_separator);
    return *this;
}
#endif  // >= C++11

path& path::operator+=(const string_type& s)
{
    if (s.empty())  return *this;
    if ((*s.begin() == '\\' || *s.begin() == preferred_separator) && (!pathname.empty() && *pathname.rbegin() == preferred_separator))
    {
#if __cplusplus >= 201103L || defined(_MSC_VER)
        pathname.pop_back();
#else
        pathname.erase(pathname.end() - 1);
#endif  // >= C++11
    }
    pathname += s;
    util::replace_all(pathname, '\\', preferred_separator);
    return *this;
}

path& path::operator+=(const value_type* s)
{
    pathname += std::string(s);
    return *this;
}

path& path::operator+=(value_type c)
{
    if ((c == '\\' || c == preferred_separator) && (!pathname.empty() && *pathname.rbegin() == preferred_separator))
    {
        return *this;
    }
    pathname += c;
    return *this;
}

int path::compare(const path& p) const noexcept
{
    if (pathname.size() == 0)   return p.pathname.size() == 0;
    if (p.pathname.size() == 0) return false;
    const char *p0 = pathname.c_str();
    const char *p1 = p.pathname.c_str();
    while (*p0 && *p1)
    {
        int res = *p0 - *p1;
        if (res)    return res;
        ++p0;
        ++p1;
    }
    if (*p0 == preferred_separator) ++p0;
    if (*p1 == preferred_separator) ++p1;
    return *p0 - *p1;
}

path path::parent_path() const
{
    if (pathname.empty())   return pathname;
    // reference : http://www.qnx.com/developers/docs/7.0.0/index.html#com.qnx.doc.neutrino.lib_ref/topic/d/dirname.html
    // https://linux.die.net/man/3/basename
    std::string copy = generic_string();
    char* ppath = dirname(&copy[0]);
    if (nullptr == ppath)
    {
        std::cerr << "dirname return nullptr!\n";
        return ".";
    }
    return path(ppath);
//    if (pathname == "/")    return pathname;
//    std::string parent_path = pathname;
//    if (preferred_separator == *parent_path.rbegin())
//    {
//#if __cplusplus >= 201103L || defined(_MSC_VER)
//        parent_path.pop_back();
//#else
//        parent_path.erase(parent_path.end() - 1);
//#endif  // >= C++11
//    }
//    std::string::size_type pos = parent_path.find_last_of(preferred_separator);
//    if (std::string::npos == pos)   return ".";
//    return parent_path.substr(0, pos);
}

path path::filename() const
{
    if (pathname.empty())   return pathname;
    // http://www.qnx.com/developers/docs/7.0.0/index.html#com.qnx.doc.neutrino.lib_ref/topic/b/basename.html
    // https://linux.die.net/man/3/basename
    std::string copy = generic_string();
    char* fn = basename(&copy[0]);
    if (nullptr == fn)
    {
        std::cerr << "basename return nullptr!\n";
        return pathname;
    }
    return path(fn);
}

#pragma mark - operations
static file_status status(const path& p, struct stat &st)
{
    if (::stat(p.c_str(), &st)!= 0)
    {
        if (errno == ENOENT || errno == ENOTDIR)
        {
            return file_status(file_type::not_found, perms::none);
        }
        return file_status(file_type::none);
    }
    if (S_ISDIR(st.st_mode))
        return file_status(file_type::directory, static_cast<perms>(st.st_mode) & perms(perms::mask));
    if (S_ISREG(st.st_mode))
        return file_status(file_type::regular, static_cast<perms>(st.st_mode) & perms(perms::mask));
    if (S_ISBLK(st.st_mode))
        return file_status(file_type::block, static_cast<perms>(st.st_mode) & perms(perms::mask));
    if (S_ISCHR(st.st_mode))
        return file_status(file_type::character, static_cast<perms>(st.st_mode) & perms(perms::mask));
    if (S_ISFIFO(st.st_mode))
        return file_status(file_type::fifo, static_cast<perms>(st.st_mode) & perms(perms::mask));
    if (S_ISSOCK(st.st_mode))
        return file_status(file_type::socket, static_cast<perms>(st.st_mode) & perms(perms::mask));
    return file_status(file_type::unknown);
}

path absolute(const path& p)
{
    // https://linux.die.net/man/3/realpath
#if defined(PATH_MAX)
    // in OSX, PATH_MAX = 1024, @sys/syslimits.h
    char buf[PATH_MAX] = {};
#else
    char buf[4096] = {};
#endif  // PATH_MAX
    char* r = realpath(p.c_str(), &buf[0]);
    if (nullptr != r)
    {
        return path(r);
    }
    return p;
}

bool create_directories(const path& p)
{
    file_status fs = status(p);
    
    // path exists
    if (fs.type() == file_type::directory)
    {
        return false;
    }
    
    path parent = p.parent_path();
    if (parent == p)
    {
        return create_directory(p);
    }
    else
    {
        create_directories(parent);
        return create_directory(p);
    }
}

bool create_directory(const path &p)
{
    int r = ::mkdir(p.c_str(), S_IRWXU|S_IRWXG|S_IRWXO);
    return 0 == r;
}

path current_path()
{
    char buf[1024];
    char* pth = getcwd(&buf[0], sizeof(buf));
    if (nullptr == pth)
    {
        if (ERANGE == errno)
        {
            pth = getcwd(nullptr, 0);
            if (nullptr == pth)
            {
                return path(".");
            }
            path cur(pth);
            free(pth);
            return cur;
        }
        else
        {
            std::cerr << "getcwd fails" << strerror(errno) << std::endl;
            return path(".");
        }
    }
    else
    {
        return path(pth);
    }
}

uintmax_t file_size(const path& p)
{
    struct stat st;
    file_status s = status(p, st);
    if (is_regular_file(s))
    {
        return st.st_size;
    }
    return 0;
}

bool is_empty(const path& p)
{
    struct stat st;
    file_status s = status(p, st);
    if (is_directory(s))
    {
        directory_iterator it(p);
        return it == directory_iterator();
    }
    else if (is_regular_file(s))
    {
        return st.st_size;
    }
    return false;
}

// https://en.cppreference.com/w/cpp/chrono/c/timespec_get
// https://linux.die.net/man/3/clock_gettime
file_time_type last_write_time(const path& p)
{
    struct stat st;
    file_status s = status(p, st);
    if (file_type::none == s.type()
        || file_type::not_found == s.type())
    {
        return file_time_type();
    }
    typedef chrono::duration<file_time_type::rep> secs;
#if defined(__APPLE__) && (!defined(_POSIX_C_SOURCE) || defined(_DARWIN_C_SOURCE))
    typedef chrono::duration<file_time_type::rep, nano> nsecs;
    return file_time_type(secs(st.st_mtimespec.tv_sec) + chrono::duration_cast<file_time_type::duration>(nsecs(st.st_mtimespec.tv_nsec)));
#else
    return file_time_type(secs(st.st_mtime));
#endif
}
    
bool remove(const path& p)
{
    file_status fs = status(p);
    if (file_type::none == fs.type()
        || file_type::not_found == fs.type())
    {
        return false;
    }
    return 0 == ::remove(p.c_str());
}

uintmax_t remove_all(const path &p)
{
    error_code tmp_ec;
    file_status st = status(p);
    file_type ft = st.type();
    if (file_type::none == ft || file_type::not_found == ft)  return 0;
    uintmax_t cnt = 1;
    if (file_type::directory == ft)
    {
        directory_iterator it(p);
        for (; it != end(it); ++it)
        {
            cnt += remove_all(*it);
        }
    }
    remove(p);
    return cnt;
}

#if __cplusplus >= 201103L || defined(_MSC_VER)
#else
    static inline void do_mult(uintmax_t& out, uintmax_t other, /*space_info &si, */const struct statvfs& svfs)
    {
        out = other * svfs.f_frsize;
        if (0 == other || out / other != svfs.f_frsize)
            out = static_cast<uintmax_t>(-1);
    }
#endif  // >= C++11

#if defined(__ANDROID_API__) && __ANDROID_API__ < 19
space_info space(const path& p)
{
    space_info si{0};
    return si;
}
#else
space_info space(const path& p)
{
    space_info si;
    struct statvfs m_svfs = {};
    // https://linux.die.net/man/2/statvfs
    if (::statvfs(p.c_str(), &m_svfs) == -1)
    {
        si.capacity = si.free = si.available = static_cast<uintmax_t>(-1);
        return si;
    }
#if __cplusplus >= 201103L || defined(_MSC_VER)
    // Multiply with overflow checking.
    auto do_mult = [&](uintmax_t& out, uintmax_t other) {
        out = other * m_svfs.f_frsize;
        if (other == 0 || out / other != m_svfs.f_frsize)
            out = static_cast<uintmax_t>(-1);
    };
    do_mult(si.capacity, m_svfs.f_blocks);
    do_mult(si.free, m_svfs.f_bfree);
    do_mult(si.available, m_svfs.f_bavail);
#else
    do_mult(si.capacity, m_svfs.f_blocks, m_svfs);
    do_mult(si.free, m_svfs.f_bfree, m_svfs);
    do_mult(si.available, m_svfs.f_bavail, m_svfs);
#endif  // >= C++11
    return si;
}
#endif  // __ANDROID_API__ < 19

file_status status(const path &p)
{
    struct stat st;
    return status(p, st);
}

}   // filesystem
}   // std

#endif
