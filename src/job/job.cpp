//
//  job.cpp
//  test_common
//
//  Created by Shi, Zexing on 8/7/18.
//

#include "job.h"

#include <errno.h>                              // EINVAL
#include <iostream>                             // cout

#include <cxx/utility.hpp>                      // move

namespace job {
    
JobExecutor & JobExecutor::instance() {
    static JobExecutor INSTANCE;
    return INSTANCE;
}

#if HAS_CXX11_FEATURE_RVALUE_REFERENCE
bool JobExecutor::register_job_handler(std::unique_ptr<JobHandler> &&job) {
    if (_jobs.count(job->key())) {
        std::cout << "WARNING: job[" << job->key() << "] already registered\n";
        return false;
    }
    _jobs[job->key()] = std::move(job);
    return true;
}
#else
bool JobExecutor::register_job_handler(std::unique_ptr<JobHandler> &job) {
    if (_jobs.count(job->key()))
    {
        std::cout << "WARNING: job[" << job->key() << "] already registered\n";
        return false;
    }
    _jobs[job->key()] = std::move(job);
    return true;
}
#endif  // HAS_CXX11_FEATURE_RVALUE_REFERENCE

std::pair<std::string, boost::program_options::variables_map> JobExecutor::parsing(int argc, char **argv) const {
    {
        // debug
        std::cout << "argument#:" << argc << std::endl;
        for (int i = 0; i < argc; ++i) {
            std::cout << "arg[" << i << "]=" << argv[i] << std::endl;
        }
    }
    // http://www.radmangames.com/programming/how-to-use-boost-program_options
    // Generic options
    boost::program_options::options_description generic("Generic options");
    generic.add_options()
    ("help,h", boost::program_options::value<std::string>()->value_name("option")->implicit_value(""), "Print help messages")
    ("version,v", "Show version")
    ;
    
    // command line option
    boost::program_options::options_description cmdline_options("Options");
    cmdline_options
    .add(generic)
//    .add_options()
//    (ARG_JOB_LEGACY, boost::program_options::value<std::vector<std::string>>(), "legacy style command line arguments")
    ;
    
    for (std::unordered_map<std::string, std::unique_ptr<JobHandler> >::const_iterator it = _jobs.begin(); it != _jobs.end(); ++it)
    {
        cmdline_options.add(it->second->option());
    }
    
//    boost::program_options::positional_options_description positionalOptions;
//    positionalOptions.add(ARG_JOB_LEGACY, -1);
    
    std::string key;
    boost::program_options::variables_map vm;
    
    std::cout << cmdline_options << std::endl;
    
    try {
        boost::program_options::parsed_options parsed = boost::program_options::command_line_parser(argc, argv)
        .options(cmdline_options)
//        .positional(positionalOptions)
        .allow_unregistered()
        .run();
        
        boost::program_options::store(parsed, vm);
        
//        {
//            // Debug information
//            std::vector<std::string> legacyArgs = boost::program_options::collect_unrecognized(parsed.options, boost::program_options::include_positional);
//            // for_each(legacyArgs.begin(), legacyArgs.end(), [](auto s){    // c++14
//            std::cout << "unrecognized:";
//            std::for_each(legacyArgs.begin(), legacyArgs.end(), [](const std::string &s) {
//                std::cout << s << ",";
//            });
//            std::cout << "\b " << std::endl;    // use ' ' to replace ','
//        }
        {
//            // debug information
//            for (auto p : vm) {
//                boost::program_options::variable_value pv = p.second;
//                std::cout << "key:" << p.first << std::endl;
//                if (p.first == ARG_JOB_LEGACY) {
//                    const std::vector<std::string>& args = pv.as<std::vector<std::string>>();
//                    for_each(args.begin(), args.end(), [](const std::string &s){
//                        std::cout << s << ",";
//                    });
//                    std::cout << std::endl;
//                } else {
//                    try {
//                        std::cout << pv.as<std::string>() << std::endl;
//                    } catch (std::bad_cast &e) {
//                        std::cerr << "args is not std::string:" << e.what() << std::endl;
//                    }
//                }
//            }
        }
        
        boost::program_options::notify(vm); // throws on error, so do after help in case
        // there are any problems
        
        /** --help option
         */
        std::cout << "vm.size()=" << vm.size() << std::endl;
        if (0 == vm.size()) {
            std::cout << cmdline_options << std::endl;
            // in compile.sh. this app will parse no parameter at beginning. then read params line by line. So, no need to exit here
            //            exit(0);
        }
        if (vm.count("help"))
        {
            const std::string& s = vm["help"].as<std::string>();
            if (_jobs.count(s)) {
                std::cout << _jobs.at(s)->options() << std::endl;
            }
            else {
                std::cout << cmdline_options << std::endl;
            }
            exit(0);
        }
        for (std::unordered_map<std::string, std::unique_ptr<JobHandler> >::const_iterator it = _jobs.begin(); it != _jobs.end(); ++it)
        {
            std::unordered_map<std::string, std::unique_ptr<JobHandler> >::const_reference p = *it;
            if (vm.count(p.first)) {
                key = p.first;
                boost::program_options::options_description sub_options = p.second->options();
                try {
                    boost::program_options::parsed_options parsed = boost::program_options::command_line_parser(argc, argv)
                    .options(sub_options)
                    .run();
                    vm.clear();
                    boost::program_options::store(parsed, vm);
                    
                    // NOTE: zexings, only notify will throw required_option exception
                    boost::program_options::notify(vm);
                } catch (const boost::program_options::error &e) {
                    std::cerr << e.what() << "\t @" << __FILE__ << " +" << __LINE__ << std::endl << std::endl;
                    std::cout << sub_options << std::endl;
                    exit(EINVAL);
                }
                
                break;
            }
        }
    } catch (const boost::program_options::error &e) {
        std::cerr << e.what() << std::endl << std::endl;
        std::cout << cmdline_options << std::endl;
        exit(EINVAL);
    }
    
    return {key,vm};
}
    
}   // job
