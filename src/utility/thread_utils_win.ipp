//
//  thread_utils_win.cpp
//  CXX
//
//  Created by Shi, Zexing on 9/13/18.
//

#if defined(_WIN32) || defined(WIN32)

#include <windows.h>

#include <string.h>

#include <iostream>

namespace util
{
namespace thread
{

static void log_error()
{
    DWORD ec = GetLastError();
    LPSTR buf = nullptr;
    size_t size = FormatMessageA(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS, nullptr, ec, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), (LPSTR)&buf, 0, nullptr);
    std::string msg(buf, size);
    LocalFree(buf);
    std::cerr << "error code:" << ec << ", msg:" << msg << std::endl;
}

__declspec( thread ) std::string thread_name;

std::string get_name()
{
    return thread_name;
}

std::string get_name(std::thread& t)
{
    if (GetCurrentThread() == t.native_handle())
    {
        return get_name();
    }
    return "";
}

bool set_name(const std::string& name)
{
    // NOTE: windows thread doesnot support naming thread. 
    // a work around for >=c++11 is using a thread local variable to store its name
    thread_name = name;
    return true;
}

bool set_name(std::thread& t, const std::string& name)
{
    if (GetCurrentThread() == t.native_handle())
    {
        return set_name(name);
    }
    return false;
}

// https://docs.microsoft.com/en-us/windows/desktop/api/processthreadsapi/nf-processthreadsapi-getcurrentthread
// ref: https://docs.microsoft.com/en-us/windows/desktop/api/processthreadsapi/nf-processthreadsapi-setthreadpriority
int get_priority(std::thread& t)
{
    return GetThreadPriority(t.native_handle());
}

static bool set_win_thread_priority(HANDLE h, int priority)
{
    BOOL r = SetThreadPriority(h, priority);
    if (0 != r) return true;
    return false;
}

bool set_priority(int priority)
{
    return set_win_thread_priority(GetCurrentThread(), priority);
}

bool set_priority(std::thread& t, int priority)
{
    return set_win_thread_priority(t.native_handle(), priority);
}

}}  // util::thread

#endif  // defined(_WIN32) || defined(WIN32)
