/**
 * Copyright (c) 2018 Shi, Zexing
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 * 
 * 
 * logger.cpp
 * Created by Shi, Zexing on 5/24/18.
 * 
 */
#include "utility/logging/logger.h"

#include <functional>               // hash
#include <unordered_map>

namespace std
{
    template <class E>
    struct hash
    {
        inline typename enable_if<is_enum<E>::value, size_t>::type operator()(const E& e) const
        {
            using type = typename underlying_type<E>::type;
            return hash<type>()(static_cast<type>(e));
        }
    };

}   // std

namespace zs {
namespace utils {
namespace logging {

const char* const log_level_name(log_level lvl)
{
    static std::unordered_map<log_level, const char* const> dict {
        {log_level::NONE, "NONE"},
        {log_level::VERBOSE, "VERBOSE"},
        {log_level::DEBUG, "DEBUG"},
        {log_level::INFO, "INFO"},
        {log_level::WARN, "WARN"},
        {log_level::ERROR, "ERROR"}
    };
    if (dict.count(lvl))    return dict[lvl];
    return "UNKNOWN";
}

loggers& loggers::instance()
{
    static loggers singleton;
    return singleton;
}

}}} // zs::utils::logging