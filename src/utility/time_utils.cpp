//
//  time_utils.cpp
//  cxx_test
//
//  Created by Shi, Zexing on 11/27/18.
//

#include <cxx/utility/time.h>

#include <cxx/chrono.hpp>                   // chrono

namespace util
{
double now()
{
    std::chrono::system_clock::time_point tp = std::chrono::system_clock::now();
    std::chrono::duration<double> d = tp.time_since_epoch();
    return d.count();
}
}
