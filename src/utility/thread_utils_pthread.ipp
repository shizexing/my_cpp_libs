//
//  thread_utils_pthread.cpp
//  CXX
//
//  Created by Shi, Zexing on 9/13/18.
//

#if defined(__linux__) || defined(__unix__) || defined(__QNX__)

#include <pthread.h>

#include <errno.h>
#include <string.h>

#include <iostream>
#include <sstream>

#include <cxx/thread.hpp>                       // thread
#include <cxx/unordered_map.hpp>                // unordered_map

namespace util
{
namespace thread
{

static void log_error(int ec)
{
    std::stringstream ss;
    ss << "error code:" << ec << ", msg:" << strerror(ec) << std::endl;
    std::cerr << ss.str();
}

static std::string get_pthread_name(pthread_t t)
{
    char buf[256] = {};
    int r = pthread_getname_np(t, &buf[0], sizeof(buf) / sizeof(char));
    if (0 == r) return std::string(&buf[0]);
    log_error(r);
    return "";
}

std::string get_name()
{
    return get_pthread_name(pthread_self());
}

std::string get_name(std::thread& t)
{
    return get_pthread_name(t.native_handle());
}

static bool set_pthread_name(pthread_t t, const std::string& name)
{
    int r = pthread_setname_np(t, name.c_str());
    if (0 == r) return true;
    log_error(r);
    return false;
}

bool set_name(const std::string& name)
{
    return set_pthread_name(pthread_self(), name);
}

bool set_name(std::thread& t, const std::string& name)
{
    return set_pthread_name(t.native_handle(), name);
}

// http://man7.org/linux/man-pages/man3/pthread_setschedparam.3.html
// http://man7.org/linux/man-pages/man3/pthread_setschedprio.3.html
// The sched_priority member in param must lie between the minimum and maximum values returned by sched_get_priority_max() and sched_get_priority_min().
int get_priority(std::thread& t)
{
    sched_param sch;
    int policy; 
    int r = pthread_getschedparam(t.native_handle(), &policy, &sch);
    if (0 == r) return sch.sched_priority;
    log_error(r);
    return -1;
}

static bool set_pthread_priority(pthread_t t, int priority)
{
    sched_param sch;
    int policy;
    int r = pthread_getschedparam(t, &policy, &sch);
    if (0 != r)
    {
        log_error(r);
        return false;
    }
    {
        int min = 0, max = 0;
        static std::unordered_map<int, std::pair<int, int> > ranges;
        if (0 == ranges.count(policy))
        {
            min = sched_get_priority_min(policy);
            max = sched_get_priority_max(policy);
            ranges[policy] = {min, max};
        }
        else
        {
            min = ranges[policy].first;
            max = ranges[policy].second;
        }
        if (priority < min || priority > max)
        {
            std::stringstream ss;
            ss << "invalid thread priority : " << priority << ". accepted range [" << min << " - " << max << "]\n";
            std::cerr << ss.str();
            return false;
        }
    }
    sch.sched_priority = priority;
    r = pthread_setschedparam(t, policy, &sch);
    if (0 == r) return true;
    log_error(r);
    return false;
}

bool set_priority(int priority)
{
    return set_pthread_priority(pthread_self(), priority);
}

bool set_priority(std::thread& t, int priority)
{
    return set_pthread_priority(t.native_handle(), priority);
}

}}  // util::thread

#endif  // defined(__linux__) || defined(__unix__) || defined(__QNX__)
