//
//  algorithm.cpp
//  CXX
//
//  Created by Shi, Zexing on 11/19/18.
//

#include <cxx/utility/algorithm.h>

#include <stddef.h>                         // size_t
#include <stdint.h>                         // uint8_t

#include "md5.h"

#if __cplusplus >= 201103L || defined(_MSC_VER)
// TODO: detect endianness at compile time
enum class endianness : uint32_t {
    E_LITTLE_ENDIAN   = 0x03020100,
    E_BIG_ENDIAN      = 0x00010203
};

/**
 * A constant array used to determine a program's target endianness. The
 * values
 *  in this array can be compared against the values placed in the
 * hl_endianness enumeration.
 */
static constexpr uint8_t endianValues[4] = {0, 1, 2, 3};
#endif  // >= C++11

namespace util
{

std::string md5(const std::string& s)
{
    MD5_CTX ctx;
    MD5Init(&ctx);
    std::basic_string<unsigned char> ustr((unsigned char*)s.c_str(), s.length());
    MD5Update(&ctx, &ustr[0], ustr.length());
    unsigned char digest[16];
    MD5Final(&ctx, digest);
    std::string ret;
    ret.resize(16 * 2);
    for (size_t i = 0; i < 16; ++i)
    {
        ret[i * 2] = "0123456789abcdef"[digest[i] >> 4];
        ret[i * 2 + 1] = "0123456789abcdef"[digest[i] & 0xF];
    }
    return ret;
}

}   // util
