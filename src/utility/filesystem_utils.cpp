//
//  filesystem_utils.cpp
//  CXX
//
//  Created by Shi, Zexing on 11/28/18.
//

#include <cxx/utility/filesystem.h>

#include <sys/stat.h>                                   // stat

namespace util
{
namespace filesystem
{

std::filesystem::file_time_type last_access_time(const std::filesystem::path& p) noexcept
{
    namespace fs = std::filesystem;
    fs::file_status s = fs::status(p);
    if (fs::file_type::none == s.type()
        || fs::file_type::not_found == s.type())
    {
        return fs::file_time_type();
    }
    struct stat st;
    if (::stat(p.string().c_str(), &st) != 0)
    {
        return fs::file_time_type();
    }
    typedef std::chrono::duration<fs::file_time_type::rep> secs;
#if defined(__APPLE__) && (!defined(_POSIX_C_SOURCE) || defined(_DARWIN_C_SOURCE))
    typedef std::chrono::duration<fs::file_time_type::rep, std::nano> nsecs;
    return fs::file_time_type(secs(st.st_atimespec.tv_sec) + std::chrono::duration_cast<fs::file_time_type::duration>(nsecs(st.st_atimespec.tv_nsec)));
#else
    return fs::file_time_type(secs(st.st_atime));
#endif  // APPLE
}
    
}}  // util::filesystem
