//
//  string_utils.cpp
//  CXX
//
//  Created by Shi, Zexing on 10/22/18.
//

#include "cxx/utility/string.h"

#include <algorithm>                                // replace
#include <cctype>                                   // tolower
#include <sstream>                                  // stringstream

namespace util {
/// string equal ignore case
bool iequals(const std::string& a, const std::string& b)
{
    if (a.size() != b.size())   return false;
    for (std::string::const_iterator ai = a.begin(), bi = b.begin(); ai != a.end(); ++ai, ++bi)
    {
        if (std::tolower(*ai) != std::tolower(*bi))
        {
            return false;
        }
    }
    return true;
}
    
/// string ends with
bool ends_with(std::string const & value, std::string const & ending)
{
    if (ending.size() > value.size()) return false;
    return std::equal(ending.rbegin(), ending.rend(), value.rbegin());
}

std::string& replace_all(std::string& s, char from, char to)
{
    std::replace(s.begin(), s.end(), from, to);
    return s;
}

std::string& replace_all(std::string& str, const std::string &from, const std::string &to)
{
    std::string::size_type start_pos = 0;
    while((start_pos = str.find(from, start_pos)) != std::string::npos)
    {
        str.replace(start_pos, from.length(), to);
        start_pos += to.length(); // Handles case where 'to' is a substring of 'from'
    }
    return str;
}
    
std::vector<std::string> split(std::string const& s, const char d)
{
    std::stringstream ss(s);
    std::vector<std::string> ret;
    std::string tmp;
    while (std::getline(ss, tmp, d)) {
        ret.push_back(tmp);
    }
    return ret;
}
}   // util


