//
//  thread_utils_pthread.cpp
//  CXX
//
//  Created by Shi, Zexing on 9/13/18.
//

#if defined(ANDROID) || defined(__ANDROID__)

#include <pthread.h>

#include <errno.h>
#include <string.h>

#include <iostream>
#include <cxx/thread.hpp>                       // thread

namespace utils
{
namespace thread
{

static void log_error(int ec)
{
    std::cerr << "error code:" << ec << ", msg:" << strerror(ec) << std::endl;
}

thread_local std::string thread_name;

std::string get_name()
{
    return thread_name;
}

std::string get_name(std::thread& t)
{
    if (t.native_handle() == pthread_self())
    {
        return get_name();
    }
    std::cerr << "not supported!\n";
    return "";
}
/*
#if defined(__USE_GNU)
#if __ANDROID_API__ >= 26
int pthread_getname_np(pthread_t, char* _Nonnull, size_t) __INTRODUCED_IN(26);
#endif // __ANDROID_API__ >= 26
#endif
 */
bool set_name(const std::string& name)
{
    thread_name = name;
    return true;
}

bool set_name(std::thread& t, const std::string& name)
{
    if (t.native_handle() == pthread_self())
    {
        return set_name(name);
    }
    std::cerr << "not supported!\n";
    return false;
}

// http://man7.org/linux/man-pages/man3/pthread_setschedparam.3.html
// http://man7.org/linux/man-pages/man3/pthread_setschedprio.3.html
// The sched_priority member in param must lie between the minimum and maximum values returned by sched_get_priority_max() and sched_get_priority_min().
int get_priority(std::thread& t)
{
    sched_param sch;
    int policy; 
    int r = pthread_getschedparam(t.native_handle(), &policy, &sch);
    if (0 == r) return sch.sched_priority;
    log_error(r);
    return -1;
}

static bool set_pthread_priority(pthread_t t, int priority)
{
    sched_param sch;
    int policy;
    int r = pthread_getschedparam(t, &policy, &sch);
    if (0 != r)
    {
        log_error(r);
        return false;
    }
    sch.sched_priority = priority;
    r = pthread_setschedparam(t, policy, &sch);
    if (0 == r) return true;
    log_error(r);
    return false;
}

bool set_priority(int priority)
{
    return set_pthread_priority(pthread_self(), priority);
}

bool set_priority(std::thread& t, int priority)
{
    return set_pthread_priority(t.native_handle(), priority);
}

}}  // utils::thread

#endif  // defined(ANDROID) || defined(__ANDROID__)
