//
//  thread_utils_apple.cpp
//  CXX
//
//  Created by Shi, Zexing on 9/13/18.
//

#if defined(__APPLE__)

#include <pthread.h>
#include <sched.h>

#include <assert.h>
#include <errno.h>
#include <string.h>

#include <iostream>

namespace utils
{
namespace thread
{

static void log_error(int ec)
{
    std::cerr << "error code:" << ec << ", msg:" << strerror(ec) << std::endl;
}

static std::string get_pthread_name(pthread_t t)
{
    char buf[256] = {};
    int r = pthread_getname_np(t, &buf[0], sizeof(buf) / sizeof(char));
    if (0 == r) return std::string(&buf[0]);
    log_error(r);
    return "";
}

std::string get_name()
{
    return get_pthread_name(pthread_self());
}

std::string get_name(std::thread& t)
{
    return get_pthread_name(t.native_handle());
}

bool set_name(const std::string& name)
{
    assert(name.length() < 256);
    int r = pthread_setname_np(name.c_str());
    if (0 == r) return true;
    log_error(r);
    return false;
}


bool set_name(std::thread& t, const std::string& name)
{
    if (t.native_handle() == pthread_self())
    {
        return set_name(name);
    }
    else
    {
        std::cerr << "not supported on OSX for set thread name from another thread\n";
    }
    return false;
}

// https://developer.apple.com/library/archive/documentation/Cocoa/Conceptual/Multithreading/CreatingThreads/CreatingThreads.html
// https://developer.apple.com/library/archive/documentation/System/Conceptual/ManPages_iPhoneOS/man3/pthread_setschedparam.3.html#//apple_ref/doc/man/3/pthread_setschedparam
// The sched_priority member in param must lie between the minimum and maximum values returned by sched_get_priority_max() and sched_get_priority_min().
// https://developer.apple.com/library/archive/documentation/System/Conceptual/ManPages_iPhoneOS/man3/pthread.3.html#//apple_ref/doc/man/3/pthread
// https://developer.apple.com/library/archive/documentation/Darwin/Conceptual/KernelProgramming/scheduler/scheduler.html
int get_priority(std::thread& t)
{
    sched_param sch;
    int policy; 
    int r = pthread_getschedparam(t.native_handle(), &policy, &sch);
    if (0 == r) return sch.sched_priority;
    log_error(r);
    return -1;
}

static bool set_pthread_priority(pthread_t t, int priority)
{
    sched_param sch;
    int policy;
    int r = pthread_getschedparam(t, &policy, &sch);
    if (0 != r)
    {
        log_error(r);
        return false;
    }
    // memset(&sch, 0, sizeof(sched_param));
    sch.sched_priority = priority;
    r = pthread_setschedparam(t, policy, &sch);
    if (0 == r) return true;
    log_error(r);
    if (EINVAL == r && t != pthread_self())
    {
        std::cerr << "pthread_setschedparam cannot used for non current thread\n";
    }
    return false;
}

bool set_priority(int priority)
{
    return set_pthread_priority(pthread_self(), priority);
}

bool set_priority(std::thread& t, int priority)
{
    return set_pthread_priority(t.native_handle(), priority);
}

}}  // utils::thread

#endif  // defined(__APPLE__)
