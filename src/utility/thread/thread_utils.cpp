//
//  thread.cpp
//  CXX
//
//  Created by Shi, Zexing on 9/13/18.
//

#include <utility/thread.h>

#if defined(ANDROID) || defined(__ANDROID__)
#   include "thread_utils_android.ipp"
#elif defined(__APPLE__)
// OSX & iOS
#   include "thread_utils_apple.ipp"
#elif defined(__QNX__)
// QNX
#   include "thread_utils_pthread.ipp"
#elif defined(__linux__)
#   include "thread_utils_pthread.ipp"
#elif defined(__unix__)
#   include "thread_utils_pthread.ipp"
#elif defined(WIN32) || defined(_WIN32)
#   include "thread_utils_win.ipp"
#else
#pragma message("not implemented")
#   include "thread_utils_dummy.ipp"
#endif
