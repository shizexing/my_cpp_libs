/**
 * Copyright (c) 2018 Shi, Zexing
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 * 
 * interpose_io.cpp
 * Created by Shi, Zexing on 07/02/18.
 * 
 */
#include <stdio.h>                          // fopen

#if defined(__APPLE__)
/// compile : clang -dynamiclib interpose_io.cpp -o libzs_io.dylib                                                      // works
/// run     : DYLD_PRINT_LIBRARIES=1 DYLD_INSERT_LIBRARIES=./libzs_io.dylib DYLD_FORCE_FLAT_NAMESPACE=1 ${APP}          // works

#include <string>

#include "mach-o/dyld-interposing.h"        // DYLD_INTERPOSE
#include "utility/profile/stacktrace.h"       // stacktrace

// $ man -S 2 open

/// this works for both fopen and fstream
FILE *interpose_fopen(const char *filename, const char *mode)
{
    static const char* const tag = "[IOI]";
    std::string st = zs::utils::profile::stacktrace(2);
    static int cnt = 0;
    // printf("%sopen file [%s] : %s\n", tag, mode, filename);
    // printf("%s@\n", tag);
    // printf("%s", st.c_str());
    // printf("%s#\n", tag);
    FILE* ret = fopen(filename, mode);
    printf("%s%7d open file [%s] (%s): %s\n%s@\n%s%s#\n", tag, ++cnt, mode, (nullptr == ret ? "f" : "t"), filename, tag, st.c_str(), tag);
    return ret;
}

DYLD_INTERPOSE(interpose_fopen, fopen);

#elif defined(__linux__)
// usage: $ strace -o logfile -eopen ${APP}
#pragma message("io interpose doesnot support for your platform yet!")
#else
#pragma message("io interpose doesnot support for your platform yet!")
#endif  // __APPLE__