/**
 * Copyright (c) 2018 Shi, Zexing
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 * 
 * interpose_memory.cpp
 * Created by Shi, Zexing on 07/01/18.
 * 
 */
// #include "utils/macros.h"

#ifdef __APPLE__
/// compile : clang -dynamiclib interpose_memory.cpp -o libzs_memory.dylib                                                      // works
///     or  : clang -dynamiclib -undefined suppress -flat_namespace -std=c99 -fPIC interpose_memory.cpp -o libzs_memory.dylib   // works
/// run     : DYLD_PRINT_LIBRARIES=1 DYLD_INSERT_LIBRARIES=./libzs_memory.dylib DYLD_FORCE_FLAT_NAMESPACE=1 ${APP}          // works
/// 
/// @ref: https://opensource.apple.com/source/dyld/dyld-210.2.3/include/mach-o/dyld-interposing.h
/// https://opensource.apple.com/source/dyld/dyld-519.2.2/include/mach-o/dyld-interposing.h.auto.html
/// *** actually, APPLE has libgmalloc.dylib for this purpose.
/// @ret: https://developer.apple.com/library/archive/documentation/Performance/Conceptual/ManagingMemory/Articles/MallocDebug.html
/// DYLD_INSERT_LIBRARIES=/usr/lib/libgmalloc.dylib MallocPreScribble=1 ${APP}

// #define _GNU_SOURCE // we need this before dlfcn.h to define RTLD_NEXT
// #include <dlfcn.h>
#include <malloc/malloc.h>                      // malloc_printf
#include <pthread.h>                            // pthread_key_create
// #include <unistd.h>                             // STDOUT_FILENO

// #include <stdio.h>
#include <stdlib.h>                             // malloc + size_t

#include <string>                               // std::string

#include "mach-o/dyld-interposing.h"            // DYLD_INTERPOSE
#include "utility/profile/stacktrace.h"           // stacktrace

// #define DYLD_INTERPOSE(_replacment,_replacee) \
//     __attribute__((used)) static struct{ const void* replacment; const void* replacee; } _interpose_##_replacee \
// __attribute__ ((section ("__DATA,__interpose"))) = { (const void*)(unsigned long)&_replacment, (const void*)(unsigned long)&_replacee };

// someone said we should declare trace_malloc without mangle, but I test its OK to has mangle
// #if defined(__cplusplus)
// extern "C" {
// #endif  // c++

// void* trace_malloc(size_t size);

// #if defined(__cplusplus)
// }
// #endif  // c++

pthread_key_t key;
static int key_ret = pthread_key_create(&key, nullptr);

// @ref: http://toves.freeshell.org/interpose/
// e.g. https://gist.github.com/ytlvy/db42fdd7b2a6b5f24862
// void* trace_malloc(size_t size) //would be nice if I didn't have to rename my function..
// {
    // ERROR: zexings, thread_local cause Segmentation fault. use pthread_key_t instead
//     // // thread_local static bool traced = false;
//     static bool traced = false;
//     // // thread_local bool traced = false;
//     if (traced)
//     {
//         return malloc(size);
//     }
//     traced = true;
//     {
//     // NOTE: printf will call malloc (Need to prove), and cause segmentation fault issue
//     // printf("Allocated: %zu\n", size);
//     }
//     {
//     // write - OK
//     // char buf[128];
//     // sprintf(buf, "allocate: %zu\n", size);
//     // write(STDOUT_FILENO, &buf[0], strlen(buf));
//     }
//     void* ret = malloc(size);    // call before DYLD_INTERPOSE, original malloc will be invoked
//     {
//     // malloc_printf - 
//     // No allocation is performed during execution of this function;
//     // Only understands usual %p %d %s formats, and %y that expresses a number of bytes (5b,10KB,1MB...)

//     // https://opensource.apple.com/source/libmalloc/libmalloc-140.40.1/include/malloc/malloc.h.auto.html
//     malloc_printf("alloc: %p %y\n", ret, size);
//     std::string bt = zs::utils::profile::stacktrace();  // segmentation fault if no traced guard, backtrace_symbols will call malloc
//     fwrite(bt.c_str(), 1, bt.length(), stdout);
//     }
//     traced = false;
//     return ret;
// }

void* trace_malloc(size_t size)
{
    if (key_ret)    // 0 == key_ret means success
    {
        return malloc(size);
    }
    if (nullptr != pthread_getspecific(key))
    {
        return malloc(size);
    }
    int r = pthread_setspecific(key, &key);
    if (r)
    {
        return malloc(size);
    }
    void* ret = malloc(size);    // call before DYLD_INTERPOSE, original malloc will be invoked
    {
    // malloc_printf - 
    // No allocation is performed during execution of this function;
    // Only understands usual %p %d %s formats, and %y that expresses a number of bytes (5b,10KB,1MB...)

    // https://opensource.apple.com/source/libmalloc/libmalloc-140.40.1/include/malloc/malloc.h.auto.html
    uint64_t tid;
    pthread_threadid_np(NULL, &tid);
    std::string bt = zs::utils::profile::stacktrace();  // segmentation fault if no traced guard, backtrace_symbols will call malloc
    malloc_printf("[%ld] alloc: %p %y\n", tid, ret, size);
    malloc_printf("%s\n", bt.c_str());
    // fwrite(bt.c_str(), 1, bt.length(), stdout);
    }
    pthread_setspecific(key, nullptr);
    return ret;
}

DYLD_INTERPOSE(trace_malloc, malloc);

#if 0       //this is one of my interpose version for OSX. but it stuck when calling dlsym(RTLD_NEXT, "malloc");
#if defined(__cplusplus)
extern "C" {
#endif  // c++

#define _GNU_SOURCE // we need this before dlfcn.h to define RTLD_NEXT
#include <dlfcn.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

static void* (*real_malloc)(size_t)=NULL;
static void mtrace_init(void)
{
    write(STDOUT_FILENO, "mtrace_init\n", strlen("mtrace_init\n"));
    /// looks like, stuck here
    real_malloc = dlsym(RTLD_NEXT, "malloc");
    /// /usr/lib/system/libsystem_malloc.dylib
    // void* handle = dlopen("/usr/lib/system/libsystem_malloc.dylib", RTLD_LAZY);
    // if (!handle)
    // {
    //     write(STDOUT_FILENO, "dlopen failed", strlen("dlopen failed") + 1);
    //     exit(1);
    // }
    // real_malloc = dlsym(handle, "malloc");
    write(STDOUT_FILENO, "dlsym malloc\n", strlen("dlsym malloc\n"));
    char buf[128];
    if (NULL == real_malloc) {
        // exit(1);
        sprintf(buf, "Error in `dlsym`: %s\n", dlerror());
        write(STDOUT_FILENO, &buf[0], strlen(&buf[0]) + 1);
        // fprintf(stderr, "Error in `dlsym`: %s\n", dlerror());
    } else {
        sprintf(buf, "malloc addr:%p\n", real_malloc);
        write(STDOUT_FILENO, &buf[0], strlen(&buf[0]));
        // fprintf(stderr, "malloc addr:%p\n", real_malloc);
    }
}

static volatile int malloc_hook_active = 1;
void *malloc(size_t size)
{
    write(STDOUT_FILENO, "@malloc\n", strlen("@malloc\n"));
    if(real_malloc==NULL) {
        malloc_hook_active = 0;
        mtrace_init();
        malloc_hook_active = 1;
    }
    void *p = NULL;
    // fprintf(stderr, "\nmalloc(%ld) = ", size);
    p = real_malloc(size);
    // fprintf(stderr, "%p\n", p);
    if (malloc_hook_active) {
        malloc_hook_active = 0;
        fprintf(stderr, "\nmalloc(%ld) = ", size);
        fprintf(stderr, "%p\n", p);
        // stacktrace(STDERR_FILENO);
        malloc_hook_active = 1;
    }
    return p;
}

#if defined(__cplusplus)
}
#endif  // c++
#endif  // 0 @ __APPLE__
#elif defined(ANDROID)
#pragma message("interpose memory not implemented on Android!!!")
#elif defined(__QNX__)
#pragma message("interpose memory not implemented on QNX !!!")
#elif defined(__linux__)
/// linux has mtrace 
/// compile : gcc -shared src/utils/interpose/interpose_memory.cpp -o libzs_memory.so -ldl -rdynamic -std=c++11 -fPIC
/// run     : LD_PRELOAD=./libzs_memory.so ${APP}
#define _GNU_SOURCE
#include <stdio.h>
#include <dlfcn.h>

// stacktrace
#include <unistd.h>
#include <execinfo.h>

#include <string.h>

#if defined(__cplusplus)
extern "C" void *malloc(size_t size);   // linux needs this
#endif  // c++

#define MAX_STACK_TRACE_NUMBER 128

void stacktrace(int fd)
{
    void *callstack[MAX_STACK_TRACE_NUMBER];
    int nFrames = backtrace(callstack, MAX_STACK_TRACE_NUMBER);
    const int SKIP_STACK_NUM=2;
    // NOTE: 2 means skip 
    //  0 : stacktrace
    //  1 : malloc
    backtrace_symbols_fd(callstack+SKIP_STACK_NUM, nFrames-SKIP_STACK_NUM, fd);
}
// end stacktrace

static void* (*real_malloc)(size_t) = nullptr;

static void mtrace_init(void)
{
    write(STDOUT_FILENO, "@mtrace_init\n", strlen("@mtrace_init\n"));

    /* Writing: cosine = (void* (*)(size_t)) dlsym(RTLD_NEXT, "malloc");
       would seem more natural, but the C99 standard leaves
       casting from "void *" to a function pointer undefined.
       The assignment used below is the POSIX.1-2003 (Technical
       Corrigendum 1) workaround; see the Rationale for the
       POSIX specification of dlsym(). */
    *(void**)(&real_malloc) = dlsym(RTLD_NEXT, "malloc");
    if (nullptr == real_malloc) 
    {
        fprintf(stderr, "Error in `dlsym`: %s\n", dlerror());
    }
}

static int malloc_hook_active = 1;

void *malloc(size_t size)
{
    if(nullptr == real_malloc) 
    {
        mtrace_init();
    }
    void *p = nullptr;
    // fprintf(stderr, "\nmalloc(%ld) = ", size);
    p = real_malloc(size);
    // fprintf(stderr, "%p\n", p);
    if (malloc_hook_active) {
        malloc_hook_active = 0;
        fprintf(stderr, "\nmalloc(%ld) = ", size);
        fprintf(stderr, "%p\n", p);
        stacktrace(STDERR_FILENO);
        malloc_hook_active = 1;
    }
    return p;
}

#elif defined(_WIN32)
#pragma message("interpose memory not implmeneted on Windows!!!")
#endif
