# utilities for tracking memory

this utilities provide dynamic libraries used to tracing memory allocation / deallocation without recompile code
you can also recompile your code with this library if LD_PRELOAD / DYLD_INSERT_LIBRARIES does not working as expect.

## interpose_memory.cpp - intercept malloc/free

compile : clang -dynamiclib interpose_memory.cpp -o libzs_memory.dylib
use     : DYLD_PRINT_LIBRARIES=1 DYLD_INSERT_LIBRARIES=./libzs_memory.dylib DYLD_FORCE_FLAT_NAMESPACE=1 ${APP}

## malloc_hook.cc - gcc malloc hook intercept

TODO: test on linux first
