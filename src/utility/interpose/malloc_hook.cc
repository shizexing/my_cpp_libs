/* 
 * Prototypes for __malloc_hook, __free_hook (Only working on linux)
 * 
 * mac is not working
 */
// gcc malloc_hook.cc -shared -o malloc_hook.so
// clang failed due to no __malloc_hook ...

#if defined(__linux__)
#   include <malloc.h>

/* Prototypes for our hooks.  */
extern "C" 
// static
void my_init_hook (void);
static void *my_malloc_hook (size_t, const void *);
static void *(*old_malloc_hook) (size_t, const void *);
static void my_free_hook (void*, const void *);
static void (*old_free_hook) (void*, const void *);

// zexings: seems not working
///* Override initializing hook from the C library. */
//void (*__malloc_initialize_hook) (void) = my_init_hook;

extern "C" 
//static 
void
my_init_hook (void)
{
    old_malloc_hook = __malloc_hook;
    old_free_hook = __free_hook;
    __malloc_hook = my_malloc_hook;
    __free_hook = my_free_hook;
}

static void *
my_malloc_hook (size_t size, const void *caller)
{
    void *result;
    /* Restore all old hooks */
    __malloc_hook = old_malloc_hook;
    __free_hook = old_free_hook;
    /* Call recursively */
    result = malloc (size);
    /* Save underlying hooks */
    old_malloc_hook = __malloc_hook;
    old_free_hook = __free_hook;
    /* printf might call malloc, so protect it too. */
    printf ("malloc (%u) returns %p\n", (unsigned int) size, result);
    /* Restore our own hooks */
    __malloc_hook = my_malloc_hook;
    __free_hook = my_free_hook;
    return result;
}

static void
my_free_hook (void *ptr, const void *caller)
{
    /* Restore all old hooks */
    __malloc_hook = old_malloc_hook;
    __free_hook = old_free_hook;
    /* Call recursively */
    free (ptr);
    /* Save underlying hooks */
    old_malloc_hook = __malloc_hook;
    old_free_hook = __free_hook;
    /* printf might call free, so protect it too. */
    printf ("freed pointer %p\n", ptr);
    /* Restore our own hooks */
    __malloc_hook = my_malloc_hook;
    __free_hook = my_free_hook;
}

#endif  // __linux__
