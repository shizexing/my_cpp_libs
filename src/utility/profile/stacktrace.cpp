/**
 * Copyright (c) 2018 Shi, Zexing
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 * 
 * 
 * stacktrace.cpp
 * Created by Shi, Zexing on 05/16/2018
 */

#include "utility/profile/stacktrace.h"

#if defined(__ANDROID__)        // Android is linux
//#   include <libunwind.h>   // in android-ndk-r15c, fatal error: 'libunwind.h' file not found
// TODO: check whether we include the right path. glog has libunwind.h
// in android-ndk-r10d, fatal error: libunwind.h: No such file or directory
#   include <unwind.h>
#elif defined(__QNX__)          // QNX is UNIX
// TODO: qnx
#elif defined(__APPLE__) || defined(__unix__)
#   include <cxxabi.h>      // abi::__cxa_demangle
#   include <dlfcn.h>       // Dl_info
#   include <execinfo.h>    // backtrace
#   ifdef __APPLE__             // apple only
#       include <mach/mach.h>
#   endif   // __APPLE__
#elif defined(_MSC_VER)
//  TODO: windows
#endif  // __APPLE__ || __unix__

#include <iostream>
#include <sstream>

namespace zs {
namespace utils {
namespace profile {

std::string stacktrace(size_t ignore)
{
    std::ostringstream os;
    stacktrace(os, ignore+1);
    return os.str();
}

#if defined(__ANDROID__)
/// @ref: https://stackoverflow.com/questions/8115192/android-ndk-getting-the-backtrace
/// @ref: <a href="https://github.com/alexeikh/android-ndk-backtrace-test">backtrace source code</a>
/// @ref: https://github.com/google/glog/blob/master/src/stacktrace_libunwind-inl.h
/// @ref: http://grokbase.com/t/gg/android-ndk/13c8jb434s/backtrace-corkscrew-unwind-callstack-does-anything-work

struct BacktraceBuffer
{
    BacktraceBuffer()
        : _cnt(0)
    {
        memset(&_callstacks[0], 0, sizeof(_callstacks));
    }
    
    bool add_callstack(void* pc)
    {
        if (full()) return false;
        _callstacks[_cnt++] = pc;
        return true;
    }
    
    inline bool full() const
    {
        return MAX_STACK_TRACE_NUMBER == _cnt;
    }
        
    std::ostream& stacktrace(std::ostream& os, size_t ignore) const
    {
        char buf[1024] = {0};
        for (size_t i = ignore; i < _cnt; ++i)
        {
            const void* addr = _callstacks[i];
            Dl_info info;
            if (dladdr(addr, &info) && info.dli_sname)
            {
                int status;
                // https://gcc.gnu.org/onlinedocs/libstdc++/libstdc++-html-USERS-4.3/a01696.html
                char* demangled = abi::__cxa_demangle(info.dli_sname, NULL, 0, &status);
                int r = snprintf(buf, sizeof(buf)
                    , "%-3d %-*p %s + %zd\n"
                    , static_cast<int>(i - ignore)
                    , static_cast<int>(2 + sizeof(void*) * 2)
                    , _callstacks[i]
                    , status == 0 ? demangled : info.dli_sname
                    , (char *)_callstacks[i] - (char *)info.dli_saddr);
                if (r <= 0) {
                    os << "ERROR: " << sizeof(buf) << " is not enough to hold stacktrace\n";
                }
                free(demangled);
            }
            else
            {
                // TODO: use std::printf(os, fmt, ...)
                int r = snprintf(buf, sizeof(buf)
                    , "%-3d %-*p\n"
                    , static_cast<int>(i - ignore)
                    , static_cast<int>(2 + sizeof(void*) * 2)
                    , _callstacks[i]
                );
                if (r <= 0) {
                    os << "ERROR: " << sizeof(buf) << " is not enough to hold stacktrace\n";
                }
            }
            os << buf;
        }
    }   // std::ostream& stacktrace(std::ostream& os, size_t ignore)
    
    void* _callstacks[MAX_STACK_TRACE_NUMBER];    // = {0};
    size_t _cnt;
};  // BacktraceBuffer

static _Unwind_Reason_Code unwindCallback(struct _Unwind_Context* context, void* arg)
{
    BacktraceBuffer* buffer = static_cast<BacktraceBuffer*>(arg);
    uintptr_t pc = _Unwind_GetIP(context);
    if (pc && buffer)
    {
        if (buffer->full())
        {
            return _URC_END_OF_STACK;
        }
        else
        {
            buffer->add_callstack(reinterpret_cast<void*>(pc));
        }
    }
    return _URC_NO_REASON;
}

std::ostream& stacktrace(std::ostream& os, size_t ignore)
{
    BacktraceBuffer buffer;
    _Unwind_Backtrace(unwindCallback, &buffer);
    return buffer.stacktrace(os, ++ignore);
}
#elif defined(__unix__) || defined(__APPLE__)
static constexpr int MAX_STACK_TRACE_NUMBER=128;
std::ostream &stacktrace(std::ostream &os, size_t ignore)
{
    // reference : 
    //  https://gist.github.com/fmela/591333/c64f4eb86037bb237862a8283df70cdfc25f01d3
    //  http://man7.org/linux/man-pages/man3/backtrace.3.html
    //  https://developer.apple.com/legacy/library/documentation/Darwin/Reference/ManPages/man3/backtrace.3.html
    //
    //  TO-READ: 
    //  https://stackoverflow.com/questions/6254058/how-to-get-fullstacktrace-using-unwind-backtrace-on-sigsegv
    //  https://stackoverflow.com/questions/8115192/android-ndk-getting-the-backtrace/50027799#50027799
    //  https://github.com/alexeikh/android-ndk-backtrace-test
    //  
    void *callstack[MAX_STACK_TRACE_NUMBER] = {0};
    int nFrames = backtrace(callstack, MAX_STACK_TRACE_NUMBER);
    char **symbols = backtrace_symbols(callstack, nFrames);
    
    if (nullptr == symbols)
    {
        return os;
    }
    
    char buf[1024];
    // start from 1, skip stacktrace function
    ++ignore;
    for (int i = ignore; i < nFrames; ++i)
    {
        Dl_info info;
        if (dladdr(callstack[i], &info))
        {
            char *demangled = nullptr;
            int status;
            // https://gcc.gnu.org/onlinedocs/libstdc++/libstdc++-html-USERS-4.3/a01696.html
            // not working on windows
            demangled = abi::__cxa_demangle(info.dli_sname, NULL, 0, &status);
            snprintf(buf, sizeof(buf)
                , "%-3d %*p %s + %zd\n"
                , static_cast<int>(i - ignore)
                , static_cast<int>(2 + sizeof(void*) * 2)
                , callstack[i]
                , status == 0 ? demangled : info.dli_sname
                , (char *)callstack[i] - (char *)info.dli_saddr);
            // snprintf(buf, sizeof(buf), "%-3d %*0p %s + %zd\n",
                // i - ignore, 2 + sizeof(void*) * 2, callstack[i],
                // status == 0 ? demangled : info.dli_sname,
                // (char *)callstack[i] - (char *)info.dli_saddr);
            free(demangled);
            os << buf;
        }
        else
        {
            os << symbols[i] << std::endl;
//            snprintf(buf, sizeof(buf), "%-3d %*0p\n", i, 2 + sizeof(void*) * 2, callstack[i]);
        }
    }
    free(symbols);
    
    return os;
}

void stacktrace(int fd)
{
    void *callstack[MAX_STACK_TRACE_NUMBER];
    int nFrames = backtrace(callstack, MAX_STACK_TRACE_NUMBER);
    backtrace_symbols_fd(callstack, nFrames, fd);
}

#else
// QNX : http://www.qnx.com/developers/docs/7.0.0/index.html#com.qnx.doc.neutrino.technotes/topic/backtrace.html
#error "undefined stacktrace(std::ostream &os, size_t ignore"
#endif  // __unix__ || __APPLE__
}}} // zs::utils::profile
