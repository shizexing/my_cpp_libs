/**
 * query memory usage
 */

#include "utility/profile/memory.h"

#include <mach/mach.h>

#pragma mark - Mach
void report_memory(void) {
	struct mach_task_basic_info info;
	mach_msg_type_number_t size = MACH_TASK_BASIC_INFO_COUNT;
	kern_return_t kerr = task_info(mach_task_self(), MACH_TASK_BASIC_INFO, (task_info_t)&info, &size);
	if( kerr == KERN_SUCCESS ) {
		printf("Memory in use (in bytes): %llu", info.resident_size);
		printf("Memory in use (in MiB): %.2lf", ((double)info.resident_size / 1048576));
		printf("Virtual Memory in use (in bytes): %llu", info.virtual_size);
		printf("Virtual Memory in use (in MiB): %.2lf", ((double)info.virtual_size / 1048576));
	} else {
		printf("Error with task_info(): %s", mach_error_string(kerr));
	}
}
