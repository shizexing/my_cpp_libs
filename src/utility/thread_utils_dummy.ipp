//
//  thread_utils_dummy.cpp
//  CXX
//
//  Created by Shi, Zexing on 9/13/18.
//

#if !(defined(__APPLE__) || defined(__linux__) || defined(__unix__) || defined(ANDROID) || defined(__ANDROID__) || defined(__QNX__) || defined(_WIN32) || defined(WIN32))

namespace util
{
namespace thread
{

std::string get_name()
{
    return "";
}

std::string get_name(std::thread&)
{
    return "";
}

bool set_name(const std::string&)
{
    return false;
}

bool set_name(std::thread&, const std::string&)
{
    return false;
}

int get_priority(std::thread&)
{
    return -1;
}

bool set_priority(int)
{
    return false;
}

bool set_priority(std::thread&, int)
{
    return false;
}

}}  // util::thread

#endif  // !(defined(__APPLE__) || defined(__linux__) || defined(__unix__) || defined(ANDROID) || defined(__ANDROID__) || defined(__QNX__) || defined(_WIN32) || defined(WIN32))
