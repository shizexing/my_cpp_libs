#!/usr/bin/python
 # -*- coding: utf-8 -*-

import argparse
import os
import re

class bcolors:
    DEFAULT = '\033[0m'
    HEADER = '\033[95m'
    RED='\033[31m'
    OKBLUE = '\033[94m'
    GREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

class IOLogParser:
    def unique_files(self, filename, sort=False):
        ''' get all the opened files
        @param filename: log filename
        @param sort: True if we want to sort the file alphabetic
        '''
        # print 'log:', filename
        filenames = set()
        d = {}
        cnt = {}
        sd = {
            'initializeBase': 'ib',       # initialize base, 
            'initializeTask': 'it',
            'warmupTask'    : 'wm',
            'initialize [Done]' : 'nm'
        }
        status = 'initializeBase'
        ''' other status
        it: initialize task
        wm: warmup
        nm: normal
        '''
        with open(filename, 'r') as f:
            for ln in f:
                mo = re.search(r'^\[IOI\] *(\d+) open file \[(.*)\] \([tf]\): (.*)$', ln.strip())
                if mo:
                    idx = int(mo.groups()[0])
                    mode = mo.groups()[1]
                    fn = mo.groups()[2]
                    if 'w' in mode: continue
                    # if 'r' in mode: continue
                    cnt[fn] = cnt.get(fn, 0) + 1
                    if fn not in filenames:
                        filenames.add(fn)
                        d[idx] = [mode, fn, sd[status]]
                mo = re.search(r'^-* (.*) -*$', ln.strip())
                if mo:
                    status = mo.groups()[0]
        filelist = []
        for i in sorted(d.keys()):
            mode = d[i][0]
            fn = d[i][1]
            step = d[i][2]
            # e = os.path.exists(fn
            # print bcolors.DEFAULT if e else bcolors.RED, '%4d' % i, '[%4d]' % cnt[fn], mode, fn, bcolors.DEFAULT
            filelist.append([i, cnt[fn], mode, fn, step])
        return filelist


if __name__ == '__main__':
    p = argparse.ArgumentParser()
    p.add_argument('-f', '--filename', help = 'log filename')

    args = p.parse_args()
    if args.filename is None:
        p.print_help()
        exit(0)
    
    logparser = IOLogParser()
    fl = logparser.unique_files(args.filename)
    fs = set()
    print 'exist step rIdx  count  mode    filename'
    for info in fl:
        idx = info[0]
        cnt = info[1]
        mode = info[2]
        fn = info[3]
        step = info[4]

        finfo = ' [%s] %4d [%4d]   %s %s' % (step, idx, cnt, mode, fn)
        if os.path.exists(fn):
            print bcolors.DEFAULT, '[y]', finfo, bcolors.DEFAULT
        else:
            print bcolors.GREEN, '[n]', finfo, bcolors.DEFAULT
        fs.add(fn)
    
    print '-'*60
    for subdir, dirs, files in os.walk('./tiles'):
        for f in files:
            fn = os.path.join(subdir, f)
            if 'tiles/logs' in fn:  continue
            if fn not in fs:
                print fn
    