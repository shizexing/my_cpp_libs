//
//  cxx_features.hpp
//  c++ standard feature support
//  cxx
//
//  Created by Shi, Zexing on 6/20/18.
//

#ifndef cxx_features_h
#define cxx_features_h

#ifdef __QNXNTO__
#   include <sys/neutrino.h>   // _NTO_VERSION
#endif

/// @ref: <a href="https://msdn.microsoft.com/en-us/library/hh567368.aspx">MSVC: Support For C++11/14/17 Features (Modern C++)</a>
/// @ref: <a href="http://en.cppreference.com/w/cpp/compiler_support">compiler support</a>

/// delegating constructor
/// @ref: <a href="https://en.cppreference.com/w/cpp/language/initializer_list">delegating constructor</a>
/// @ref: <a href="http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2006/n1986.pdf">Delegating constructors</a>
#if __cplusplus >= 201103L || (defined(_MSC_VER) && _MSC_VER >= 1800)
#   define HAS_CXX11_FEATURE_DELEGATING_CONSTRUCTOR     1
#endif  // >= c++11 || VS2013

/// defaulted and deleted function
/// @ref: <a href="https://en.cppreference.com/w/cpp/language/function">funtion</a>
/// @ref: <a href="http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2007/n2346.htm">defaulted and deleted function</a>
#if __cplusplus >= 201103L || (defined(_MSC_VER) && _MSC_VER >= 1800)
#	define HAS_CXX11_FEATURE_DEFAULT_DELETE_FUNCTION	1
#endif	// >= c++11 || VS2013

/// inheriting constructors
/// @ref: <a href="https://en.cppreference.com/w/cpp/language/using_declaration">using declaration</a>
#if (__cplusplus >= 201103L && (!defined(_NTO_VERSION) || _NTO_VERSION >= 700)) || (defined(_MSC_VER) && _MSC_VER >= 1900)
#   define HAS_CXX11_FEATURE_INHERITING_CONSTRUCTOR     1
// GCC 4.8 start support this
// QNX-660 : error: 'stream::FileStream::FileStream' names constructor, using FileStream::FileStream
#endif  // >= c++11 || VS2015 || QNX-700

/// lambda
/// @ref: <a href="https://en.cppreference.com/w/cpp/language/lambda">lambda</a>
#if __cplusplus >=201103L || defined(_MSC_VER)
#   define HAS_CXX11_FEATURE_LAMBDA                     1
#elif defined(__has_feature)  // clang
#   if __has_feature(cxx_lambdas) || __has_extension(cxx_lambdas)
#       define HAS_CXX11_FEATURE_LAMBDA                 1
#   endif   // clang
#endif  // >= C++11

/// inline namespace
/// @ref: <a href="https://en.cppreference.com/w/cpp/language/namespace">namespace</a>
#if __cplusplus >= 201103L || defined(_MSC_VER)
#   define HAS_CXX11_FEATURE_INLINE_NAMESPACE           1
#endif  // >= c++11 || VS2015

/// rvalue reference
/// @ref: <a href="https://en.cppreference.com/w/cpp/language/reference">reference</a>
#if __cplusplus >= 201103L || defined(_MSC_VER)
#   define HAS_CXX11_FEATURE_RVALUE_REFERENCE           1
#endif  // >= c++11

/// variadic template
/// @ref: <a href="https://en.cppreference.com/w/cpp/language/parameter_pack">variadic template (a.k.a parameter pack)</a>
#if __cplusplus >= 201103L || (defined(_MSC_VER) && _MSC_VER >= 1800)
#   define HAS_CXX11_FEATURE_VARIADIC_TEMPLATE          1
#endif  // >= c++11 || VS2013

/// propagate_const
/// @ref: <a href="https://en.cppreference.com/w/cpp/experimental/propagate_const">propagate_const</a>
// NOTE: gcc6+ support this
// MSVS is not support this yet
#if __cplusplus >= 201402L && ((defined(__APPLE_CC__) && __clang_major__ >= 9) || (defined(__GNUC__) && __GNUC__ >= 6))
#   define HAS_CXX_EXPERIMENTAL_PROPAGATE_CONST 1
#endif  // AppleClang

#endif /* cxx_features_h */
