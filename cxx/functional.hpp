//
//  functional.hpp
//  cxx
//
//  Created by Shi, Zexing on 6/21/18.
//

#ifndef cxx_functional_h
#define cxx_functional_h

#if __cplusplus >= 201103L || defined(_MSC_VER)
#   include <functional>
#   define HASH_NAMESPACE   std
#else
#   include <boost/bind/bind.hpp>                   // bind
#   include <boost/function.hpp>                    // function
#   include <boost/functional/hash.hpp>             // hash
#   include <boost/ref.hpp>                         // ref
#   define USE_BOOST_HASH   1
#   define HASH_NAMESPACE   boost
namespace std
{
    using boost::bind;
    using boost::cref;
    using boost::function;
    using boost::hash;
    using boost::ref;
    namespace placeholders
    {
#ifndef BOOST_BIND_NO_PLACEHOLDERS
        using ::_1;
        using ::_2;
        using ::_3;
        using ::_4;
        using ::_5;
        using ::_6;
        using ::_7;
        using ::_8;
        using ::_9;
#endif
    }
}
#endif  // >= c++11

#endif /* cxx_functional_h */
