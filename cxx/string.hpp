//
//  string.hpp
//  CXX
//
//  Created by Shi, Zexing on 10/23/18.
//

#ifndef cxx_string_h
#define cxx_string_h

#include <string>

#if __cplusplus >= 201103L || defined(_MSC_VER)
#else
#include <sstream>
namespace std
{
template <class T>
string to_string(const T& v)
{
    stringstream ss;
    ss << v;
    return ss.str();
}

inline string to_string( int value )
{
    return to_string<int>(value);
}

inline string to_string( long value )
{
    return to_string<long>(value);
}

inline string to_string( long long value )
{
    return to_string<long long>(value);
}

inline string to_string( unsigned value )
{
    return to_string<unsigned>(value);
}

inline string to_string( unsigned long value )
{
    return to_string<unsigned long>(value);
}

inline string to_string( unsigned long long value )
{
    return to_string<unsigned long long>(value);
}

inline string to_string( float value )
{
    return to_string<float>(value);
}

inline string to_string( double value )
{
    return to_string<double>(value);
}

inline string to_string( long double value )
{
    return to_string<long double>(value);
}
    
}   // std
#endif  // >= C++11

#endif /* cxx_string_h */
