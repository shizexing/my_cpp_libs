//
//  memory.hpp
//  cxx
//
//  Created by Shi, Zexing on 6/20/18.
//

#ifndef cxx_memory_h
#define cxx_memory_h

#if __cplusplus >= 201103L || defined(_MSC_VER)
#   include <memory>
#else
#   include <stddef.h>                                      // ptrdiff_t
#   include <string.h>                                      // memcpy
#   include <boost/interprocess/smart_ptr/unique_ptr.hpp>   // unique_ptr
#   include <boost/smart_ptr/enable_shared_from_this.hpp>   // enable_shared_from_this
#   include <boost/smart_ptr/make_shared.hpp>               // make_shared
#   include <boost/smart_ptr/owner_less.hpp>                // owner_less
#   include <boost/smart_ptr/shared_ptr.hpp>                // shared_ptr
#   include <boost/smart_ptr/weak_ptr.hpp>                  // weak_ptr
#   include <boost/thread/detail/memory.hpp>                // default_delete

namespace std
{
    using boost::const_pointer_cast;
    using boost::default_delete;
    using boost::dynamic_pointer_cast;
    using boost::enable_shared_from_this;
    using boost::make_shared;
    using boost::owner_less;
    using boost::reinterpret_pointer_cast;
    using boost::shared_ptr;
    using boost::static_pointer_cast;
    using boost::weak_ptr;
    
    // boost introduce unique_ptr in move library : https://www.boost.org/doc/libs/1_57_0/doc/html/move/release_notes.html#move.release_notes.release_notes_boost_1_57_00
    // #include <boost/move/unique_ptr.hpp>
    // using boost::movelib::unique_ptr.
    //
    // there is no equivalent in boost 1.55.
    // here I use boost::interprocess::unique_ptr to simulate std::unique_ptr
    // 
    // NOTE: zexings, boost::interprocess::unique_ptr needs two template types. we cannot using it directly
    // using boost::interprocess::unique_ptr;    // <T, D>
    template <class T>
    class unique_ptr : public boost::interprocess::unique_ptr<T, default_delete<T> >
    {
    public:
        unique_ptr()
        {}
        
        explicit unique_ptr( T* p )
        : boost::interprocess::unique_ptr<T, default_delete<T> >(p)
        {}
    };
    
}   // namespace std
#endif  // >= c++11

#endif /* cxx_memory_h */
