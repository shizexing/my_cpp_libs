//
//  macros.hpp
//  cxx
//
//  Created by Shi, Zexing on 6/21/18.
//

#ifndef cxx_macros_h
#define cxx_macros_h

// https://www.boost.org/doc/libs/1_62_0/boost/current_function.hpp
// #if defined(__GNUC__)
// #   define __TN_FUNCTION__ __PRETTY_FUNCTION__
// #elif defined(__FUNCSIG__)
// #   define __TN_FUNCTION__ __FUNCSIG__
// #else
// #   define __TN_FUNCTION__ __FUNCTION__
// #endif
#if defined(__GNUC__)
#   define __FUNCTION__ __PRETTY_FUNCTION__
#elif defined(__FUNCSIG__)
#   define __FUNCTION__ __FUNCSIG__
#else
#endif

#define __TN_FUNCTION__             __FUNCTION__

/// @ref: <a href="https://msdn.microsoft.com/en-us/library/7e3a913x.aspx">stringing operator (#)</a>
/// @ref: <a href="https://msdn.microsoft.com/en-us/library/09dwwt6y.aspx">token-pasting operator (##)</a>
/// @ref: <a href="https://gcc.gnu.org/onlinedocs/gcc-3.0.2/cpp_3.html#SEC17">stringification</a>
#define VALUE_TO_STRING(x) #x
#define VALUE(x) VALUE_TO_STRING(x)
#define VAR_NAME_VALUE(var) #var "=" VALUE(var)

#endif /* cxx_macros_h */
