//
//  condition_variable.hpp
//  cxx
//
//  Created by Shi, Zexing on 8/4/18.
//

#ifndef cxx_condition_variable_h
#define cxx_condition_variable_h

#if __cplusplus >= 201103L || defined(_MSC_VER)
#   include <condition_variable>
#else
#   include <boost/thread/condition_variable.hpp>   // condition_variable, condition_variable_any
namespace std
{
    using boost::condition_variable;
    using boost::condition_variable_any;
    using boost::cv_status;
}   // namespace std
#endif  // >= c++11

#endif /* cxx_condition_variable_h */
