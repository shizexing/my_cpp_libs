//
//  mutex.hpp
//  cxx
//
//  Created by Shi, Zexing on 6/21/18.
//

#ifndef cxx_mutex_h
#define cxx_mutex_h

#if __cplusplus >= 201103L || defined(_MSC_VER)
#   include <mutex>
// NOTE: zexings, in android-ndk-r10d, there is no timed_mutex
// @ref: https://issuetracker.google.com/issues/36962905
#else
#   include <boost/thread/once.hpp>                 // call_once
#   include "boost/thread/mutex.hpp"                // mutex
#   include "boost/thread/locks.hpp"                // lock_guard
#   include "boost/thread/recursive_mutex.hpp"      // recursive_mutex.

namespace std
{
    using boost::adopt_lock;                        // adopt_lock_t instanceå
    using boost::adopt_lock_t;                      // adopt_lock_t
    using boost::call_once;                         // call_once function
    using boost::lock;                              // lock function
    using boost::lock_guard;                        // lock_guard
    using boost::mutex;                             // mutex
    using boost::once_flag;                         // once_flag
    using boost::recursive_mutex;                   // recursive_mutex
    using boost::recursive_timed_mutex;
    using boost::timed_mutex;                       // timed_mutex
    using boost::unique_lock;                       // unique_lock
}   // std
#endif  // >= c++11

/// <a href="https://en.cppreference.com/w/cpp/thread/scoped_lock">scoped_lock</a>
#if !(__cplusplus >= 201703L || (defined(_MSVC_LANG) && _MSVC_LANG >= 201703L))
#include "features.hpp"
namespace std
{
    // scoped_lock
//#   if 0
#   if HAS_CXX11_FEATURE_VARIADIC_TEMPLATE
template <class ..._Mutexes>
class scoped_lock;

template <>
class scoped_lock<> {
public:
    explicit scoped_lock() {}
    explicit scoped_lock(adopt_lock_t) {}
    ~scoped_lock() = default;
    scoped_lock(scoped_lock const&) = delete;
    scoped_lock& operator=(scoped_lock const&) = delete;
};  // scoped_lock #0

template <class _Mutex>
class scoped_lock<_Mutex> {
public:
    typedef _Mutex  mutex_type;
private:
    mutex_type& m;
public:
    explicit scoped_lock(mutex_type & m)
        : m(m)
    {
        m.lock();
    }
    
    explicit scoped_lock(adopt_lock_t, mutex_type& m)
        : m(m)
    {}

    ~scoped_lock()
    {
        m.unlock();
    }

    scoped_lock(scoped_lock const&) = delete;
    scoped_lock& operator=(scoped_lock const&) = delete;
};  // scoped_lock #1

template <class ...MArgs>
class scoped_lock
{
    static_assert(sizeof...(MArgs) > 1, "At least 2 lock types required");
    typedef tuple<MArgs&...> MutexTuple;

public:
    explicit scoped_lock(MArgs&... margs)
      : mt(margs...)
    {
        lock(margs...);
    }

    scoped_lock(adopt_lock_t, MArgs&... margs)
        : mt(margs...)
    {
    }

    ~scoped_lock()
    {
        unlock<sizeof...(MArgs)>();
    }

    scoped_lock(scoped_lock const&) = delete;
    scoped_lock& operator=(scoped_lock const&) = delete;

private:
    template <size_t Index>
    typename enable_if<Index == 0>::type
    unlock()
    {}
    
    template <size_t Index>
    typename enable_if<Index != 0>::type
    unlock()
    {
        unlock<Index - 1>();
        get<Index - 1>(mt).unlock();
    }
    
    MutexTuple mt;
};  // scoped_lock #2+
#   else
// NOTE: zexing, add scoped_lock for concurrent library.
//   currently, I only needs two mutexes for the following APIs
// T& operator=(const T&)
// T(const T&)
// void swap(T&)
// std::swap(T&, T&)
template <class MutexT1, class MutexT2>
class scoped_lock
{
public:
    explicit scoped_lock(MutexT1& m1, MutexT2& m2)
        : m1(m1)
        , m2(m2)
    {
        lock(m1, m2);
    }
    
    scoped_lock(adopt_lock_t, MutexT1& m1, MutexT2& m2)
        : m1(m1)
        , m2(m2)
    {}
    
    ~scoped_lock()
    {
        m1.unlock();
        m2.unlock();
    }
    
private:
    scoped_lock(scoped_lock const&);
    scoped_lock& operator=(scoped_lock const&);
    MutexT1& m1;
    MutexT2& m2;
};  // scoped_lock #2
#   endif   // HAS_CXX11_FEATURE_VARIADIC_TEMPLATE
}   // std
#   endif  // < c++17

#endif /* cxx_mutex_h */
