//
//  unordered_set.hpp
//  cxx
//
//  Created by Shi, Zexing on 6/20/18.
//

#ifndef cxx_unordered_set_h
#define cxx_unordered_set_h

#if __cplusplus >= 201103L || defined(_MSC_VER)
#   include <unordered_set>
#else
#   include <boost/unordered_set.hpp>  // unordered_set
namespace std
{
    using boost::unordered_set;
}   // namespace std
#endif  // >= c++11

#endif /* cxx_unordered_set_h */
