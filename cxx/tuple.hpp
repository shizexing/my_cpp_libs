//
//  tuple.hpp
//  CXX
//
//  Created by Shi, Zexing on 11/7/18.
//

#ifndef cxx_tuple_h
#define cxx_tuple_h

#if __cplusplus >= 201103L || defined(_MSC_VER)
#   include <tuple>
#else
#   include <boost/tuple/tuple.hpp>

namespace std
{
    using boost::get;
    using boost::make_tuple;
    using boost::tie;
    using boost::tuple;
}   // namespace std
#endif  // >= c++11

#endif /* cxx_tuple_h */
