//
//  unordered_map.h
//  The reason to add this cxx patch is because by default, foundtion use boost implementatino.
//  the cxx/xxx.hpp will try to use c++11/14/17/20 first, if it cannot , use boost instead.
//  
//  cxx
//
//  Created by Shi, Zexing on 6/20/18.
//

#ifndef cxx_unordered_map_h
#define cxx_unordered_map_h

#if __cplusplus >= 201103L || defined(_MSC_VER)
#   include <unordered_map>
#else
#   include <boost/unordered_map.hpp>  // unordered_map
namespace std
{
    using boost::unordered_map;
}   // namespace std
#endif  // >= c++11

#endif /* cxx_unordered_map_h */
