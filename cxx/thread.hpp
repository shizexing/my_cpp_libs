//
//  thread.hpp
//  cxx
//
//  Created by Shi, Zexing on 6/20/18.
//

#ifndef cxx_thread_h
#define cxx_thread_h

#if __cplusplus >= 201103L || defined(_MSC_VER)
#   include <thread>
#else
#   include <cxx/chrono.hpp>            // chrono
#   include <boost/thread/thread.hpp>   // this_thread, thread
namespace std
{
    namespace this_thread = boost::this_thread;
    using boost::thread;
}   // namespace std
#endif  // >= c++11

#endif /* cxx_thread_h */
