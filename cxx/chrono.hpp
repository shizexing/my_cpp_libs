//
//  chrono.hpp
//  cxx
//
//  Created by Shi, Zexing on 6/29/18.
//

#ifndef cxx_chrono_h
#define cxx_chrono_h

#if __cplusplus >= 201103L || defined(_MSC_VER)
#   include <chrono>
#else
#   include <cxx/ratio.hpp>                 // milli, etc...
#   include <boost/chrono/chrono.hpp>
namespace std
{
    namespace chrono = boost::chrono;
}
#endif  // >= c++11

#endif /* cxx_chrono_h */
