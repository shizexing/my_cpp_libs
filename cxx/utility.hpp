//
//  utility.hpp
//  cxx
//
//  Created by Shi, Zexing on 8/10/18.
//

#ifndef cxx_utility_h
#define cxx_utility_h

#if __cplusplus >= 201103L || defined(_MSC_VER)
#   include <utility>
#else
#   include <boost/move/utility.hpp>                // move
namespace std
{
    using boost::move;
}
#endif  // >= c++11

#endif /* cxx_utility_h */
