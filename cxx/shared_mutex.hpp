//
//  shared_mutex.hpp
//  cxx
//
//  Created by Shi, Zexing on 6/28/18.
//

#ifndef cxx_shared_mutex_h
#define cxx_shared_mutex_h

// https://msdn.microsoft.com/en-us/library/mt420524.aspx
#if __cplusplus >= 201402L || (defined(_MSC_VER) && _MSC_VER >= 1900)
#   include<shared_mutex>                       // shared_timed_mutex
#else
#   include <boost/thread/shared_mutex.hpp>     // shared_mutex
#   include <boost/thread/lock_types.hpp>       // shared_lock
namespace std
{
    typedef boost::shared_mutex shared_timed_mutex;
    using boost::shared_lock;
}   // std
#endif  // >= c++14 || VS2015

#endif /* cxx_shared_mutex_h */
