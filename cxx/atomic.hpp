//
//  atomic.hpp
//  cxx
//
//  Created by Shi, Zexing on 6/20/18.
//

#ifndef cxx_atomic_h
#define cxx_atomic_h

#if __cplusplus >= 201103L || defined(_MSC_VER)
#   include <atomic>
#else
#   include <boost/atomic.hpp>

namespace std
{
    using boost::atomic;
    using boost::atomic_bool;
}   // namespace std
#endif  // >= c++11

#endif /* cxx_atomic_h */
