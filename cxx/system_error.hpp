//
//  system_error.hpp
//  CXX
//
//  Created by Shi, Zexing on 11/26/18.
//

#ifndef cxx_system_error_h
#define cxx_system_error_h

#if __cplusplus >= 201103L || defined(_MSC_VER)
#   include <system_error>
#else
#   include <boost/system/system_error.hpp>

namespace std
{
    using namespace boost::system;
}   // std
#endif  // >= C++11

#endif /* cxx_system_error_h */
