//
//  optional.hpp
//  cxx
//
//  Created by Shi, Zexing on 6/29/18.
//

#ifndef cxx_optional_h
#define cxx_optional_h

#if __cplusplus >= 201703L
#   include<optional>
#else
#   include <boost/optional.hpp>
namespace std
{
    using boost::optional;
}
#endif  // >= c++17

#endif /* cxx_optional_h */
