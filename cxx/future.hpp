//
//  future.hpp
//  cxx
//
//  Created by Shi, Zexing on 6/29/18.
//

#ifndef cxx_future_h
#define cxx_future_h

#if __cplusplus >= 201103L || defined(_MSC_VER)
#   include <future>
#   define FUTURE   futre
#else
#   include <boost/thread/future.hpp>
namespace std
{
    using boost::async;

#   define FUTURE   boost::BOOST_THREAD_FUTURE
//    template <class R>
//    using future=boost::BOOST_THREAD_FUTURE<R>;
//    typedef boost::BOOST_THREAD_FUTURE<R> future<R>;
//    using boost::future;                        // future ? BOOST_THREAD_PROVIDES_FUTURE, BOOST_THREAD_VERSION>=3
    using boost::launch;
    using boost::packaged_task;
    using boost::promise;
}
#endif  // >= c++11

#endif /* cxx_future_h */
