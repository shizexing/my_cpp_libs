//
//  cxx_keywords.hpp
//  c++ standard compatible keywords
//
//  cxx
//
//  Created by Shi, Zexing on 6/20/18.
//

#ifndef cxx_keywords_h
#define cxx_keywords_h

/// constexpr
/// @ref: <a href="https://en.cppreference.com/w/cpp/language/constexpr">constexpr</a>
#if __cplusplus >= 201103L || (defined(_MSC_VER) && _MSC_VER >= 1900)
#   define CONSTEXPR constexpr
#else
// NOTE: const doesnot equal to constexpr to provide compile time constant
#   define CONSTEXPR const
#   define constexpr const
#endif  // >= c++11 || VS2015

/// final
/// @ref: <a href="https://en.cppreference.com/w/cpp/language/final">final</a>
/// @ref: <a href="https://github.com/boostorg/config/blob/boost-1.67.0/include/boost/config/compiler/gcc.hpp">BOOST_NO_CXX11_FINAL</a>
#if __cplusplus >= 201103L || defined(_MSC_VER)
#   define FINAL    final
#else
#   define FINAL        // deprecated if we can define final directly
//#   define final      // boost/multi_index/detail/header_holder.hpp has a function using final as name
#endif  // >= c++11
// #if __cplusplus < 201103L && !defined(_MSC_VER)
// #   define final
// #endif  // < c++11

/// noexcept
/// @ref: <a href="https://en.cppreference.com/w/cpp/language/noexcept_spec">noexcept</a>
/// @ref: <a href="https://github.com/boostorg/config/blob/boost-1.67.0/include/boost/config/detail/suffix.hpp">BOOST_NOEXCEPT</a>
/// @ref: <a href="https://msdn.microsoft.com/en-us/library/hh567368.aspx">MSVC: Support For C++11/14/17 Features (Modern C++)</a>
// https://stackoverflow.com/questions/31266275/no-c11-on-vs-2015-pro
#if __cplusplus >= 201103L || (defined(_MSC_VER) && _MSC_VER >= 1900)
#   define NOEXCEPT noexcept
#else
#   define NOEXCEPT
#   define noexcept
#endif  // >= c++11 || >= VS2015

/// nullptr
/// @ref: <a href="https://en.cppreference.com/w/cpp/language/nullptr">nullptr</a>
#if !(__cplusplus >= 201103L || defined(_MSC_VER))
#   define nullptr  NULL
#endif  // < c++11

/// override
/// @ref: <a href="https://en.cppreference.com/w/cpp/language/override">override</a>
#if __cplusplus >= 201103L || defined(_MSC_VER)
#   define OVERRIDE override
#else
#   define OVERRIDE
#   define override
#endif  // >= c++11
// #if __cplusplus < 201103L && !defined(_MSC_VER)
// #   define override
// #endif  // >= c++11

/// deprecated
/// @ref: <a href="http://en.cppreference.com/w/cpp/language/attributes">deprecated</a>
#if __cplusplus >= 201402L   // [[deprecated]] needs c++14 support
#   define DEPRECATED(reason) [[deprecated(reason)]]
#elif defined(_MSC_VER)
#   define DEPRECATED(reason) __declspec(deprecated(reason))
#elif defined(__clang__)
#   define DEPRECATED(reason) __attribute__((deprecated(reason)))
#elif defined(__GNUC__)
// gcc 4.4.7 doesnot support add deprecated reason
// https://gcc.gnu.org/onlinedocs/gcc-4.4.7/gcc/Function-Attributes.html#Function-Attributes : NO
// https://gcc.gnu.org/onlinedocs/gcc-4.5.4/gcc/Function-Attributes.html#Function-Attributes : OK
#   if __GNUC__ > 4 || (__GNUC__ == 4 && __GNUC_MINOR__ > 4)
#       define DEPRECATED(reason) __attribute__((deprecated(reason)))
#   else
#       define DEPRECATED(reason) __attribute__((deprecated))
#   endif   // gcc4.4+
#else
#   define DEPRECATED(reason)
#endif  // >= c++14

#endif /* cxx_keywords_h */
