//
//  algorithm.hpp
//  cxx
//
//  Created by Shi, Zexing on 7/18/18.
//

#ifndef cxx_algorithm_h
#define cxx_algorithm_h

#include <algorithm>
#if __cplusplus >= 201103L || defined(_MSC_VER)
#   include <functional>                        // not1
#else
namespace std
{
    template<class InputIt, class UnaryPredicate>
    InputIt find_if_not(InputIt first, InputIt last, UnaryPredicate q)
    {
        return std::find_if(first, last, std::not1(q));
    }
    
    template< class InputIt, class UnaryPredicate >
    bool any_of(InputIt first, InputIt last, UnaryPredicate p)
    {
        return std::find_if(first, last, p) != last;
    }
    
    template< class InputIt, class UnaryPredicate >
    bool none_of(InputIt first, InputIt last, UnaryPredicate p)
    {
        return std::find_if(first, last, p) == last;
    }
}
#endif  // >= c++11

#endif /* cxx_algorithm_h */
