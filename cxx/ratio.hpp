//
//  ratio.hpp
//  CXX
//
//  Created by Shi, Zexing on 11/5/18.
//

#ifndef cxx_ratio_h
#define cxx_ratio_h

#if __cplusplus >= 201103L || defined(_MSC_VER)
#   include <ratio>
#else
#   include <boost/ratio/ratio.hpp>
namespace std
{
    using boost::micro;
    using boost::milli;
    using boost::nano;
}
#endif  // >= c++11

#endif /* cxx_ratio_h */
