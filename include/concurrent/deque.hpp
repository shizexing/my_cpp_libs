//
//  deque.hpp
//  ALL_BUILD
//
//  Created by Shi, Zexing on 8/4/18.
//
//#error "not finished yet"

#ifndef deque_h
#define deque_h

#include <deque>                                // deque

#include <cxx/atomic.hpp>                       // atomic
#include <cxx/keywords.hpp>                     // NOEXCEPT
#include <cxx/memory.hpp>                       // shared_ptr
#include <cxx/mutex.hpp>                        // recursive_mutex

namespace concurrent {
/// similar to CopyOnWriteQueue in JAVA
// TODO: copy-on-write
template <class T, class Allocator = std::allocator<T> >
class deque
{
    // added by zexing
private:
    typedef std::deque<T, Allocator> container_type;
    typedef std::shared_ptr<container_type> container_ptr;
    typedef size_t ref_type;
    typedef std::shared_ptr<ref_type> ref_ptr;
    typedef std::mutex mutex_type;
    typedef std::shared_ptr<mutex_type> mutex_ptr;
    
public:
    // types:
    typedef T value_type;
    typedef Allocator allocator_type;
    
    struct reference {
    private:
        // make sure r is the first member variable, so that &reference == &r
        T& r;
        container_ptr p_queue;
    public:
        // TODO: copy constructor, assignment
        // TODO: default constructor ???
        // TODO: move constructor, assignment
        reference(T& r, container_ptr pq)
            : r(r)
            , p_queue(pq)
        {}
        // NOTE: add type-cast method
        operator T&()
        {
            return r;
        }
        operator const T&() const
        {
            return r;
        }
        reference operator=(const T& v)
        {
            r = v;
            return *this;
        }
        T* operator&() = delete;
    };  // reference
    typedef const reference const_reference;
//    struct const_reference {
//        const T& r;
//        container_ptr p_queue;
//        const_reference(const T& r, container_ptr pq)
//            : r(r)
//            , p_queue(pq)
//        {}
//        operator const T&() const
//        {
//            return r;
//        }
//        const T* operator&() = delete;
//    };  // const_reference
    
    struct iterator {
    public:
        typedef T* pointer;
        typedef typename container_type::difference_type difference_type;
        typedef T                  value_type;
        typedef std::random_access_iterator_tag  iterator_category;
        typedef T&                 reference;
        
    private:
        typename container_type::iterator it;
        container_ptr pq;
        
    public:
        iterator(typename container_type::iterator it, container_ptr pq)
            : it(it)
            , pq(pq)
        {}
        iterator(container_ptr pq)
            : it(pq->end())
        {}
        
    public:
        reference operator*() const
        {
            return *it;
        }
        pointer operator->() const
        {
            return it.operator->();
        }
        
        iterator& operator++()
        {
            ++it;
            return *this;
        }
        
        iterator operator++(int)
        {
            iterator ret = *this;
            ++(*this);
            return ret;
        }
        
        iterator& operator--()
        {
            --it;
            return *this;
        }
        
        iterator operator--(int)
        {
            iterator ret = *this;
            --(*this);
            return ret;
        }
        
        iterator& operator+=(difference_type n)
        {
            it += n;
            return *this;
        }
        
        iterator& operator-=(difference_type n)
        {
            return *this += -n;
        }
        
        iterator operator+(difference_type n) const
        {
            iterator ret(*this);
            ret += n;
            return ret;
        }
        
        iterator operator-(difference_type n) const
        {
            iterator ret(*this);
            ret -= n;
            return ret;
        }
        
        friend iterator operator+(difference_type n, const iterator& it)
        {
            return it + n;
        }
        
        friend difference_type operator-(const iterator& x, const iterator& y)
        {
            if (x != y)
            {
                return x.it - y.it;
            }
            return 0;
        }
        
        reference operator[](difference_type n) const
        {
            return *(*this + n);
        }
        
        // TODO: zexing, how to handle x.pq != y.pq (one is modified)
        friend bool operator==(const iterator& x, const iterator& y)
        {
            return x.it == y.it;
        }
        
        friend bool operator!=(const iterator& x, const iterator& y)
        {
            return !(x == y);
        }
        
        friend bool operator<(const iterator& x, const iterator& y)
        {
            return x.it < y.it;
        }
        
        friend
        bool operator>(const iterator& x, const iterator& y)
        {
            return y < x;
        }
        
        friend
        bool operator<=(const iterator& x, const iterator& y)
        {
            return !(y < x);
        }
        
        friend
        bool operator>=(const iterator& x, const iterator& y)
        {
            return !(x < y);
        }
        
    private:
    };  // iterator
    
    typedef const iterator const_iterator;
    
    typedef std::reverse_iterator<iterator>          reverse_iterator;
    typedef std::reverse_iterator<const_iterator>    const_reverse_iterator;
    typedef typename allocator_type::size_type       size_type;
    typedef typename allocator_type::difference_type difference_type;
    
    // TODO: zexings, we should define pointer by ourselves
//    typedef typename allocator_type::pointer         pointer;
//    typedef typename allocator_type::const_pointer   const_pointer;
    struct pointer {
        T* p;
        container_ptr p_queue;
        pointer(T* p, container_ptr pq)
            : p(p)
            , p_queue(pq)
        {}
        // TODO: copy constructor, assignment
        // TODO: default constructor ???
        // TODO: move constructor, assignment
        // NOTE: add type-cast method
        operator T*()
        {
            return p;
        }
        operator const T*() const
        {
            return p;
        }
//        pointer operator=(const T& v)
//        {
//            r = v;
//            return *this;
//        }
//        T* operator&() = delete;
    };  // pointer
    typedef const pointer const_pointer;
public:
    // construct/copy/destroy:
    deque()
        : p_queue(new container_type)
    {}
    explicit deque(const allocator_type& a)
        : p_queue(new container_type(a))
    {}
    explicit deque(size_type n)
        : p_queue(new container_type(n))
    {}
#if __cplusplus >= 201402L
    explicit deque(size_type n, const allocator_type& a) // C++14
        : p_queue(new container_type(n, a))
    {}
#endif  // >= C++14
    deque(size_type n, const value_type& v)
        : p_queue(new container_type(n, v))
    {}
    deque(size_type n, const value_type& v, const allocator_type& a)
        : p_queue(new container_type(n, v, a))
    {}
    template <class InputIterator>
    deque(InputIterator f, InputIterator l)
        : p_queue(new container_type(f, l))
    {}
    template <class InputIterator>
    deque(InputIterator f, InputIterator l, const allocator_type& a)
        : p_queue(new container_type(f, l, a))
    {}
    deque(const deque& c)
        : deque()
    {
        // call operator=
        *this = c;
    }
    deque(const deque& c, const allocator_type& a)
        : deque(a)
    {
        // call operator=
        *this = c;
    }
#if __cplusplus >= 201103L
    deque(deque&& c)
        : deque()
    {
        // call operator=
        *this = std::move(c);
    }
    deque(std::initializer_list<value_type> il, const Allocator& a = allocator_type())
        : p_queue(new container_type(il, a))
    {}
    deque(deque&& c, const allocator_type& a)
        : deque(a)
    {
        // call operator=
        *this = std::move(c);
    }
#endif  // >= C++11
//    ~deque();
    
    deque& operator=(const deque& c)
    {
        if (&c == this) return *this;
        std::scoped_lock<mutex_type, mutex_type> lk(m, c.m);
        // deep copy
        *p_queue = *(c.p_queue);
        return *this;
    }

#if __cplusplus >= 201103L
    deque& operator=(deque&& c)
    {
        if (&c == this) return *this;
        std::scoped_lock<mutex_type, mutex_type> lk(m, c.m);
        p_queue = std::move(c.p_queue);
        return *this;
    }
    deque& operator=(std::initializer_list<value_type> il)
    {
        std::lock_guard<mutex_type> lk(m);
        *p_queue = il;
        return *this;
    }
#endif  // >= C++11
    
    
    template <class InputIterator>
    void assign(InputIterator f, InputIterator l)
    {
        std::lock_guard<mutex_type> lk(m);
        if (use_count() > 1)
        {
            p_queue.reset(new container_type(f, l));
        }
        else
        {
            p_queue->assign(f, l);
        }
    }
    
    void assign(size_type n, const value_type& v)
    {
        std::lock_guard<mutex_type> lk(m);
        if (use_count() > 1)
        {
            p_queue.reset(new container_type(n, v));
        }
        else
        {
            p_queue->assign(n, v);
        }
    }
    
#if __cplusplus >= 201103L
    void assign(std::initializer_list<value_type> il)
    {
        std::lock_guard<mutex_type> lk(m);
        if (use_count() > 1)
        {
            p_queue.reset(new container_type(il));
        }
        else
        {
            p_queue->assign(il);
        }
    }
#endif  // >= C++11
    
    allocator_type get_allocator() const NOEXCEPT
    {
        std::lock_guard<mutex_type> lk(m);
        return p_queue->get_allocator();
    }
    
    // iterators:
    iterator       begin() NOEXCEPT
    {
        std::lock_guard<mutex_type> lk(m);
        return iterator(p_queue->begin(), p_queue);
    }
    const_iterator begin() const NOEXCEPT
    {
        std::lock_guard<mutex_type> lk(m);
        return const_iterator(p_queue->begin(), p_queue);
    }
    iterator       end() NOEXCEPT
    {
        std::lock_guard<mutex_type> lk(m);
        return iterator(p_queue->end(), p_queue);
    }
    const_iterator end() const NOEXCEPT
    {
        std::lock_guard<mutex_type> lk(m);
        return const_iterator(p_queue->end(), p_queue);
    }

    // TODO: zexings, need to test if this is working or not
    reverse_iterator       rbegin() NOEXCEPT
    {
        std::lock_guard<mutex_type> lk(m);
        return reverse_iterator(iterator(p_queue->rbegin(), p_queue));
    }
    const_reverse_iterator rbegin() const NOEXCEPT
    {
        std::lock_guard<mutex_type> lk(m);
        return const_reverse_iterator(const_iterator(p_queue->rbegin(), p_queue));
    }
    reverse_iterator       rend() NOEXCEPT
    {
        std::lock_guard<mutex_type> lk(m);
        return reverse_iterator(iterator(p_queue->rend(), p_queue));
    }
    const_reverse_iterator rend() const NOEXCEPT
    {
        std::lock_guard<mutex_type> lk(m);
        return const_reverse_iterator(const_iterator(p_queue->rend(), p_queue));
    }

    const_iterator         cbegin() const NOEXCEPT
    {
        std::lock_guard<mutex_type> lk(m);
        return const_iterator(p_queue->cbegin(), p_queue);
    }
    const_iterator         cend() const NOEXCEPT
    {
        std::lock_guard<mutex_type> lk(m);
        return const_iterator(p_queue->end(), p_queue);
    }
    const_reverse_iterator crbegin() const NOEXCEPT
    {
        std::lock_guard<mutex_type> lk(m);
        return const_reverse_iterator(const_iterator(p_queue->rbegin(), p_queue));
    }
    const_reverse_iterator crend() const NOEXCEPT
    {
        std::lock_guard<mutex_type> lk(m);
        return const_reverse_iterator(const_iterator(p_queue->rend(), p_queue));
    }
    
    // capacity:
    size_type size() const NOEXCEPT
    {
        std::lock_guard<mutex_type> lk(m);
        return p_queue->size();
    }
    size_type max_size() const NOEXCEPT
    {
        std::lock_guard<mutex_type> lk(m);
        return p_queue->max_size();
    }
    void resize(size_type n)
    {
        std::lock_guard<mutex_type> lk(m);
        if (n == p_queue->size())   return;
        check_and_clone();
        p_queue->resize(n);
    }
    void resize(size_type n, const value_type& v)
    {
        std::lock_guard<mutex_type> lk(m);
        if (n == p_queue->size())   return;
        check_and_clone();
        p_queue->resize(n, v);
    }
    void shrink_to_fit()
    {
        std::lock_guard<mutex_type> lk(m);
        check_and_clone();
        p_queue->shrink_to_fit();
    }
    bool empty() const NOEXCEPT
    {
        std::lock_guard<mutex_type> lk(m);
        p_queue->empty();
    }
    
    // ??? how to set deque[i] = T
    // element access:
    reference operator[](size_type i)
    {
        std::lock_guard<mutex_type> lk(m);
        return reference(p_queue->operator[](i), p_queue);
    }
    const_reference operator[](size_type i) const
    {
        std::lock_guard<mutex_type> lk(m);
        return const_reference(p_queue->operator[](i), p_queue);
    }
//    value_type operator[](size_type i) const
//    {
//        std::lock_guard<mutex_type> lk(m);
//        return p_queue->at(i);
//    }
    reference at(size_type i)
    {
        std::lock_guard<mutex_type> lk(m);
        return reference(p_queue->at[i], p_queue);
    }
    const_reference at(size_type i) const
    {
        std::lock_guard<mutex_type> lk(m);
        return const_reference(p_queue->at(i), p_queue);
    }
//    value_type at(size_type i) const
//    {
//        std::lock_guard<mutex_type> lk(m);
//        return p_queue->at(i);
//    }

    reference front()
    {
        std::lock_guard<mutex_type> lk(m);
        return reference(p_queue->front(), p_queue);
    }
    const_reference front() const
    {
        std::lock_guard<mutex_type> lk(m);
        return const_reference(p_queue->front(), p_queue);
    }
//    value_type front() const
//    {
//        std::lock_guard<mutex_type> lk(m);
//        return p_queue->front();
//    }
    
    /// ?? how to set back()
    reference back()
    {
        std::lock_guard<mutex_type> lk(m);
        return reference(p_queue->back(), p_queue);
    }
    const_reference back() const
    {
        std::lock_guard<mutex_type> lk(m);
        return const_reference(p_queue->back(), p_queue);
    }
//    value_type back() const
//    {
//        std::lock_guard<mutex_type> lk(m);
//        return p_queue->back();
//    }
    
    // modifiers:
    void push_front(const value_type& v)
    {
        std::lock_guard<mutex_type> lk(m);
        check_and_clone();
        p_queue->push_front(v);
    }
#if __cplusplus >= 201103L
    void push_front(value_type&& v)
    {
        std::lock_guard<mutex_type> lk(m);
        check_and_clone();
        p_queue->push_front(std::move(v));
    }
#endif  // >= C++11
    void push_back(const value_type& v)
    {
        std::lock_guard<mutex_type> lk(m);
        check_and_clone();
        p_queue->push_back(v);
    }
#if __cplusplus >= 201103L
    void push_back(value_type&& v)
    {
        std::lock_guard<mutex_type> lk(m);
        check_and_clone();
        p_queue->push_back(std::move(v));
    }
#   if __cplusplus < 201703L
    template <class... Args>
    void emplace_front(Args&&... args)  // reference in C++17
    {
        std::lock_guard<mutex_type> lk(m);
        check_and_clone();
        p_queue->emplace_front(std::forward<Args>(args)...);
    }
    template <class... Args>
    void emplace_back(Args&&... args)   // reference in C++17
    {
        std::lock_guard<mutex_type> lk(m);
        check_and_clone();
        p_queue->emplace_back(std::forward<Args>(args)...);
    }
#   else
    template <class... Args>
    reference emplace_front(Args&&... args)  // reference in C++17
    {
        std::lock_guard<mutex_type> lk(m);
        check_and_clone();
        return reference(p_queue->emplace_front(std::forward<Args>(args)...), p_queue);
    }
    template <class... Args>
    reference emplace_back(Args&&... args)   // reference in C++17
    {
        std::lock_guard<mutex_type> lk(m);
        check_and_clone();
        return reference(p_queue->emplace_back(std::forward<Args>(args)...), p_queue);
    }
#   endif   // < C++17
    template <class... Args>
    iterator emplace(const_iterator p, Args&&... args)
    {
        std::lock_guard<mutex_type> lk(m);
        check_and_clone();
        return iterator(p_queue->emplace(p, std::forward<Args>(args)...), p_queue);
    }
    iterator insert(const_iterator p, value_type&& v)
    {
        std::lock_guard<mutex_type> lk(m);
        check_and_clone();
        return iterator(p_queue->insert(p, std::move(v)), p_queue);
    }
    iterator insert(const_iterator p, std::initializer_list<value_type> il)
    {
        std::lock_guard<mutex_type> lk(m);
        check_and_clone();
        return iterator(p_queue->insert(p, il), p_queue);
    }
#endif  // >= C++11
    iterator insert(const_iterator p, const value_type& v)
    {
        std::lock_guard<mutex_type> lk(m);
        check_and_clone();
        return iterator(p_queue->insert(p, v), p_queue);
    }
    iterator insert(const_iterator p, size_type n, const value_type& v)
    {
        std::lock_guard<mutex_type> lk(m);
        check_and_clone();
        return iterator(p_queue->insert(p, n, v), p_queue);
    }
    template <class InputIterator>
    iterator insert(const_iterator p, InputIterator f, InputIterator l)
    {
        std::lock_guard<mutex_type> lk(m);
        check_and_clone();
        return iterator(p_queue->insert(p, f, l), p_queue);
    }
    void pop_front()
    {
        std::lock_guard<mutex_type> lk(m);
        check_and_clone();
        p_queue->pop_front();
    }
    void pop_back()
    {
        std::lock_guard<mutex_type> lk(m);
        check_and_clone();
        p_queue->pop_back();
    }
    iterator erase(const_iterator p)
    {
        std::lock_guard<mutex_type> lk(m);
        check_and_clone();
        return iterator(p_queue->erase(p), p_queue);
    }
    iterator erase(const_iterator f, const_iterator l)
    {
        std::lock_guard<mutex_type> lk(m);
        check_and_clone();
        return iterator(p_queue->erase(f, l), p_queue);
    }
    void swap(deque& c)
#if __cplusplus >= 201703L
    noexcept
#endif  // >= C++17
    {
        std::scoped_lock<mutex_type, mutex_type> lk(m, c.m);
        p_queue->swap(*c.p_queue);
    }
    void clear() NOEXCEPT
    {
        std::lock_guard<mutex_type> lk(m);
        p_queue->clear();
    }
    
    // additional API
    bool try_pop_front(value_type& v)
    {
        std::lock_guard<mutex_type> lk(m);
        if (p_queue->empty())   return false;
        v = p_queue->front();
        p_queue->pop_front();
        return true;
    }
    bool try_pop_back(value_type& v)
    {
        std::lock_guard<mutex_type> lk(m);
        if (p_queue->empty())   return false;
        v = p_queue->back();
        p_queue->pop_back();
        return true;
    }

    bool operator==(const deque& r) const
    {
        std::scoped_lock<mutex_type, mutex_type> lk(m, r.m);
        return *p_queue == *r.p_queue;
    }
    bool operator<(const deque& r) const
    {
        std::scoped_lock<mutex_type, mutex_type> lk(m, r.m);
        return *p_queue < *r.p_queue;
    }
    bool operator!=(const deque& r) const
    {
        std::scoped_lock<mutex_type, mutex_type> lk(m, r.m);
        return *p_queue != *r.p_queue;
    }
    bool operator>(const deque& r) const
    {
        std::scoped_lock<mutex_type, mutex_type> lk(m, r.m);
        return *p_queue > *r.p_queue;
    }
    bool operator>=(const deque& r) const
    {
        std::scoped_lock<mutex_type, mutex_type> lk(m, r.m);
        return *p_queue >= *r.p_queue;
    }
    bool operator<=(const deque& r) const
    {
        std::scoped_lock<mutex_type, mutex_type> lk(m, r.m);
        return *p_queue <= *r.p_queue;
    }
private:
    // no lock for internal API
    size_t use_count()
    {
        // NOTE: zexing, use_count may return invalid value in multi-core CPU
        // I need to find a new way to solve this problem
        return p_queue.use_count();
    }
    void check_and_clone()
    {
        if (use_count() > 1)
        {
            p_queue.reset(new container_type(*p_queue));
        }
    }
    
private:
    container_ptr p_queue;
    mutable mutex_type m;
    // NOTE: zexing, use std::shared_ptr::use_count() instead
//    ref_ptr ref_counter;
};  // deque
}   // concurrent

namespace std
{
    template <class T, class Allocator>
    bool operator==(const concurrent::deque<T,Allocator>& x, const concurrent::deque<T,Allocator>& y)
    {
        return x == y;
    }
    template <class T, class Allocator>
    bool operator< (const concurrent::deque<T,Allocator>& x, const concurrent::deque<T,Allocator>& y)
    {
        return x < y;
    }
    template <class T, class Allocator>
    bool operator!=(const concurrent::deque<T,Allocator>& x, const concurrent::deque<T,Allocator>& y)
    {
        return x != y;
    }
    template <class T, class Allocator>
    bool operator> (const concurrent::deque<T,Allocator>& x, const concurrent::deque<T,Allocator>& y)
    {
        return x > y;
    }
    template <class T, class Allocator>
    bool operator>=(const concurrent::deque<T,Allocator>& x, const concurrent::deque<T,Allocator>& y)
    {
        return x >= y;
    }
    template <class T, class Allocator>
    bool operator<=(const concurrent::deque<T,Allocator>& x, const concurrent::deque<T,Allocator>& y)
    {
        return x <= y;
    }
    
    // specialized algorithms:
    template <class T, class Allocator>
    void swap(concurrent::deque<T,Allocator>& x, concurrent::deque<T,Allocator>& y) NOEXCEPT
    {
        x.swap(y);
    }
}   // std
#endif /* deque_h */
