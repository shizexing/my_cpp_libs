//
//  queue.hpp
//  CXX
//
//  Created by Shi, Zexing on 10/23/18.
//

#ifndef queue_h
#define queue_h

#include <queue>
#include "deque.hpp"                // concurrent::deque

/*
 queue synopsis
 
 namespace std
 {
 
 template <class T, class Container = deque<T>>
 class queue
 {
 public:
 typedef Container                                container_type;
 typedef typename container_type::value_type      value_type;
 typedef typename container_type::reference       reference;
 typedef typename container_type::const_reference const_reference;
 typedef typename container_type::size_type       size_type;
 
 protected:
 container_type c;
 
 public:
 queue() = default;
 ~queue() = default;
 
 queue(const queue& q) = default;
 queue(queue&& q) = default;
 
 queue& operator=(const queue& q) = default;
 queue& operator=(queue&& q) = default;
 
 explicit queue(const container_type& c);
 explicit queue(container_type&& c)
 template <class Alloc>
 explicit queue(const Alloc& a);
 template <class Alloc>
 queue(const container_type& c, const Alloc& a);
 template <class Alloc>
 queue(container_type&& c, const Alloc& a);
 template <class Alloc>
 queue(const queue& q, const Alloc& a);
 template <class Alloc>
 queue(queue&& q, const Alloc& a);
 
 bool      empty() const;
 size_type size() const;
 
 reference       front();
 const_reference front() const;
 reference       back();
 const_reference back() const;
 
 void push(const value_type& v);
 void push(value_type&& v);
 template <class... Args> reference emplace(Args&&... args); // reference in C++17
 void pop();
 
 void swap(queue& q) noexcept(is_nothrow_swappable_v<Container>)
 };
 
 template <class T, class Container>
 bool operator==(const queue<T, Container>& x,const queue<T, Container>& y);
 
 template <class T, class Container>
 bool operator< (const queue<T, Container>& x,const queue<T, Container>& y);
 
 template <class T, class Container>
 bool operator!=(const queue<T, Container>& x,const queue<T, Container>& y);
 
 template <class T, class Container>
 bool operator> (const queue<T, Container>& x,const queue<T, Container>& y);
 
 template <class T, class Container>
 bool operator>=(const queue<T, Container>& x,const queue<T, Container>& y);
 
 template <class T, class Container>
 bool operator<=(const queue<T, Container>& x,const queue<T, Container>& y);
 
 template <class T, class Container>
 void swap(queue<T, Container>& x, queue<T, Container>& y)
 noexcept(noexcept(x.swap(y)));
 
 template <class T, class Container = vector<T>,
 class Compare = less<typename Container::value_type>>
 class priority_queue
 {
 public:
 typedef Container                                container_type;
 typedef typename container_type::value_type      value_type;
 typedef typename container_type::reference       reference;
 typedef typename container_type::const_reference const_reference;
 typedef typename container_type::size_type       size_type;
 
 protected:
 container_type c;
 Compare comp;
 
 public:
 priority_queue() = default;
 ~priority_queue() = default;
 
 priority_queue(const priority_queue& q) = default;
 priority_queue(priority_queue&& q) = default;
 
 priority_queue& operator=(const priority_queue& q) = default;
 priority_queue& operator=(priority_queue&& q) = default;
 
 explicit priority_queue(const Compare& comp);
 priority_queue(const Compare& comp, const container_type& c);
 explicit priority_queue(const Compare& comp, container_type&& c);
 template <class InputIterator>
 priority_queue(InputIterator first, InputIterator last,
 const Compare& comp = Compare());
 template <class InputIterator>
 priority_queue(InputIterator first, InputIterator last,
 const Compare& comp, const container_type& c);
 template <class InputIterator>
 priority_queue(InputIterator first, InputIterator last,
 const Compare& comp, container_type&& c);
 template <class Alloc>
 explicit priority_queue(const Alloc& a);
 template <class Alloc>
 priority_queue(const Compare& comp, const Alloc& a);
 template <class Alloc>
 priority_queue(const Compare& comp, const container_type& c,
 const Alloc& a);
 template <class Alloc>
 priority_queue(const Compare& comp, container_type&& c,
 const Alloc& a);
 template <class Alloc>
 priority_queue(const priority_queue& q, const Alloc& a);
 template <class Alloc>
 priority_queue(priority_queue&& q, const Alloc& a);
 
 bool            empty() const;
 size_type       size() const;
 const_reference top() const;
 
 void push(const value_type& v);
 void push(value_type&& v);
 template <class... Args> void emplace(Args&&... args);
 void pop();
 
 void swap(priority_queue& q)
 noexcept(is_nothrow_swappable_v<Container> &&
 is_nothrow_swappable_v<Comp>)
 };
 
 template <class T, class Container, class Compare>
 void swap(priority_queue<T, Container, Compare>& x,
 priority_queue<T, Container, Compare>& y)
 noexcept(noexcept(x.swap(y)));
 
 }  // std
 
 */

namespace concurrent
{
template <class T/*, class Container = deque<T>*/>
class queue
{
public:
    typedef deque<T>                                container_type;
    typedef typename container_type::value_type      value_type;
    typedef typename container_type::reference       reference;
    typedef typename container_type::const_reference const_reference;
    typedef typename container_type::size_type       size_type;
    
protected:
    container_type c;
    
public:
//    queue() = default;
    queue()
    {}
//    ~queue() = default;
    ~queue()
    {}
    
//    queue(const queue& q) = default;
    queue(const queue& q)
        : c(q.c)
    {}
#if __cplusplus >= 201103L
    queue(queue&& q) = default;
#endif  // >= C++11
    
//    queue& operator=(const queue& q) = default;
    queue& operator=(const queue& q)
    {
        if (this == &q) return *this;
        c = q.c;
        return *this;
    }
#if __cplusplus >= 201103L
    queue& operator=(queue&& q) = default;
#endif  // >= C++11

    explicit queue(const container_type& c)
        : c(c)
    {}
#if __cplusplus >= 201103L
    explicit queue(container_type&& c)
        : c(std::move(c))
    {}
#endif  // >= C++11
    template <class Alloc>
    explicit queue(const Alloc& a)
        : c(a)
    {}
    template <class Alloc>
    queue(const container_type& c, const Alloc& a)
        : c(c, a)
    {}
#if __cplusplus >= 201103L
    template <class Alloc>
    queue(container_type&& c, const Alloc& a)
        : c(std::move(c), a)
    {}
#endif  // >= C++11
    template <class Alloc>
    queue(const queue& q, const Alloc& a)
        : c(q.c, a)
    {}
#if __cplusplus >= 201103L
    template <class Alloc>
    queue(queue&& q, const Alloc& a)
        : c(std::move(q.c), a)
    {}
#endif  // >= C++11

    bool      empty() const
    {
        return c.empty();
    }
    size_type size() const
    {
        return c.size();
    }
    
    reference       front()
    {
        return c.front();
    }
    const_reference front() const
    {
        return c.front();
    }
    reference       back()
    {
        return c.back();
    }
    const_reference back() const
    {
        return c.back();
    }
    
    void push(const value_type& v)
    {
        c.push_back(v);
    }
#if __cplusplus >= 201103L
    void push(value_type&& v)
    {
        c.push_back(std::move(v));
    }
#   if __cplusplus >= 201703L
    template <class... Args>
    decltype(auto) emplace(Args&&... args) // reference in C++17
    {
        return c.emplace_back(std::forward<Args>(args)...);
    }
#   else
    template< class... Args >
    void emplace( Args&&... args )
    {
        c.emplace_back(std::forward<Args>(args)...);
    }
#   endif   // >= C++17
#endif  // >= C++11
    void pop()
    {
        c.pop_front();
    }
    
    void swap(queue& q)
#if __cplusplus >= 201103L
    noexcept
#endif  // >= C++11
    {
        c.swap(q.c);
    }
    
    // additional API
    // relation operator
    bool operator==(const queue& r) const
    {
        return c == r.c;
    }
    bool operator<(const queue& r) const
    {
        return c < r.c;
    }
    bool operator!=(const queue& r) const
    {
        return c != r.c;
    }
    bool operator>(const queue& r) const
    {
        return c > r.c;
    }
    bool operator>=(const queue& r) const
    {
        return c >= r.c;
    }
    bool operator<=(const queue& r) const
    {
        return c <= r.c;
    }
    
    bool try_pop(value_type& v)
    {
        return c.try_pop_front(v);
    }
};  // queue
}   // concurrent

namespace std
{
    template <class T>
    bool operator==(const concurrent::queue<T>& x,const concurrent::queue<T>& y)
    {
        return x == y;
    }
    
    template <class T>
    bool operator< (const concurrent::queue<T>& x,const concurrent::queue<T>& y)
    {
        return x < y;
    }
    
    template <class T>
    bool operator!=(const concurrent::queue<T>& x,const concurrent::queue<T>& y)
    {
        return x != y;
    }
    
    template <class T>
    bool operator> (const concurrent::queue<T>& x,const concurrent::queue<T>& y)
    {
        return x > y;
    }
    
    template <class T>
    bool operator>=(const concurrent::queue<T>& x,const concurrent::queue<T>& y)
    {
        return x >= y;
    }
    
    template <class T>
    bool operator<=(const concurrent::queue<T>& x,const concurrent::queue<T>& y)
    {
        return x <= y;
    }
    
    template <class T>
    void swap(concurrent::queue<T>& x, concurrent::queue<T>& y)
#if __cplusplus >= 201103L
    noexcept
#endif  // >= C++11
    {
        x.swap(y);
    }
}   // std


// TODO priority_queue
#endif /* queue_h */
