//
//  stack.hpp
//  ALL_BUILD
//
//  Created by Shi, Zexing on 8/3/18.
//

#ifndef stack_h
#define stack_h

#include <algorithm>                    // swap, < c++11
#include <stack>                        // stack
#include <utility>                      // swap, >= c++11

#include <cxx/features.hpp>             // HAS_CXX11_FEATURE_RVALUE_REFERENCE
#include <cxx/mutex.hpp>                // recursive_mutex, lock_guard

namespace concurrent
{
// Better to implement concurrent deque. leave it later
template <class T, class Container = std::deque<T> >
// NOTE: inherit from std::stack needs lots of work,
// instead of implement the API for stack, we also need implement method for stack in std namespace
//class stack : public std::stack<T, Container> {
class stack {
public:
    typedef typename Container::value_type value_type;
    typedef typename Container::reference reference;
    typedef typename Container::const_reference const_reference;
    typedef typename Container::size_type size_type;
    
public:
    ~stack()
    {
        std::lock_guard<mutex_type> lk(m);
        std::stack<T, Container>().swap(stk);
    }
    stack()
    {}
    explicit stack(const Container& c)
        : stk(c)
    {}
    stack(const stack& s)
    {
        operator=(s);
    }
    template <class Alloc>
    explicit stack(const Alloc& a)
        : stk(a)
    {}
    template <class Alloc>
    stack(const Container& c, const Alloc& a)
        : stk(c, a)
    {}
    template <class Alloc>
    stack(const stack& s, const Alloc& a)
        : stk(a)
    {
        operator=(s);
    }
#if HAS_CXX11_FEATURE_RVALUE_REFERENCE
    explicit stack(Container&& c/* = Container()*/)
        : stk(std::move(c))
    {}
    template <class Alloc>
    stack(Container&& c, const Alloc& a)
        : stk(std::move(c), a)
    {}
    stack( stack&& s )
    {
        operator=(std::move(s));
    }
    template <class Alloc>
    stack(stack&& s, const Alloc& a)
        : stk(a)
    {
        operator=(std::move(s));
    }
    stack& operator=(stack&& s)
    {
        std::scoped_lock<mutex_type, mutex_type> lk(m, s.m);
        stk = std::move(s.stk);
        return *this;
    }
#endif  // HAS_CXX11_FEATURE_RVALUE_REFERENCE
    stack& operator=(const stack& s)
    {
        std::scoped_lock<mutex_type, mutex_type> lk(m, s.m);
        stk = s.stk;
        return *this;
    }
    bool empty() const {
        std::lock_guard<mutex_type> lk(m);
        return stk.empty();
    }
    size_type size() const {
        std::lock_guard<mutex_type> lk(m);
        return stk.size();
    }
    // NOTE: zexings, not reference here. (performance drop)
    value_type top() {
        std::lock_guard<mutex_type> lk(m);
        return stk.top();
    }
    // NOTE: zexings, not const_reference here. (performance drop)
    value_type top() const {
        std::lock_guard<mutex_type> lk(m);
        return stk.top();
    }
    void push(const value_type& x) {
        std::lock_guard<mutex_type> lk(m);
        stk.push(x);
    }
#if HAS_CXX11_FEATURE_RVALUE_REFERENCE
    void push(value_type&& x) {
        std::lock_guard<mutex_type> lk(m);
        stk.push(std::move(x));
    }
    template <class... Args> void emplace(Args&&... args) {
        std::lock_guard<mutex_type> lk(m);
        stk.emplace(std::forward<Args>(args)...);
    }
#endif  // HAS_CXX11_FEATURE_RVALUE_REFERENCE
    void pop() {
        std::lock_guard<mutex_type> lk(m);
        stk.pop();
    }
    void swap(stack& s) {
        if (&s == this) return;
        std::scoped_lock<mutex_type, mutex_type> lk(m, s.m);
        std::swap(stk, s.stk);
    }

    bool operator==(const stack& s) const
    {
        if (&s == this) return true;
        std::scoped_lock<mutex_type, mutex_type> lk(m, s.m);
        return stk == s.stk;
    }
    
    bool operator<(const stack& s) const
    {
        if (&s == this) return false;
        std::scoped_lock<mutex_type, mutex_type> lk(m, s.m);
        return stk < s.stk;
    }
    
    bool operator!=(const stack& s) const
    {
        return !operator==(s);
    }
    
    bool operator>(const stack& s) const
    {
        return s.operator<(*this);
    }
    
    bool operator>=(const stack& s) const
    {
        return !operator<(s);
    }
    bool operator<=(const stack& s) const
    {
        return !s.operator<(*this);
    }
protected:
    typedef Container container_type;

    std::stack<T, Container> stk;
    typedef std::recursive_mutex mutex_type;
    mutable mutex_type m;
};
}   // namespace concurrent

namespace std
{
    template <class T, class Container>
    inline void swap(concurrent::stack<T, Container>& x, concurrent::stack<T, Container>& y)
    {
        x.swap(y);
    }

    template <class T, class Container, class Alloc>
    struct uses_allocator<concurrent::stack<T, Container>, Alloc>
        : public uses_allocator<Container, Alloc>
    {
    };
}

#endif /* stack_h */
