//
//  utility.hpp
//  CXX
//
//  Created by Shi, Zexing on 10/31/18.
//

#ifndef utility_h
#define utility_h

#include <cxx/features.hpp>                     // HAS_CXX11_FEATURE_RVALUE_REFERENCE
#include <cxx/mutex.hpp>                        // mutex
#if __cplusplus >= 201103L
#   include <type_traits>                       // compile time type check
#endif  // >= C++11

namespace concurrent
{
// NOTE: T must be copy_constructible
template <class T, class mutex_type = std::mutex>
class atomic
{
private:
    // TODO: zexings, should we remove const too
#if __cplusplus >= 201103L
    typedef typename std::remove_reference<T>::type value_type;
    static_assert(!std::is_reference<T>::value, "T cannot be reference type!!!");
    static_assert(std::is_copy_constructible<value_type>::value, "Not copy constructible!!!");
    static_assert(std::is_copy_assignable<value_type>::value, "Not copy assignable!!!");
#else
    typedef T value_type;
#endif  // >= C++11
    // NOTE: mutex has no copy/move constructor/assignment
    mutable mutex_type m;
    value_type t;
public:
    // is_default_constructible
#if __cplusplus >= 201103L
    template <bool enable = std::is_default_constructible<value_type>::value, typename std::enable_if<enable, bool>::type = true>   // OK
//    template <bool enable = std::is_default_constructible<value_type>::value, typename std::enable_if<std::is_default_constructible<value_type>::value, bool>::type = true>   // Failed requirement 'std::is_default_constructible<value_type>::value'; 'enable_if' cannot be used to disable this declaration
//    template <typename = typename std::enable_if<std::is_default_constructible<value_type>::value>::type>   // Failed requirement 'std::is_default_constructible<value_type>::value'; 'enable_if' cannot be used to disable this declaration
    atomic()
//    atomic(typename std::enable_if<std::is_default_constructible<value_type>::value, bool>::type u = true)  // Failed requirement 'std::is_default_constructible<value_type>::value'; 'enable_if' cannot be used to disable this declaration
#else
    atomic()
#endif  // >= C++11
    {}
    // is_copy_constructible
#if __cplusplus >= 201103L
    atomic(typename std::enable_if<std::is_copy_constructible<value_type>::value, const value_type&>::type t)
#else
    atomic(const value_type& t)
#endif  // >= C++11
        : t(t)
    {}
    // is_copy_assignable
#if __cplusplus >= 201103L
    atomic(typename std::enable_if<std::is_copy_assignable<value_type>::value, const atomic&>::type o)
#else
    atomic(const atomic& o)
#endif  // >= C++11
    {
        operator=(o);
    }
    // is_copy_assignable
#if __cplusplus >= 201103L
    typename std::enable_if<std::is_copy_assignable<value_type>::value, atomic&>::type
#else
    atomic&
#endif  // >= C++11
    operator=(const atomic& o)
    {
        std::scoped_lock<mutex_type, mutex_type> lk(m, o.m);
        t = o.t;
        return *this;
    }
#if __cplusplus >= 201103L
    typename std::enable_if<std::is_copy_assignable<value_type>::value, atomic&>::type
#else
    atomic&
#endif  // >= C++11
    operator=(const value_type& v)
    {
        std::lock_guard<mutex_type> lk(m);
        t = v;
        return *this;
    }
    operator T() const
    {
        std::lock_guard<mutex_type> lk(m);
        return t;
    }
#if HAS_CXX11_FEATURE_RVALUE_REFERENCE
    // is_move_assignable
    atomic(typename std::enable_if<std::is_move_assignable<value_type>::value, atomic&&>::type o)
    {
        operator=(std::move(o));
    }
    atomic(typename std::enable_if<std::is_move_assignable<value_type>::value, value_type&&>::type o)
        : t(std::move(o))
    {}
    typename std::enable_if<std::is_move_assignable<value_type>::value, atomic&>::type
    operator=(atomic&& o)
    {
        std::scoped_lock<mutex_type, mutex_type> lk(m, o.m);
        t = std::move(o.t);
        return *this;
    }
    typename std::enable_if<std::is_move_assignable<value_type>::value, atomic&>::type
    operator=(value_type&& v)
    {
        std::lock_guard<mutex_type> lk(m);
        t = std::move(v);
        return *this;
    }
#endif  // >= C++11
};  // atomic

}   // concurrent
#endif /* utility_h */
