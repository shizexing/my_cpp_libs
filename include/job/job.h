//
//  job.hpp
//  test_common
//
//  Created by Shi, Zexing on 8/7/18.
//

#ifndef job_hpp
#define job_hpp

#include <iostream>                                     // cout
#include <string>

#include <cxx/features.hpp>                             // HAS_CXX11_FEATURE_RVALUE_REFERENCE
#include <cxx/memory.hpp>                               // shared_ptr
#include <cxx/unordered_map.hpp>                        // unordered_map

#include <boost/program_options.hpp>                    // program_options

namespace job {
//// common argument name
//static constexpr auto ARG_KEY_OSM_FILENAME = "osm_filename";
//static constexpr auto ARG_KEY_OUT_FILENAME = "out_filename";
//static constexpr auto ARG_KEY_CSV_FILENAME = "csv_filename";
//
//// legacy parameters' name
//static constexpr auto ARG_JOB_LEGACY = "legacy";

/**
 * base job handler
 */
class JobHandler {
public:
#if HAS_CXX11_FEATURE_RVALUE_REFERENCE
#else
#endif  // HAS_CXX11_FEATURE_RVALUE_REFERENCE
    /**
     * execute this job
     */
    virtual bool operator()(const boost::program_options::variables_map &vm) const = 0;
    
    /**
     * name of the job
     */
    virtual std::string key() const = 0;
    
    /**
     * option for the main command line parser
     */
    virtual boost::shared_ptr<boost::program_options::option_description> option() const = 0;
    
    /**
     * sub options for this job
     */
    virtual boost::program_options::options_description options() const = 0;
};

class JobExecutor {
public:
    /**
     * instance method
     */
    static JobExecutor & instance();
    
#if HAS_CXX11_FEATURE_RVALUE_REFERENCE
    bool register_job_handler(std::unique_ptr<JobHandler> &&job);
#else
    bool register_job_handler(std::unique_ptr<JobHandler> &job);
#endif  // HAS_CXX11_FEATURE_RVALUE_REFERENCE
    
    bool operator()(int argc, char **argv) const {
        // parsing arguments
        std::pair<std::string, boost::program_options::variables_map> p = parsing(argc, argv);
        if (_jobs.count(p.first)) {
            const JobHandler &handler = *_jobs.at(p.first);
            std::cout << "====== start " << handler.key() << " ======" << std::endl;
            bool ret = handler(p.second);
            std::cout << "====== end   " << handler.key() << " ======" << std::endl;
            return ret;
        }
        return false;
    }
    
private:
    std::unordered_map<std::string, std::unique_ptr<JobHandler> > _jobs;
private:
    /**
     * parsing cmdline options
     */
    std::pair<std::string, boost::program_options::variables_map> parsing(int argc, char **argv) const;
};
    
}   // job

#endif /* job_hpp */
