//
//  filesystem.h
//  CXX
//
//  Created by Shi, Zexing on 11/28/18.
//

#ifndef utility_filesystem_h
#define utility_filesystem_h

#include <cxx/filesystem.hpp>                       // filesystem

namespace util
{
namespace filesystem
{

inline std::filesystem::path absolute(const std::filesystem::path& p) noexcept
{
#ifdef _MSC_VER
    return std::filesystem::absolute(p);
#else
    std::error_code ec;
    return std::filesystem::absolute(p, ec);
#endif  // _MSC_VER
}

inline bool create_directory(const std::filesystem::path& p) noexcept
{
    std::error_code ec;
    return std::filesystem::create_directory(p, ec);
}

/**
 * create_directories recursive if neccessary
 * @return true: if directories create success or directory already exists (is this compatible with stl?)
 */
inline bool create_directories(const std::filesystem::path& p) noexcept
{
    std::error_code ec;
    return std::filesystem::create_directories(p, ec);
}

inline std::filesystem::path current_path() noexcept
{
    std::error_code ec;
    return std::filesystem::current_path(ec);
}

/**
 * Checks if the given path corresponds to an existing file or directory.
 * <a href="https://en.cppreference.com/w/cpp/filesystem/exists">exists</a>
 */
inline bool exists(const std::filesystem::path& p) noexcept
{
    std::error_code ec;
    return std::filesystem::exists(p, ec);
}

inline uintmax_t file_size(const std::filesystem::path& p) noexcept
{
    std::error_code ec;
    return std::filesystem::file_size(p, ec);
}

inline bool is_directory(const std::filesystem::path& p) noexcept
{
    std::error_code ec;
    return std::filesystem::is_directory(p, ec);
}

inline bool is_empty(const std::filesystem::path& p) noexcept
{
    std::error_code ec;
    return std::filesystem::is_empty(p, ec);
}

/**
 * Checks if the given path corresponds to a regular file
 */
inline bool is_regular_file(const std::filesystem::path& p) noexcept
{
    std::error_code ec;
    return std::filesystem::is_regular_file(p, ec);
}

/**
 * get last access time for the file
 * NOTE: it looks weird that, stl doesnot has last_access_time
 * if the file is not exist. return epoch time
 */
std::filesystem::file_time_type last_access_time(const std::filesystem::path& p) noexcept;

inline std::filesystem::file_time_type last_write_time(const std::filesystem::path& p) noexcept
{
    std::error_code ec;
    return std::filesystem::last_write_time(p, ec);
}

/**
 * The file or empty directory identified by the path p is deleted as if by the POSIX remove. Symlinks are not followed (symlink is removed, not its target)
 * <a href="https://en.cppreference.com/w/cpp/filesystem/remove">remove</a>
 * @param p : the path
 * @return: true if the file was deleted, false if it did not exist.
 */
inline bool remove(const std::filesystem::path& p) noexcept
{
    std::error_code ec;
    return std::filesystem::remove(p, ec);
}

inline uintmax_t remove_all(const std::filesystem::path &p) noexcept
{
    std::error_code ec;
    return std::filesystem::remove_all(p, ec);
}

inline std::filesystem::space_info space(const std::filesystem::path& p) noexcept
{
    std::error_code ec;
    return std::filesystem::space(p, ec);
}

}}  // util::filesystem

#endif /* utility_filesystem_h */
