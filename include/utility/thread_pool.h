//
//  thread_pool.h
//  extensions/utility
//
//  Created by Zexing Shi on 11/25/19.
//  All rights reserved.
//
#include <array>
#include <functional>
#include <memory>
#include <thread>

// @reference: 
// https://en.cppreference.com/w/cpp/utility/functional/invoke
// https://en.cppreference.com/w/cpp/thread/packaged_task
// https://gitlab.com/cantordust/threadpool/blob/master/include/threadpool.hpp

namespace zs {
namespace util {
template <size_t N>
class thread_pool
{
	// http://www.open-std.org/jtc1/sc22/wg21/docs/cwg_defects.html#45
private:
	class impl : public std::enable_shared_from_this<impl> {
	public:
		~impl() {
			stop(true);
		}

		void start() {
			for (auto & t : _threads) {
				t = std::make_shared<std::thread>(&impl::run, this->shared_from_this());	// ok
				// NOTE: due to template alternate, it has compile error
	//			t = std::make_shared<std::thread>(&thread_pool::run, shared_from_this());	// error: use of undeclared identifier 'shared_from_this'
			}
		}

		void stop(bool const join = false) {
			_quit = true;
			{
				std::lock_guard<decltype(_m)> lk(_m);
				_cv.notify_all();
			}
			if (!join) {
				return;
			}
			for (auto & t : _threads) {
				if (t && t->joinable() && t->get_id() != std::this_thread::get_id()) {
					t->join();
				}
			}
		}

		/**
		 * enqueue the async operation and return future
		 */
		template <typename F, class ...Args>
		inline auto enqueue(F&& f, Args&&... args)
		-> std::future<std::result_of_t<std::decay_t<F>(std::decay_t<Args>...)>>
		{
			using RetT = typename std::result_of_t<std::decay_t<F>(std::decay_t<Args>&...)>;
			auto bd = std::bind(std::forward<F>(f), std::forward<Args>(args)...);
			// NOTE: packaged_task::operator() is non const
			// 	I cannot remember what is the error if we create the lambda (in push) with mutable
			//	but seems wrap it with shared_ptr fix the issue
			// TODO: zexing, figure out the root cause
			auto task = std::make_shared<std::packaged_task<RetT()>>(bd);

			auto ft = task->get_future();

			{
				std::unique_lock<decltype(_m)> lk(_m);
				_queue.push([task = std::move(task)] {
					(*task)();
				});
				_cv.notify_one();
			}
			return ft;
		}

		/**
		 * execute the operation in thread_pool and wait for the result
		 * usually, this is working when N == 1
		 */
		template <typename F, class ...Args>
		inline auto execute(F&& f, Args&&... args)
		-> std::enable_if_t<N==1, std::result_of_t<std::decay_t<F>(std::decay_t<Args>...)>>
		{
			using RetT = typename std::result_of_t<std::decay_t<F>(std::decay_t<Args>&...)>;
			auto bd = std::bind(std::forward<F>(f), std::forward<Args>(args)...);
			// NOTE: packaged_task::operator() is non const
			// 	I cannot remember what is the error if we create the lambda (in push) with mutable
			//	but seems wrap it with shared_ptr fix the issue
			// TODO: zexing, figure out the root cause
			auto task = std::make_shared<std::packaged_task<RetT()>>(bd);
			auto ft = task->get_future();
			{
				std::unique_lock<decltype(_m)> lk(_m);
				_queue.push([task = std::move(task)] {
					(*task)();
				});
				_cv.notify_one();
			}
			return ft.get();
		}
	private:
		void run() {
			using namespace std::literals::chrono_literals;
			while(!_quit) {
				std::function<void()> functor;
				{
					std::unique_lock<decltype(_m)> lk(_m);
					if (_queue.empty()) {
						_cv.wait_for(lk, 15s);
					} else {
						functor = std::move(_queue.front());
						_queue.pop();
					}
				}
				if (functor) {
					functor();
				}
			}
		}

	private:
		volatile bool _quit = false;
		std::array<std::shared_ptr<std::thread>, N> _threads;
		std::queue<std::function<void()>> _queue;
		std::mutex _m;
		std::condition_variable _cv;
	};	// impl
	// http://www.open-std.org/jtc1/sc22/wg21/docs/cwg_defects.html#45
private:
	std::shared_ptr<impl> _impl;
public:
	thread_pool() {
		_impl = std::make_shared<impl>();
		if (_impl) {
			_impl->start();
		}
	}
    ~thread_pool() {
		if (_impl) {
			_impl->stop(true);
		}
    }
	void stop(bool const join = true) {
		if (_impl) {
			_impl->stop(join);
		}
	}
    template <typename F, class ...Args>
	inline auto enqueue(F&& f, Args&&... args)
	-> std::future<std::result_of_t<std::decay_t<F>(std::decay_t<Args>...)>>
	{
		if (_impl) {
			return _impl->enqueue(std::forward<F>(f), std::forward<Args>(args)...);
		}
		return {};
	}

	template <typename F, class ...Args>
	inline auto execute(F&& f, Args&&... args)
	-> std::enable_if_t<N==1, std::result_of_t<std::decay_t<F>(std::decay_t<Args>...)>>
	{
		assert(_impl);
		return _impl->execute(std::forward<F>(f), std::forward<Args>(args)...);
	}
};  // thread_pool
}}  // zs::util

/*
 Test
 
	auto pool = std::make_shared<thread_pool<2>>();
	BOOST_REQUIRE(pool);
	pool->enqueue([]{
		ZXLOG("simple lambda");
	});
	int c;
	auto fd = pool->enqueue([a = 11, b = 22, &c]{
		c = a + b;
		ZXLOG("a = %d, b = %d, c = %d", a, b, c);
		return c;
	});
	auto fv = pool->enqueue([](int v, std::string m) {
		ZXLOG("%s, %d", m.c_str(), v);
	}, 10, "hello thread_pool : ");
	auto d = fd.get();
	ZXLOG("d = %d", d);
	BOOST_CHECK_EQUAL(d, c);
	fv.get();

	{
		thread_pool<1> sync_pool;
		auto i = sync_pool.execute([]{
			ZXLOG("test execute");
			return 2;
		});
		ZXLOG("i = %d", i);
	}
 */