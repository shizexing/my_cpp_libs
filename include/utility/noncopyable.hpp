//
//  noncopyable.hpp
//  CXX
//
//  Created by Shi, Zexing on 11/14/18.
//

#ifndef utility_noncopyable_h
#define utility_noncopyable_h

// Reference:
// https://en.cppreference.com/w/cpp/language/default_constructor
// https://en.cppreference.com/w/cpp/language/copy_constructor
// https://en.cppreference.com/w/cpp/language/move_constructor
// https://en.cppreference.com/w/cpp/language/copy_assignment
// https://en.cppreference.com/w/cpp/language/move_assignment
// https://en.cppreference.com/w/cpp/language/destructor
namespace util
{
/**
 * base class for non copyable classes
 *
 * usage:
 *  class T : private inherits me
 *  {
 *      ...
 *  }; // T
 */
#if __cplusplus >= 201103L || defined(_MSC_VER)
// if >= C++11, noncopyable is a trivial, POD class
class noncopyable
{
public:
    constexpr noncopyable() = default;
    noncopyable(const noncopyable&) = delete;
    noncopyable& operator=(const noncopyable&) = delete;
};  // noncopyable
#else
// if < C++11, noncopyable is not a non-trivial, non-POD class
class noncopyable
{
public:
    noncopyable() {}
private:
    noncopyable(const noncopyable&);
    noncopyable& operator=(const noncopyable&);
};  // noncopyable
#endif  // >= C++11

}   // util

#endif /* utility_noncopyable_h */
