/**
 * Copyright (c) 2018 Shi, Zexing
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 * 
 * 
 * stream_appender.h
 * Created by Shi, Zexing on 5/18/18.
 * 
 */
#pragma once

#include <iosfwd>
#include <vector>

#if __cplusplus >= 201103L
#   include <unordered_map>
#endif  // C++11

namespace std {
    // sepcialized for std::pair
    template <class _CharT, class _Traits, class _T1, class _T2>
    inline basic_ostream<_CharT, _Traits>& operator<<(basic_ostream<_CharT, _Traits>& os, const pair<_T1, _T2>& p);

    // specialized for std::vector
    template <class _CharT, class _Traits, class _Tp, class _Allocator/* = std::allocator<_Tp> */>
    inline std::basic_ostream<_CharT, _Traits>& operator<<(std::basic_ostream<_CharT, _Traits>& os, const std::vector<_Tp, _Allocator> &c);

#if __cplusplus >= 201103L
    // unordered_map
    template <class _CharT, class _Traits, class _Key, class _Tp, class _Hash/* = std::hash<_Key>*/, class _Pred/* = std::equal_to<_Key>*/, class _Alloc/* = std::allocator<std::pair<const _Key, _Tp> > */>
    inline basic_ostream<_CharT, _Traits>& operator<<(basic_ostream<_CharT, _Traits>& os, const unordered_map<_Key, _Tp, _Hash, _Pred, _Alloc>& c);
#endif  // >= C++11
}   // std
