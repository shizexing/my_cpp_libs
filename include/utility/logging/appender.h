/**
 * Copyright (c) 2018 Shi, Zexing
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 * 
 * 
 * appender.h
 * Created by Shi, Zexing on 5/18/18.
 * 
 */
#pragma once

#include <fstream>
#include <iostream>
#include <memory>                   // unique_ptr
#include "stream.hpp"

namespace zs {
namespace utils {
namespace logging {

template <class _CharT>
inline typename std::enable_if<std::is_same<_CharT, char>::value, std::basic_streambuf<char>* >::type
output_stream_buf(char) 
{
    return std::cout.rdbuf();
}

template <class _CharT>
inline typename std::enable_if<std::is_same<_CharT, wchar_t>::value, std::basic_streambuf<wchar_t>* >::type
output_stream_buf(wchar_t) 
{
    return std::wcout.rdbuf();
}

// reference: how to derive from streambuf
//  1. https://artofcode.wordpress.com/2010/12/12/deriving-from-stdstreambuf/
//  2. http://en.cppreference.com/w/cpp/io/basic_streambuf
template <class _CharT>
class basic_streambuf : public std::basic_streambuf<_CharT> {
public:
    // ref: http://en.cppreference.com/w/cpp/language/character_literal
    /**
     * default constructor.
     * use std::cout.rdbuf() for output
     */
    basic_streambuf() : _pbuf(output_stream_buf<_CharT>(static_cast<_CharT>('\0'))) {
        std::cout << "[debug] :" << __FUNCTION__ << "+" << __LINE__ << ", t:" << typeid(_CharT).name() << ", pbuf:" << (void*)_pbuf << std::endl;
    }
    /**
     * file streaming constructor
     * @param fn: file name
     */
    basic_streambuf(const std::string& fn) 
        : _p_file_buf(new std::basic_filebuf<_CharT>)
        , _pbuf(_p_file_buf.get())
    {
        std::cout << "[debug] :" << __FUNCTION__ << "+" << __LINE__ << ", fn:" << fn << ", fp:" << (void*)_p_file_buf.get() << std::endl;
        if (_p_file_buf)
        {
            _p_file_buf->open(fn, std::ios_base::app);
            if (!_p_file_buf->is_open()) 
            {
                std::cerr << "[fatal] open file : " << fn << ", failed\n";
                _pbuf = nullptr;
            }
            else
            {
                std::cout << "[debug] file open DONE!. pbuf:" << (void*)_pbuf << "\n";
            }
        }
    }

    virtual ~basic_streambuf() 
    {
        if (_p_file_buf)
        {
            _p_file_buf->close();
        }
    }

    std::basic_streambuf<_CharT>* rdbuf() const 
    {
        std::cout << "[debug] pbuf:" << (void*)_pbuf << "\n";
        return _pbuf;
    }

protected:
    /**
     * file_buf
     */
    std::unique_ptr<std::basic_filebuf<_CharT> > _p_file_buf;
    /**
     * the working streambuf
     */
    std::basic_streambuf<_CharT>* _pbuf;

// http://gabisoft.free.fr/articles-en.html
// http://gabisoft.free.fr/articles/fltrsbf1.html
// https://www.boost.org/doc/libs/1_67_0/libs/iostreams/doc/index.html
// NOTE: zexing, if you want to implement your own streambuf, you must implement some of the virtual functions.
// protected:
//     // 27.6.2.4 virtual functions:
//     // 27.6.2.4.1 Locales:
//     virtual void imbue(const locale& loc);

//     // 27.6.2.4.2 Buffer management and positioning:
//     virtual basic_streambuf* setbuf(char_type* s, streamsize n);
//     virtual pos_type seekoff(off_type off, ios_base::seekdir way,
//                              ios_base::openmode which = ios_base::in | ios_base::out);
//     virtual pos_type seekpos(pos_type sp,
//                              ios_base::openmode which = ios_base::in | ios_base::out);
//     virtual int sync();

//     // 27.6.2.4.3 Get area:
//     virtual streamsize showmanyc();
//     virtual streamsize xsgetn(char_type* s, streamsize n);
//     virtual int_type underflow();
//     virtual int_type uflow();

//     // 27.6.2.4.4 Putback:
//     virtual int_type pbackfail(int_type c = traits_type::eof());

//     // 27.6.2.4.5 Put area:
//     virtual streamsize xsputn(const char_type* s, streamsize n);
//     virtual int_type overflow (int_type c = traits_type::eof());
};

/**
 * wrapper streambuf, inherit std::basic_ostream<_CharT> to provide printf like functionality
 */
template <class _CharT>
class basic_appender : /*virtual*/ protected basic_streambuf<_CharT>, public std::basic_ostream<_CharT> {
public:
    // http://www.angelikalanger.com/Articles/C++Report/IOStreamsDerivation/IOStreamsDerivation.html
    // NOTE: if we use virtual inherit for basic_streambuf<_CharT>,
    //  the sequence for construct appender(const std::string&) is
    //  1. basic_streambuf<_CharT>()
    //  2. std::basic_ostream<_CharT>(std::basic_streambuf<_CharT>*)
    //  3. basic_appender(const std::string&)
    //  which is not expected. 
    /**
     * default io streaming appender
     */
    basic_appender() : std::basic_ostream<_CharT>(basic_streambuf<_CharT>::rdbuf()) {}
    /**
     * file streaming appender
     */
    basic_appender(const std::string& fn)
        : basic_streambuf<_CharT>(fn)
        , std::basic_ostream<_CharT>(basic_streambuf<_CharT>::rdbuf())
    {
        std::cout << "[debug] @" << __FUNCTION__ << ", fn:" << fn << std::endl;
    }
    /**
     * streaming appender to another stream
     */
    basic_appender(std::basic_ostream<_CharT>& os) : std::basic_ostream<_CharT>(os.rdbuf()) {}

    // http://www.stroustrup.com/C++11FAQ.html#variadic-templates
    /**
     * helper. c-style format output
     */
    template <class... TArgs>
    basic_appender& printf(const char* fmt, TArgs... args)
    {
        std::printf(*this, fmt, std::forward<TArgs>(args)...);
        return *this;
    }
    // basic_appender& printf(const char* fmt, ...)
    // {
    //     std::va_list args;
    //     va_start(args, fmt);
    //     std::vprintf(*this, fmt, args);
    //     va_end(args);
    //     return *this;
    // }
};

using appender = basic_appender<char>;

extern appender zerr;
extern appender zlog;
extern appender zout;

}}} // zs::utils::logging

using zs::utils::logging::zerr;
using zs::utils::logging::zlog;
using zs::utils::logging::zout;
