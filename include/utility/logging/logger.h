/**
 * Copyright (c) 2018 Shi, Zexing
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 * 
 * 
 * logger.h
 * Created by Shi, Zexing on 5/24/18.
 * 
 */
#pragma once

#include <list>
#include <memory>

#include "appender.h"

namespace zs {
namespace utils {
namespace logging {

// https://stackoverflow.com/questions/71416/forward-declaring-an-enum-in-c
enum class log_level : uint8_t
{
    NONE    = 0,
    VERBOSE = 2,
    DEBUG   = 4,
    INFO    = 8,
    WARN    = 16,
    ERROR   = 32
};  // enum class log_level: uint8_t

/**
 * helper function to get log_level name
 * @param lvl: log_level
 * @return name of the log_level
 */
// template <class _CharT>
// inline typename std::enable_if<std::is_same<_CharT, char>::value, const char* const>::type
const char* const log_level_name(log_level lvl);

template <class _CharT>
class basic_logger 
{
public:
    typedef _CharT char_type;
    typedef basic_appender<_CharT> appender_type;

public:
    basic_logger(appender_type& app, log_level lvl, const _CharT* const tag) : _appender(app)
    {
        _appender 
            << '[' << log_level_name(lvl) << ']'
            << '[' << tag << ']';
    }

    template <class T>
    basic_logger& operator<<(const T& t)
    {
        _appender << t;
        return *this;
    }
private:
    appender_type& _appender;
};  // class basic_logger

using logger = basic_logger<char>;
using wlogger = basic_logger<wchar_t>;

class loggers
{
public:
    static loggers& instance();
    // loggers() = delete;
private:
    loggers() {}

public:

    template <class _CharT>
    bool add_appender(basic_logger<_CharT>& lg)
    {
        auto& c = loggers_list<_CharT>(static_cast<_CharT>('\0'));
        for (auto p : c) {
            if (&lg == p)   return false;
        }
        c.push_back(&lg);
        return true;
    }

    template <class _CharT>
    bool remove_appender(basic_logger<_CharT>& lg)
    {
        auto& c = loggers_list<_CharT>(static_cast<_CharT>('\0'));
        c.remove(&lg);
        return true;
    }

    template <class T>
    loggers& operator<<(const T& t)
    {
        for (auto p : _loggers) 
        {
            *p << t;
        }
        for (auto p : _wloggers) 
        {
            *p << t;
        }
        return *this;
    }

private:
    std::list<logger*> _loggers;
    std::list<wlogger*> _wloggers;

private:
    template <class _CharT>
    typename std::enable_if<std::is_same<char, _CharT>::value, std::list<basic_logger<char>* >&>::type
    loggers_list(char)
    {
        return _loggers;
    }

    template <class _CharT>
    typename std::enable_if<std::is_same<wchar_t, _CharT>::value, std::list<basic_logger<wchar_t>* >&>::type
    loggers_list(wchar_t)
    {
        return _wloggers;
    }

};

}}} // zs::utils::logging

// https://github.com/KjellKod/g3log/blob/master/src/g3log/g3log.hpp
#if !defined(__GNUC__)
#define __PRETTY_FUNCTION__ __FUNCTION__
#endif  // !__PRETTY_FUNCTION__