/**
 * Copyright (c) 2018 Shi, Zexing
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 * 
 * 
 * stream.h
 * Created by Shi, Zexing on 5/19/18.
 * 
 */
#pragma once

#include "stream_fwd.h"

#include <cstdarg>

#include <algorithm>
#include <vector>

namespace std {
    template <class _CharT, class _Traits, class InputIterator, class _T/* = typename InputIterator::value_type*/>
    inline basic_ostream<_CharT, _Traits>& print_container(basic_ostream<_CharT, _Traits>& os, InputIterator first, InputIterator last) 
    {
        if (first == last)  return os << "[]";
        os << '[';
        os << *first++;
        for_each(first, last, [&os](const _T &v) {
            os << ',' << v;
        });
        return os << ']';
    }
    
    template <class _CharT, class _Traits, class _T1, class _T2>
    inline basic_ostream<_CharT, _Traits>& operator<<(basic_ostream<_CharT, _Traits>& os, const pair<_T1, _T2> &p) 
    {
        return os << '{' << p.first << ':' << p.second << '}';
    }

    // template <class _CharT, class _Traits, class _Tp, class _Allocator/* = allocator<_Tp> */>
    template <class _CharT, class _Traits, class _Tp, class _Allocator = allocator<_Tp> >
    inline basic_ostream<_CharT, _Traits>& operator<<(basic_ostream<_CharT, _Traits>& os, const vector<_Tp, _Allocator> &c)
    {
        // NOTE: in AppleClang, (4.2.1), if _Tp is basic type, this will cause compiler error. use the commentted code can fix this.
        using InputIterator = typename vector<_Tp, _Allocator>::const_iterator;
        using T = typename InputIterator::value_type;
        return print_container<_CharT, _Traits, InputIterator, T>(os, c.begin(), c.end());
    }
    
    template <class _CharT, class _Traits, class _Key, class _Tp, class _Hash/* = hash<_Key>*/, class _Pred/* = equal_to<_Key>*/, class _Alloc/* = allocator<pair<const _Key, _Tp> > */>
    inline basic_ostream<_CharT, _Traits>& operator<<(basic_ostream<_CharT, _Traits>& os, const unordered_map<_Key, _Tp, _Hash, _Pred, _Alloc>& c) 
    {
        return print_container(os, c.begin(), c.end());
    }

    // https://stackoverflow.com/questions/15106102/how-to-use-c-stdostream-with-printf-like-formatting/47909115#47909115
    template <class _CharT, class _Traits>
    inline basic_ostream<_CharT, _Traits>& vprintf(basic_ostream<_CharT, _Traits>& os, const char* fmt, va_list& vlist)
    {
        // get required size;
        va_list args;
        va_copy(args, vlist);
        const int required_len = vsnprintf(nullptr, 0, fmt, args) + 1;
        va_end(args);
        
        string buf(required_len, '\0');
        int r = vsnprintf(&buf[0], buf.size(), fmt, vlist);
        if (r < 0) 
        {
            return os << "FATAL ERROR: format output failed!!!";
        }
        
        return os << buf.c_str();
    }

    template <class _CharT, class _Traits>
    inline basic_ostream<_CharT, _Traits>& printf(basic_ostream<_CharT, _Traits>& os, const char* fmt, ...)
    {
        va_list args;
        va_start(args, fmt);
        vprintf(os, fmt, args);
        va_end(args);
        return os;
    }
}   // std
