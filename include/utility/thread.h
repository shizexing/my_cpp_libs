//
//  thread.h
//  CXX
//
//  Created by Shi, Zexing on 9/12/18.
//

#ifndef utility_thread_h
#define utility_thread_h

#include <string>
#include <cxx/thread.hpp>                       // thread

namespace util
{
namespace thread
{

/**
 * get current thread name
 */
std::string get_name();

/**
 * get thread name
 * @warning: NOT supported for Android & Windows platforms, if t is not current thread
 */
std::string get_name(std::thread& t);

/**
 * set current thread name
 */
bool set_name(const std::string& name);

/**
 * set thread name. 
 * @warning: NOT supported for Android, APPLE & Windows platforms, if t is not current thread
 * @param t: the thread object
 * @param name: new thread name
 * @return true if succeeded
 */
bool set_name(std::thread& t, const std::string& name);

/**
 * get thread priority
 * @return: -1, if error
 */
int get_priority(std::thread& t);

/**
 * set current thread priority
 * NOTE: on OSX, acceptable priority is in [15(min), 47(max)]
 */
bool set_priority(int priority);

/**
 * set thread priority
 * NOTE: on OSX, acceptable priority is in [15(min), 47(max)]
 * @warning: NOT working on OSX, if t is not current thread
 */
bool set_priority(std::thread& t, int priority);

}}   // util::thread

#endif /* utility_thread_h */
