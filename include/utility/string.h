//
//  string.h
//  CXX
//
//  Created by Shi, Zexing on 10/05/18.
//

#ifndef utility_string_h
#define utility_string_h

#include <string>
#include <vector>

// string utility.
namespace util {
/// string equal ignore case
bool iequals(const std::string& a, const std::string& b);

/// string ends with
bool ends_with(std::string const & value, std::string const & ending);

/**
 * replace all $from -> $to
 * @return: the string s it self
 */
std::string& replace_all(std::string& s, char from, char to);
std::string& replace_all(std::string& s, const std::string &from, const std::string &to);

/// string split by delimiter
std::vector<std::string> split(std::string const& s, const char d);

}   // util

#endif  // !utility_string_h
