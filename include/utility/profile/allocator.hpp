/**
 * Copyright (c) 2018 Shi, Zexing
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 * 
 * 
 * allocator.h
 * Created by Shi, Zexing on 6/20/18.
 * 
 */
#ifndef allocator_h
#define allocator_h

#include <functional>       // hash
#include <list>             // list
#include <memory>           // allocator
#include <mutex>            // lock_guard
#include <string>           // hash<string>
#include <unordered_map>    // unordered_map

namespace zs {
namespace utils {
namespace profile {

struct base_allocator {
    virtual ~base_allocator() {}
    virtual void* allocate(std::size_t n) = 0;
    virtual void deallocate(void *p, std::size_t n) = 0;
};  // base_allocator
    
/**
 * raw allocator. wrapper for std::allocator
 */
struct raw_allocator : public base_allocator {
public:
    static constexpr auto KEY = "raw_allocator";
    virtual void* allocate(std::size_t n) override {
        // TODO: call aligned allocate if applicable
        return std::malloc(n);
    }
    virtual void deallocate(void *p, std::size_t) override {
        std::free(p);
    }
    template <class T>
    T* allocate(std::size_t n) {
        // NOTE: zexings, the reason to use std::allocator<T>().allocate(std::size_t) other than
        //  call ::new, ::malloc directly is that std::allocator<T> may consider alignof(T) to
        //  make sure the allignment requirement is met
        return std::allocator<T>().allocate(n);
    }

    template <class T>
    void deallocate(T* p, std::size_t n) {
        std::allocator<T>().deallocate(p, n);
    }
};

/**
 * pool allocator
 */
template <class MutexT>
struct pool_allocator : base_allocator {
public:
    static constexpr auto KEY = "pool_allocator";
    virtual void* allocate(std::size_t n) override {
        {
            std::lock_guard<MutexT> lk(mutex);
            if (free_list.count(n) && !free_list[n].empty()) {
                void *ret = free_list[n].front();
                free_list[n].pop_front();
                return ret;
            }
        }
        return raw_allocator().allocate(n);
    }
    
    virtual void deallocate(void *p, std::size_t n) override {
        {
            std::lock_guard<MutexT> lk(mutex);
            free_list[n].push_front(p);
        }
    }
    
    template <class T>
    T* allocate(std::size_t n) {
        {
            std::lock_guard<MutexT> lk(mutex);
            const std::size_t sz = sizeof(T) * n;
            if (free_list.count(sz) && !free_list[sz].empty()) {
                void *ret = free_list[sz].front();
                free_list[sz].pop_front();
                return static_cast<T*>(ret);
            }
        }
        return raw_allocator().allocate<T>(n);
    }
    
    template <class T>
    void deallocate(T* p, std::size_t n) {
        {
            std::lock_guard<MutexT> lk(mutex);
            free_list[sizeof(T) * n].push_front(static_cast<void *>(p));
        }
    }
private:
    std::unordered_map<std::size_t, std::list<void *> > free_list;
    MutexT mutex;
};

/**
 * factory of allocator
 */
class allocator_factory {
public:
    // NOTE: zexings, put instance method into header file is dangerous if this header is included into two dlls/exes
    static allocator_factory& instance() {
        static allocator_factory instance;
        return instance;
    }
    /**
     * get allocator
     */
    base_allocator& allocator() {
        return *_allocator;
    }
    allocator_factory(const allocator_factory &) = delete;
    allocator_factory& operator=(const allocator_factory &) = delete;
private:
    allocator_factory() {
        // TODO: add register method
        allocator_names[raw_allocator::KEY] = std::unique_ptr<base_allocator>(new raw_allocator);
//        allocator_names[std::string(typeid(pool_allocator).name())] = std::unique_ptr<base_allocator>(new pool_allocator);
        allocator_names[pool_allocator<std::mutex>::KEY] = std::unique_ptr<base_allocator>(new pool_allocator<std::mutex>);
        
        _allocator = std::move(allocator_names[pool_allocator<std::mutex>::KEY]);
        allocator_names.clear();
    }
    std::unordered_map<std::string, std::unique_ptr<base_allocator> > allocator_names;
    std::unique_ptr<base_allocator> _allocator;
};

/**
 * logged allocator
 * requirement for allocator which can used for stl container
 *  1. cannot have state (c++03 only) - no state means no virtual functions, vtable can be considerd state.
 *  2. must support certain typedefs and methods
 *  3. must support type rebinding
 *  4. allocators of the same type must compare equally
 * 
 * @ref http://en.cppreference.com/w/cpp/concept/Allocator
 * @ref http://www.cplusplus.com/reference/memory/allocator_traits/
 * @ref http://jrruethe.github.io/blog/2015/11/22/allocators/
 */
//template <class T>
template <typename T>
struct logged_allocator
{
public:
    typedef T value_type;
    T* allocate( std::size_t n ) {
//        std::cout << "decltype:" << typeid(decltype(allocator_factory::instance().allocator())).name() << std::endl;
        auto &allocator = allocator_factory::instance().allocator();

        T* ret = nullptr;
        // C++ does not have similar feature like Reflection in JAVA
        pool_allocator<std::mutex>* pa = dynamic_cast<pool_allocator<std::mutex>*>(&allocator);
        if (pa) {
            ret = pa->allocate<T>(n);
        }
        if (nullptr == ret) {
            raw_allocator* ra = dynamic_cast<raw_allocator*>(&allocator);
            ret = ra->allocate<T>(n);
        }
        if (nullptr == ret) {
            ret = static_cast<T*>(allocator.allocate(n * sizeof(T)));
        }
#if NGX_BENCHMARK_ENABLE
        std::cout << "alloc " << (n*sizeof(T)) << " bytes for " << typeid(T).name() << ". " << std::hex << static_cast<void*>(ret) << std::dec << std::endl;
#endif  // NGX_BENCHMARK_ENABLE
        return ret;
    }
    
    void deallocate( T* p, std::size_t n ) {
#if NGX_BENCHMARK_ENABLE
        std::cout << "dealloc " << (n*sizeof(T)) << " bytes for " << typeid(T).name() << ". " << std::hex << static_cast<void*>(p) << std::dec << std::endl;
#endif  // NGX_BENCHMARK_ENABLE
        allocator_factory::instance().allocator().deallocate(static_cast<void *>(p), sizeof(T) * n);
    }

// so far, clang on osx works fine. but gcc 4.8.1 requires the following stuff
// NOTE: all these are deprecated in c++17
public:
    typedef T* pointer;
    typedef const T* const_pointer;
    typedef T& reference;
    typedef const T& const_reference;
    template <class U> struct rebind {
        typedef logged_allocator<U> other;
    };
    
    logged_allocator() noexcept {}
    template <class U> logged_allocator (const logged_allocator<U>&) noexcept {}
    
    void construct(pointer p, const_reference val)
    {
        new((void *)p) T(val);
    }
    template< class U, class... Args >
    void construct( U* p, Args&&... args )
    {
        ::new((void *)p) U(std::forward<Args>(args)...);
    }
    void destroy(pointer p)
    {
        ((T*)p)->~T();
    }
    template< class U >
    void destroy( U* p )
    {
        p->~U();
    }
// NOTE: request for std::basic_string<...> on gcc 4.8.1
public:
    typedef std::size_t size_type;
    typedef std::ptrdiff_t difference_type;
};

}}} // zs::utils::profile

#endif /* allocator_h */
