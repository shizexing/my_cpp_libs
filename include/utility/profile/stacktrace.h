/**
 * Copyright (c) 2017 Shi, Zexing
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 * 
 * 
 * stacktrace.h
 * Created by Shi, Zexing on 6/2/17.
 * 
 */
#ifndef stacktrace_h
#define stacktrace_h

#include <iosfwd>

#if defined(__unix__)
#include <unistd.h> // STDOUT_FILENO
#endif  // __unix__

namespace zs {
namespace utils {
namespace profile {

/**
 * get stack trace for the current thread
 * @param ignore : the number of stack trace that need to ignore.
 *  default value is 1, which means donot add stacktrace(size_t) into call stacks.
 * @return: call stacks as string
 */
std::string stacktrace(size_t ignore=1);

/**
 * dump stack trace for the current thread to output stream
 * @param os : output stream
 * @param ignore : the number of callstacks we want to ignore.
 * @return: the output stream
 */
std::ostream& stacktrace(std::ostream &os, size_t ignore=0);

#if defined(__unix__)
/**
 * dump stack trace to file
 * @param fd : file descriptor
 */
void stacktrace(int fd/* = STDOUT_FILENO*/);
#endif  // __unix__

}}} // zs::utils::profile

#endif /* stacktrace_h */
