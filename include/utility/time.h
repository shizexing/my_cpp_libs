//
//  time.h
//  CXX
//
//  Created by Shi, Zexing on 11/27/18.
//

#ifndef utility_time_h
#define utility_time_h

namespace util
{
/**
 * time since epoch
 */
double now();

}   // util
#endif /* utility_time_h */
