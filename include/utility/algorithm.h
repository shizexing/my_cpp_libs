//
//  algorithm.h
//  CXX
//
//  Created by Shi, Zexing on 11/19/18.
//

#ifndef utility_algorithm_h
#define utility_algorithm_h

#include <string>                               // string

namespace util
{
/**
 * generate hex (lower case) representaion of md5 for s
 */
std::string md5(const std::string& s);
}   // util

#endif /* utility_algorithm_h */
