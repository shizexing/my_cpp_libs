/**
 * Copyright (c) 2018 Shi, Zexing
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 * 
 * 
 * macros.h
 * Created by Shi, Zexing on 6/4/18.
 * 
 */

#ifdef __QNXNTO__
#include <sys/neutrino.h>   // _NTO_VERSION
#endif

#define VALUE_TO_STRING(x) #x
#define VALUE(x) VALUE_TO_STRING(x)
#define VAR_NAME_VALUE(var) #var "="  VALUE(var)

// @ref: https://docs.microsoft.com/en-us/cpp/preprocessor/predefined-macros
// @ref: clang -dM -E -x c /dev/null
// @ref: gcc -E -dM - < /dev/null

// we use the following macro to detect platform/compiler version. log this to help us detect compile errors
// platform
#pragma message("zs:" VAR_NAME_VALUE(ANDROID))
#pragma message("zs:" VAR_NAME_VALUE(__ANDROID__))
#pragma message("zs:" VAR_NAME_VALUE(__APPLE__))
#pragma message("zs:" VAR_NAME_VALUE(__linux__))
#pragma message("zs:" VAR_NAME_VALUE(_NTO_VERSION)) // >=qnx-660 define this as int
#pragma message("zs:" VAR_NAME_VALUE(__OSX__))
#pragma message("zs:" VAR_NAME_VALUE(__QNX__))
#pragma message("zs:" VAR_NAME_VALUE(__QNXNTO__))
#pragma message("zs:" VAR_NAME_VALUE(__unix__))
#pragma message("zs:" VAR_NAME_VALUE(_WIN32))
#pragma message("zs:" VAR_NAME_VALUE(_WIN64))

// language
#pragma message("zs:" VAR_NAME_VALUE(__cplusplus))
#pragma message("zs:" VAR_NAME_VALUE(_MSVC_LANG))       // __cplusplus in MSVC
#pragma message("zs:" VAR_NAME_VALUE(__STDC__))
#pragma message("zs:" VAR_NAME_VALUE(__STDC_VERSION__))

// compiler
#pragma message("zs:" VAR_NAME_VALUE(__clang__))
#pragma message("zs:" VAR_NAME_VALUE(__clang_major__))
#pragma message("zs:" VAR_NAME_VALUE(__clang_minor__))
#pragma message("zs:" VAR_NAME_VALUE(__GNUC__))
#pragma message("zs:" VAR_NAME_VALUE(__GNUC_MINOR__))
#pragma message("zs:" VAR_NAME_VALUE(_MSC_VER))

// bits
#pragma message("zs:" VAR_NAME_VALUE(_LP64))
#pragma message("zs:" VAR_NAME_VALUE(__LP64__))

// https://en.wikipedia.org/wiki/64-bit_computing

/*
. android-ndk-15c
    ANDROID=ANDROID
    __ANDROID__=1
    __APPLE__=__APPLE__
    __linux__=1
    _NTO_VERSION=_NTO_VERSION
    __OSX__=__OSX__
    __QNX__=__QNX__
    __QNXNTO__=__QNXNTO__
    __unix__=1
    _WIN32=_WIN32
    _WIN64=_WIN64
    __cplusplus=201103L
    _MSVC_LANG=_MSVC_LANG
    __clang__=1
    __clang_major__=5
    __clang_minor__=0
    __GNUC__=4
    __GNUC_MINOR__=2
    _MSC_VER=_MSC_VER


. android-ndk-r10d
    ANDROID=1
    __ANDROID__=1
    __APPLE__=__APPLE__
    __linux__=1
    _NTO_VERSION=_NTO_VERSION
    __OSX__=__OSX__
    __QNX__=__QNX__
    __QNXNTO__=__QNXNTO__
    __unix__=1
    _WIN32=_WIN32
    _WIN64=_WIN64
    __cplusplus=201103L
    _MSVC_LANG=_MSVC_LANG
    __clang__=__clang__
    __GNUC__=4
    __GNUC_MINOR__=9
    _MSC_VER=_MSC_VER
 
 
. ios-sdk8.1-lite
    ANDROID=ANDROID
    __ANDROID__=__ANDROID__
    __APPLE__=1
    __linux__=__linux__
    _NTO_VERSION=_NTO_VERSION
    __OSX__=__OSX__
    __QNX__=__QNX__
    __QNXNTO__=__QNXNTO__
    __unix__=__unix__
    _WIN32=_WIN32
    _WIN64=_WIN64
    __cplusplus=201103L
    _MSVC_LANG=_MSVC_LANG
    __clang__=1
    __GNUC__=4
    __GNUC_MINOR__=2
    _MSC_VER=_MSC_VER
 
 
. linux-centos6x64
    ANDROID=ANDROID
    __ANDROID__=__ANDROID__
    __APPLE__=__APPLE__
    __linux__=1
    _NTO_VERSION=_NTO_VERSION
    __OSX__=__OSX__
    __QNX__=__QNX__
    __QNXNTO__=__QNXNTO__
    __unix__=1
    _WIN32=_WIN32
    _WIN64=_WIN64
    __cplusplus=201103L
    _MSVC_LANG=_MSVC_LANG
    __clang__=__clang__
    __GNUC__=4
    __GNUC_MINOR__=8
    _MSC_VER=_MSC_VER
 
 
. linux-centos6
    ANDROID=ANDROID
    __ANDROID__=__ANDROID__
    __APPLE__=__APPLE__
    __linux__=1
    _NTO_VERSION=_NTO_VERSION
    __OSX__=__OSX__
    __QNX__=__QNX__
    __QNXNTO__=__QNXNTO__
    __unix__=1
    _WIN32=_WIN32
    _WIN64=_WIN64
    __cplusplus=201103L
    _MSVC_LANG=_MSVC_LANG
    __clang__=__clang__
    __GNUC__=4
    __GNUC_MINOR__=8
    _MSC_VER=_MSC_VER
 
 
. osx
    ANDROID=ANDROID
    __ANDROID__=__ANDROID__
    __APPLE__=1
    __linux__=__linux__
    _NTO_VERSION=_NTO_VERSION
    __OSX__=__OSX__
    __QNX__=__QNX__
    __QNXNTO__=__QNXNTO__
    __unix__=__unix__
    _WIN32=_WIN32
    _WIN64=_WIN64
    __cplusplus=201103L
    _MSVC_LANG=_MSVC_LANG
    __clang__=1
    __clang_major__=9
    __clang_minor__=1
    __GNUC__=4
    __GNUC_MINOR__=2
    _MSC_VER=_MSC_VER
    _LP64=1
 
 
. qnx-700
    ANDROID=ANDROID
    __ANDROID__=__ANDROID__
    __APPLE__=__APPLE__
    __linux__=__linux__
    _NTO_VERSION=_NTO_VERSION
    __OSX__=__OSX__
    __QNX__=1
    __QNXNTO__=1
    __unix__=1
    _WIN32=_WIN32
    _WIN64=_WIN64
    __cplusplus=201103L
    _MSVC_LANG=_MSVC_LANG
    __clang__=__clang__
    __GNUC__=5
    __GNUC_MINOR__=4
    _MSC_VER=_MSC_VER

 
. qnx-660
    ANDROID=ANDROID
    __ANDROID__=__ANDROID__
    __APPLE__=__APPLE__
    __linux__=__linux__
    _NTO_VERSION=_NTO_VERSION
    __OSX__=__OSX__
    __QNX__=1
    __QNXNTO__=1
    __unix__=1
    _WIN32=_WIN32
    _WIN64=_WIN64
    __cplusplus=201103L
    _MSVC_LANG=_MSVC_LANG
    __clang__=__clang__
    __GNUC__=4
    __GNUC_MINOR__=7
    _MSC_VER=_MSC_VER
 
 
. qnx (qnx-650)
    ANDROID=ANDROID
    __ANDROID__=__ANDROID__
    __APPLE__=__APPLE__
    __linux__=__linux__
    _NTO_VERSION=_NTO_VERSION
    __OSX__=__OSX__
    __QNX__=1
    __QNXNTO__=1
    __unix__=1
    _WIN32=_WIN32
    _WIN64=_WIN64
    __cplusplus=1
    _MSVC_LANG=_MSVC_LANG
    __clang__=__clang__
    __clang_major__=__clang_major__
    __clang_minor__=__clang_minor__
    __GNUC__=4
    __GNUC_MINOR__=4
    _MSC_VER=_MSC_VER
    _LP64=_LP64
 
 // https://msdn.microsoft.com/en-us/library/hh567368.aspx
. win64-vs14 (VC14, VS2015) partially support c++11
    ANDROID=ANDROID
    __ANDROID__=__ANDROID__
    __APPLE__=__APPLE__
    __linux__=__linux__
    _NTO_VERSION=_NTO_VERSION
    __OSX__=__OSX__
    __QNX__=__QNX__
    __QNXNTO__=__QNXNTO__
    __unix__=__unix__
    _WIN32=1
    _WIN64=1
    __cplusplus=199711L
    _MSVC_LANG=201402L
    __clang__=__clang__
    __clang_major__=__clang_major__
    __clang_minor__=__clang_minor__
    __GNUC__=__GNUC__
    __GNUC_MINOR__=__GNUC_MINOR__
    _MSC_VER=1900
    _LP64=_LP64
 
 
. win32-vs12 (VC12, VS2013) partially support c++11
    ANDROID=ANDROID
    __ANDROID__=__ANDROID__
    __APPLE__=__APPLE__
    __linux__=__linux__
    _NTO_VERSION=_NTO_VERSION
    __OSX__=__OSX__
    __QNX__=__QNX__
    __QNXNTO__=__QNXNTO__
    __unix__=__unix__
    _WIN32=1
    _WIN64=_WIN64
    __cplusplus=199711L
    _MSVC_LANG=_MSVC_LANG
    __clang__=__clang__
    __GNUC__=__GNUC__
    __GNUC_MINOR__=__GNUC_MINOR__
    _MSC_VER=1800
 
 
. win32-vs11 (VC11, VS2012) partially support c++11
    ANDROID=ANDROID
    __ANDROID__=__ANDROID__
    __APPLE__=__APPLE__
    __linux__=__linux__
    _NTO_VERSION=_NTO_VERSION
    __OSX__=__OSX__
    __QNX__=__QNX__
    __QNXNTO__=__QNXNTO__
    __unix__=__unix__
    _WIN32=1
    _WIN64=_WIN64
    __cplusplus=199711L
    _MSVC_LANG=_MSVC_LANG
    __clang__=__clang__
    __GNUC__=__GNUC__
    __GNUC_MINOR__=__GNUC_MINOR__
    _MSC_VER=1700
 + features not supported
    Variadic templates : https://en.cppreference.com/w/cpp/language/parameter_pack
    list initialization : http://en.cppreference.com/w/cpp/language/list_initialization
    default and delete constructor : http://en.cppreference.com/w/cpp/language/default_constructor
 */