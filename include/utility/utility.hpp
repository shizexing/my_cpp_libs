//
//  utility.hpp
//  ALL_BUILD
//
//  Created by Shi, Zexing on 8/3/18.
//

#ifndef utility_utility_h
#define utility_utility_h

#include <tuple>                                // std::get(tuple)
#include <utility>                              // forward, integer_sequence, make_index_sequence

// copy from https://codereview.stackexchange.com/questions/51407/stdtuple-foreach-implementation
template <typename Tuple, typename F, std::size_t ...Indices>
void for_each_impl(Tuple&& tuple, F&& f, std::index_sequence<Indices...>) {
    using swallow = int[];
    (void)swallow{1,
        (f(std::get<Indices>(std::forward<Tuple>(tuple))), int{})...
    };
    // int b[]{1,
    //     (f(std::get<Indices>(std::forward<Tuple>(tuple))), 0)...
    // };
    // (void)b;
}

template <typename Tuple, typename F>
void for_each(Tuple&& tuple, F&& f) {
    constexpr std::size_t N = std::tuple_size<std::remove_reference_t<Tuple>>::value;
    for_each_impl(std::forward<Tuple>(tuple), std::forward<F>(f),
                  std::make_index_sequence<N>{});
}

#endif /* utility_utility_h */
